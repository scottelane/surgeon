# README #

### How do I get set up? ###

Install the following:

* [Visual Studio 2013](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx)
* [Visual Studio Tools for Git](https://visualstudiogallery.msdn.microsoft.com/abafc7d6-dcaa-40f4-8a5e-d6724bdb980c)
* [.NET 4.5.2 Developer Pack](http://go.microsoft.com/fwlink/?LinkId=328857)
* [Microsoft Dynamics CRM 2015 SDK](http://www.microsoft.com/en-au/download/details.aspx?id=44567)
* [Open XML SDK 2.5](https://www.microsoft.com/en-au/download/details.aspx?id=30425)

### Contribution guidelines ###

* Keep all development consistent with existing code, including use of resources, code comments and coding style
* Make all modifications in a new branch from the main branch

### Who do I talk to? ###

* The repository owner