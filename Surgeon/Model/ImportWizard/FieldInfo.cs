﻿using System;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Models basic entity field information for the Job Import Wizard.
    /// </summary>
    public class FieldInfo : IComparable<FieldInfo>
    {
        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the logical name.
        /// </summary>
        public string LogicalName { get; set; }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The display name.</returns>
        public override string ToString()
        {
            return DisplayName;
        }

        /// <summary>
        /// Compares the FieldInfo with another by display name.
        /// </summary>
        /// <param name="other">The other FieldInfo object.</param>
        /// <returns>The comparison result.</returns>
        public int CompareTo(FieldInfo other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return DisplayName.CompareTo(other.DisplayName);
            }
        }
    }
}
