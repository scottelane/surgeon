﻿using System.Collections.Generic;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Models basic CRM State information for the Job Import Wizard.
    /// </summary>
    public class StateInfo
    {
        /// <summary>
        /// Gets or sets the state name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the state code.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Gets or sets the statuses associated with the state.
        /// </summary>
        public List<StatusInfo> Statuses { get; set; }

        /// <summary>
        /// Initialises a new instance of the StateInfo class.
        /// </summary>
        public StateInfo()
        {
            Statuses = new List<StatusInfo>();
        }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The name.</returns>
        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
