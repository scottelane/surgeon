﻿namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Models a CRM entity relationship for the Job Import Wizard.
    /// </summary>
    public class RelationshipInfo
    {
        /// <summary>
        /// Gets or sets the schema name.
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// Gets or sets the entity logical name.
        /// </summary>
        public string EntityLogicalName { get; set; }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The schema name.</returns>
        public override string ToString()
        {
            return SchemaName.ToString();
        }
    }
}
