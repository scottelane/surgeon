﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ScottLane.Surgeon.Model.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Base class for templates that can be created in the Job Import Wizard.
    /// </summary>
    public class Template
    {
        public string EntityDisplayName { get; set; }
        public string EntityLogicalName { get; set; }
        public string EntityPluralName { get; set; }
        public string PrimaryIdFieldName { get; set; }
        public string FileName { get; set; }
        public OperationType OperationType { get; set; }
        public string OrganisationName { get; set; }
        public string RelatedEntityLogicalName { get; set; }
        public string RelatedEntityPluralName { get; set; }
        public string RelatedEntityPrimaryIdFieldName { get; set; }
        public string RelationshipName { get; set; }
        public string Team { get; set; }
        public string User { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public List<FieldInfo> Fields { get; set; }

        /// <summary>
        /// Initialises a new instance of the Template class.
        /// </summary>
        public Template()
        {
            OperationType = OperationType.Unspecified;
            Fields = new List<FieldInfo>();
        }

        /// <summary>
        /// Generates a template.
        /// </summary>
        public virtual void Generate() { }

        /// <summary>
        /// Opens the template.
        /// </summary>
        public virtual void Open()
        {
            Process.Start(FileName);
        }

        protected string GetUniqueFileName(string extension)
        {
            string uniqueFileName = string.Empty;
            string name = string.Empty;

            if (OperationType == OperationType.Activate ||
                OperationType == OperationType.Assign ||
                OperationType == OperationType.Create ||
                OperationType == OperationType.Deactivate ||
                OperationType == OperationType.Delete ||
                OperationType == OperationType.Update ||
                OperationType == OperationType.Upsert)
            {
                name = string.Format(TemplateStrings.OperationEntityFileName, OperationType.ToString(), EntityPluralName);
            }
            else if (OperationType == OperationType.Associate)
            {
                name = string.Format(TemplateStrings.AssociateFileName, OperationType.ToString(), EntityPluralName, RelatedEntityPluralName);
            }
            else if (OperationType == OperationType.Disassociate)
            {
                name = string.Format(TemplateStrings.DisassociateFileName, OperationType.ToString(), EntityPluralName, RelatedEntityPluralName);
            }
            else if (OperationType == OperationType.Publish)
            {
                name = string.Format(TemplateStrings.PublishFileName, OperationType.ToString(), EntityDisplayName);
            }
            else if (OperationType == OperationType.PublishAll)
            {
                name = string.Format(TemplateStrings.PublishAllFileName, OperationType.ToString());
            }
            else if (OperationType == OperationType.SetState)
            {
                name = string.Format(TemplateStrings.SetStatusFileName, EntityPluralName, Status);
            }

            // strip invalid characters from the name
            foreach (char character in Path.GetInvalidFileNameChars())
            {
                name = name.Replace(character, '_');
            }

            uniqueFileName = GetUniqueFileName(name, extension);

            return uniqueFileName;
        }

        /// <summary>
        /// Gets a unique file name in the temp directory with the specified name prefix and extension.
        /// </summary>
        /// <param name="name">The name prefix.</param>
        /// <param name="extension">The extension.</param>
        /// <returns>A unique file name.</returns>
        public static string GetUniqueFileName(string name, string extension)
        {
            return GetUniqueFileName(Path.GetTempPath(), name, extension);
        }

        /// <summary>
        /// Gets a unique file name in the specified path with the specified name prefix and extension.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="name">The file name prefix.</param>
        /// <param name="extension">The extension.</param>
        /// <returns>A unique file name.</returns>
        public static string GetUniqueFileName(string path, string name, string extension)
        {
            string fileName = string.Concat(path, name, extension);
            int index = 0;

            while (File.Exists(fileName))
            {
                fileName = string.Concat(path, name, string.Format(" ({0})", ++index), extension);
            }

            return fileName;
        }
    }
}
