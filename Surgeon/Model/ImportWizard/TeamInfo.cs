﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Models basic CRM Team information for the Job Import Wizard.
    /// </summary>
    public class TeamInfo : IComparable<TeamInfo>
    {
        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public Guid ID { get; set; }
        
        /// <summary>
        /// Gets or sets the team name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets a list of teams for the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>A list of TeamInfo objects.</returns>
        public static List<TeamInfo> GetAllTeams(Connection connection)
        {
            List<TeamInfo> teams = new List<TeamInfo>();
            QueryExpression teamQuery = new QueryExpression("team");
            teamQuery.ColumnSet = new ColumnSet(true);

            if (connection.IsVersionOrAbove(CrmVersion.Crm2013))
            {
                teamQuery.Criteria.AddCondition(new ConditionExpression("teamtype", ConditionOperator.Equal, 0));
            }

            RetrieveMultipleRequest teamRequest = new RetrieveMultipleRequest();
            teamRequest.Query = teamQuery;
            RetrieveMultipleResponse formResponse = (RetrieveMultipleResponse)connection.Service.Execute(teamRequest);

            foreach (Microsoft.Xrm.Sdk.Entity teamMetadata in formResponse.EntityCollection.Entities)
            {
                TeamInfo team = new TeamInfo();
                team.ID = teamMetadata.Id;
                team.Name = teamMetadata.Attributes["name"].ToString();
                teams.Add(team);
            }

            teams.Sort();

            return teams;
        }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The name.</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Compares the object with another TeamInfo object.
        /// </summary>
        /// <param name="other">The other TeamInfo object.</param>
        /// <returns>The comparison result.</returns>
        public int CompareTo(TeamInfo other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return Name.CompareTo(other.Name);
            }
        }
    }
}
