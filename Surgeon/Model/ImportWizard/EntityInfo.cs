﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Models basic CRM entity information for the Job Import Wizard.
    /// </summary>
    public class EntityInfo : IComparable<EntityInfo>
    {
        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the logical name.
        /// </summary>
        public string LogicalName { get; set; }

        /// <summary>
        /// Gets or sets the plural name.
        /// </summary>
        public string PluralName { get; set; }

        /// <summary>
        /// Gets or sets the primary field name.
        /// </summary>
        public string PrimaryFieldName { get; set; }

        /// <summary>
        /// Gets or sets the primary key field name.
        /// </summary>
        public string PrimaryIdFieldName { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether the entity is user or team owned.
        /// </summary>
        public bool IsUserTeamOwned { get; set; }

        /// <summary>
        /// Initialises a new instance of the EntityInfo class.
        /// </summary>
        public EntityInfo()
        {
        }

        /// <summary>
        /// Gets all standard entities using the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>A list of entities.</returns>
        public static List<EntityInfo> GetAllEntities(Connection connection)
        {
            List<EntityInfo> entities = new List<EntityInfo>();
            RetrieveAllEntitiesRequest request = new RetrieveAllEntitiesRequest();
            request.EntityFilters = EntityFilters.Entity;
            request.RetrieveAsIfPublished = false;

            RetrieveAllEntitiesResponse response = (RetrieveAllEntitiesResponse)connection.Service.Execute(request);

            foreach (EntityMetadata entityMetadata in response.EntityMetadata)
            {
                if (entityMetadata.DisplayName.UserLocalizedLabel != null
                        && (entityMetadata.IsCustomizable.Value || !entityMetadata.IsManaged.Value))
                {
                    EntityInfo entity = new EntityInfo();
                    entity.DisplayName = entityMetadata.DisplayName.UserLocalizedLabel.Label;
                    entity.LogicalName = entityMetadata.LogicalName;
                    entity.PluralName = entityMetadata.DisplayCollectionName.UserLocalizedLabel.Label;
                    entity.PrimaryFieldName = entityMetadata.PrimaryNameAttribute;
                    entity.PrimaryIdFieldName = entityMetadata.PrimaryIdAttribute;
                    entity.DisplayName = entityMetadata.DisplayName.UserLocalizedLabel.Label;
                    entity.IsUserTeamOwned = entityMetadata.OwnershipType == OwnershipTypes.TeamOwned || entityMetadata.OwnershipType == OwnershipTypes.UserOwned ? true : false;
                    entities.Add(entity);
                }
            }

            entities.Sort();

            return entities;
        }

        /// <summary>
        /// Gets states associated with the entity.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>A list of statuses.</returns>
        public List<StateInfo> GetStates(Connection connection)
        {
            List<StateInfo> states = new List<StateInfo>();
            RetrieveEntityRequest request = new RetrieveEntityRequest();
            request.LogicalName = LogicalName;
            request.EntityFilters = EntityFilters.Attributes;
            request.RetrieveAsIfPublished = false;

            RetrieveEntityResponse response = (RetrieveEntityResponse)connection.Service.Execute(request);
            StateAttributeMetadata stateMetadata = (StateAttributeMetadata)response.EntityMetadata.Attributes.FirstOrDefault(findField => findField is StateAttributeMetadata);

            if (stateMetadata != null)
            {
                foreach (StateOptionMetadata stateOption in stateMetadata.OptionSet.Options)
                {
                    StateInfo state = new StateInfo();
                    state.Code = (int)stateOption.Value;
                    state.Name = stateOption.Label.UserLocalizedLabel.Label;

                    StatusAttributeMetadata statusMetadata = (StatusAttributeMetadata)response.EntityMetadata.Attributes.FirstOrDefault(findField => findField is StatusAttributeMetadata);

                    if (statusMetadata != null)
                    {
                        foreach (StatusOptionMetadata statusOption in statusMetadata.OptionSet.Options)
                        {
                            if (statusOption.State == state.Code)
                            {
                                StatusInfo status = new StatusInfo();
                                status.Code = (int)statusOption.Value;
                                status.Name = statusOption.Label.UserLocalizedLabel.Label;
                                state.Statuses.Add(status);
                            }
                        }
                    }

                    states.Add(state);
                }
            }

            return states;
        }

        /// <summary>
        /// Gets entity relationships.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The relationships.</returns>
        public List<RelationshipInfo> GetRelationships(Connection connection)
        {
            List<RelationshipInfo> relationships = new List<RelationshipInfo>();
            RetrieveEntityRequest request = new RetrieveEntityRequest();
            request.LogicalName = LogicalName;
            request.EntityFilters = EntityFilters.Relationships;
            request.RetrieveAsIfPublished = false;

            RetrieveEntityResponse response = (RetrieveEntityResponse)connection.Service.Execute(request);

            foreach (ManyToManyRelationshipMetadata metadata in response.EntityMetadata.ManyToManyRelationships)
            {
                RelationshipInfo relationship = new RelationshipInfo();
                relationship.SchemaName = metadata.SchemaName;
                relationship.EntityLogicalName = metadata.Entity1LogicalName == LogicalName ? metadata.Entity2LogicalName : metadata.Entity1LogicalName;
                relationships.Add(relationship);
            }

            foreach (OneToManyRelationshipMetadata metadata in response.EntityMetadata.ManyToOneRelationships)
            {
                RelationshipInfo relationship = new RelationshipInfo();
                relationship.SchemaName = metadata.SchemaName;
                relationship.EntityLogicalName = metadata.ReferencedEntity;
                relationships.Add(relationship);
            }

            foreach (OneToManyRelationshipMetadata metadata in response.EntityMetadata.OneToManyRelationships)
            {
                RelationshipInfo relationship = new RelationshipInfo();
                relationship.SchemaName = metadata.SchemaName;
                relationship.EntityLogicalName = metadata.ReferencingEntity;
                relationships.Add(relationship);
            }

            return relationships;
        }

        /// <summary>
        /// Gets entity fields relevant to the operation type.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="operationType">The operation type.</param>
        /// <returns>The fields.</returns>
        public List<FieldInfo> GetFields(Connection connection, OperationType operationType)
        {
            List<FieldInfo> fields = new List<FieldInfo>();
            RetrieveEntityRequest request = new RetrieveEntityRequest();
            request.LogicalName = LogicalName;
            request.EntityFilters = EntityFilters.Attributes;
            request.RetrieveAsIfPublished = false;

            RetrieveEntityResponse response = (RetrieveEntityResponse)connection.Service.Execute(request);

            foreach (AttributeMetadata fieldMetadata in response.EntityMetadata.Attributes)
            {
                if (((operationType == OperationType.Create && (bool)fieldMetadata.IsValidForCreate) ||
                    (operationType == OperationType.Update && (bool)fieldMetadata.IsValidForUpdate) ||
                    (operationType == OperationType.Upsert && ((bool)fieldMetadata.IsValidForCreate || (bool)fieldMetadata.IsValidForUpdate)))
                    && !(bool)fieldMetadata.IsPrimaryId)
                {
                    if (fieldMetadata.DisplayName.LocalizedLabels.Count > 0)
                    {
                        FieldInfo field = new FieldInfo();
                        field.LogicalName = fieldMetadata.LogicalName;
                        field.DisplayName = fieldMetadata.LogicalName = fieldMetadata.DisplayName.UserLocalizedLabel.Label;
                        fields.Add(field);
                    }
                }
            }

            fields.Sort();

            return fields;
        }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The display name.</returns>
        public override string ToString()
        {
            return DisplayName;
        }

        /// <summary>
        /// Compares the EntityInfo with another by display name.
        /// </summary>
        /// <param name="other">The other EntityInfo object.</param>
        /// <returns>The comparison result.</returns>
        public int CompareTo(EntityInfo other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return DisplayName.CompareTo(other.DisplayName);
            }
        }
    }
}
