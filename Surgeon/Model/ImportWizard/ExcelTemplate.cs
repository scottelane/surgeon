﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ScottLane.Surgeon.Common.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Generates Microsoft Excel templates that provide a starting point for users to create their own Microsoft Excel spreadsheets.
    /// </summary>
    public class ExcelTemplate : Template
    {
        /// <summary>
        /// Initialises a new instance of the ExcelTemplate class.
        /// </summary>
        public ExcelTemplate()
        {
        }

        /// <summary>
        /// Generates a Microsoft Excel template based on the provided FileName, EntityLogicalName and OperationType.
        /// </summary>
        public override void Generate()
        {
            using (SpreadsheetDocument spreadsheet = SpreadsheetDocument.Create(FileName, SpreadsheetDocumentType.Workbook, false))
            {
                // create the workbook
                spreadsheet.AddWorkbookPart();
                spreadsheet.WorkbookPart.Workbook = new Workbook();
                spreadsheet.WorkbookPart.Workbook.Save();

                // add shared string part
                SharedStringTablePart sharedStringTablePart = spreadsheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                sharedStringTablePart.SharedStringTable = new SharedStringTable();
                sharedStringTablePart.SharedStringTable.Save();

                spreadsheet.WorkbookPart.AddNewPart<WorksheetPart>();
                spreadsheet.WorkbookPart.WorksheetParts.First().Worksheet = new Worksheet();
                spreadsheet.WorkbookPart.WorksheetParts.First().Worksheet.AppendChild(new SheetData());

                if (OperationType == OperationType.Activate)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName, FieldName.RecordId);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName, Guid.NewGuid().ToString());
                }
                else if (OperationType == OperationType.Assign)
                {
                    string ownerHeader = User != null ? FieldName.User : FieldName.Team;
                    string ownerValue = User != null ? User : Team;

                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName, FieldName.RecordId, ownerHeader);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName, Guid.NewGuid().ToString(), ownerValue);
                }
                else if (OperationType == OperationType.Associate)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName, FieldName.RecordId, FieldName.RelatedEntityLogicalName, FieldName.RelatedRecordId, FieldName.Relationship);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName, Guid.NewGuid().ToString(), RelatedEntityLogicalName, Guid.NewGuid().ToString(), RelationshipName);
                }
                else if (OperationType == OperationType.Create)
                {
                    List<string> parameters = new List<string>(Fields.Select(field => field.LogicalName).ToList());
                    parameters.Insert(0, FieldName.EntityLogicalName);
                    parameters.Insert(0, FieldName.OperationType);
                    CreateRow(spreadsheet, 1, parameters.ToArray());

                    List<string> values = new List<string>();
                    values.Add(this.OperationType.ToString());
                    values.Add(EntityLogicalName);

                    foreach (FieldInfo field in Fields)
                    {
                        values.Add(null);
                    }

                    CreateRow(spreadsheet, 2, values.ToArray());                    
                }
                else if (OperationType == OperationType.Deactivate)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName, FieldName.RecordId);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName, Guid.NewGuid().ToString());
                }
                else if (OperationType == OperationType.Delete)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName, FieldName.RecordId);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName, Guid.NewGuid().ToString());
                }
                else if (OperationType == OperationType.Disassociate)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName, FieldName.RecordId, FieldName.RelatedEntityLogicalName, FieldName.RelatedRecordId, FieldName.Relationship);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName, Guid.NewGuid().ToString(), RelatedEntityLogicalName, Guid.NewGuid().ToString(), RelationshipName);
                }
                else if (OperationType == OperationType.Publish)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName);
                }
                else if (OperationType == OperationType.PublishAll)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString());
                }
                else if (OperationType == OperationType.SetState)
                {
                    CreateRow(spreadsheet, 1, FieldName.OperationType, FieldName.EntityLogicalName, FieldName.RecordId, FieldName.State, FieldName.Status);
                    CreateRow(spreadsheet, 2, this.OperationType.ToString(), EntityLogicalName, Guid.NewGuid().ToString(), State, Status);
                }
                else if (OperationType == OperationType.Update)
                {
                    List<string> parameters = new List<string>(Fields.Select(field => field.LogicalName).ToList());
                    parameters.Insert(0, FieldName.RecordId);
                    parameters.Insert(0, FieldName.EntityLogicalName);
                    parameters.Insert(0, FieldName.OperationType);
                    CreateRow(spreadsheet, 1, parameters.ToArray());

                    List<string> values = new List<string>();
                    values.Add(this.OperationType.ToString());
                    values.Add(EntityLogicalName);
                    values.Add(Guid.NewGuid().ToString());

                    foreach (FieldInfo field in Fields)
                    {
                        values.Add(null);
                    }

                    CreateRow(spreadsheet, 2, values.ToArray());
                }
                else if (OperationType == OperationType.Upsert)
                {
                    List<string> parameters = new List<string>(Fields.Select(field => field.LogicalName).ToList());
                    parameters.Insert(0, FieldName.RecordId);
                    parameters.Insert(0, FieldName.EntityLogicalName);
                    parameters.Insert(0, FieldName.OperationType);
                    CreateRow(spreadsheet, 1, parameters.ToArray());

                    List<string> values = new List<string>();
                    values.Add(this.OperationType.ToString());
                    values.Add(EntityLogicalName);
                    values.Add(Guid.NewGuid().ToString());

                    foreach (FieldInfo field in Fields)
                    {
                        values.Add(null);
                    }

                    CreateRow(spreadsheet, 2, values.ToArray());
                }

                spreadsheet.WorkbookPart.WorksheetParts.First().Worksheet.Save();
                spreadsheet.WorkbookPart.Workbook.AppendChild(new Sheets());
                spreadsheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet()
                {
                    Id = spreadsheet.WorkbookPart.GetIdOfPart(spreadsheet.WorkbookPart.WorksheetParts.First()),
                    SheetId = 1,
                    Name = OperationType.ToString() // todo - what here?
                });

                spreadsheet.WorkbookPart.Workbook.Save();
            }
        }

        /// <summary>
        /// Creates a spreadsheet row with the specified cell values.
        /// </summary>
        /// <param name="spreadsheet">The spreadsheet.</param>
        /// <param name="cellValues">The cell values.</param>
        private void CreateRow(SpreadsheetDocument spreadsheet, int rowIndex, params string[] cellValues)
        {
            Row row = new Row();
            row.RowIndex = UInt32Value.FromUInt32((uint)rowIndex);
            spreadsheet.WorkbookPart.WorksheetParts.First().Worksheet.First().AppendChild(row);

            int columnNumber = 1;

            foreach (string cellValue in cellValues)
            {
                Cell cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue(cellValue);
                cell.CellReference = string.Format("{0}{1}", GetExcelColumnName(columnNumber++), rowIndex);
                spreadsheet.WorkbookPart.WorksheetParts.First().Worksheet.First().Last().AppendChild(cell);
            }
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        /// <summary>
        /// Gets a unique template file name.
        /// </summary>
        /// <returns></returns>
        protected string GetUniqueFileName()
        {
            return GetUniqueFileName(CommonStrings.ExcelFileExtension);
        }

        /// <summary>
        /// Creates a ExcelTemplate from the specified template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>The ExcelTemplate.</returns>
        public static ExcelTemplate FromTemplate(Template template)
        {
            ExcelTemplate excelTemplate = new ExcelTemplate();
            excelTemplate.EntityDisplayName = template.EntityDisplayName;
            excelTemplate.EntityLogicalName = template.EntityLogicalName;
            excelTemplate.EntityPluralName = template.EntityPluralName;
            excelTemplate.Fields = template.Fields;
            excelTemplate.OperationType = template.OperationType;
            excelTemplate.OrganisationName = template.OrganisationName;
            excelTemplate.PrimaryIdFieldName = template.PrimaryIdFieldName;
            excelTemplate.RelatedEntityLogicalName = template.RelatedEntityLogicalName;
            excelTemplate.RelatedEntityPluralName = template.RelatedEntityPluralName;
            excelTemplate.RelatedEntityPrimaryIdFieldName = template.RelatedEntityPrimaryIdFieldName;
            excelTemplate.RelationshipName = template.RelationshipName;
            excelTemplate.Team = template.Team;
            excelTemplate.User = template.User;
            excelTemplate.State = template.State;
            excelTemplate.Status = template.Status;
            excelTemplate.FileName = excelTemplate.GetUniqueFileName();

            return excelTemplate;
        }
    }
}
