﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Models basic CRM User information for the Job Import Wizard.
    /// </summary>
    public class UserInfo : IComparable<UserInfo>
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets a list of users for the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>A list of UserInfo objects.</returns>
        public static List<UserInfo> GetAllUsers(Connection connection)
        {
            List<UserInfo> users = new List<UserInfo>();
            QueryExpression userQuery = new QueryExpression("systemuser");
            userQuery.ColumnSet = new ColumnSet(true);
            RetrieveMultipleRequest userRequest = new RetrieveMultipleRequest();
            userRequest.Query = userQuery;
            RetrieveMultipleResponse formResponse = (RetrieveMultipleResponse)connection.Service.Execute(userRequest);

            foreach (Microsoft.Xrm.Sdk.Entity userMetadata in formResponse.EntityCollection.Entities)
            {
                UserInfo user = new UserInfo();
                user.ID = userMetadata.Id;
                user.Name = userMetadata.Attributes["fullname"].ToString();
                users.Add(user);
            }

            users.Sort();

            return users;
        }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The user name.</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Compares the object with another UserInfo objects.
        /// </summary>
        /// <param name="other">The other UserInfo object to compare to.</param>
        /// <returns>The comparison result.</returns>
        public int CompareTo(UserInfo other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return Name.CompareTo(other.Name);
            }
        }
    }
}
