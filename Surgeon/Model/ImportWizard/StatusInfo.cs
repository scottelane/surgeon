﻿namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Models basic CRM Status information for the Job Import Wizard.
    /// </summary>
    public class StatusInfo
    {
        /// <summary>
        /// Gets or sets the status name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        public int Code { get; set; }
        
        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The name.</returns>
        public override string ToString()
        {
            return Name.ToString();
        }        
    }
}
