﻿using System;
using System.IO;
using ScottLane.Surgeon.Common.Resources;
using ScottLane.Surgeon.Model.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Generates T-SQL templates that provide a starting point for users to create their own T-SQL job scripts.
    /// </summary>
    public class SqlTemplate : Template
    {
        /// <summary>
        /// Initializes a new instance of the SqlTemplate class.
        /// </summary>
        public SqlTemplate()
        {
        }

        /// <summary>
        /// Generates a sql template based on the provided FileName, EntityLogicalName and OperationType.
        /// </summary>
        public override void Generate()
        {
            using (StreamWriter writer = new StreamWriter(FileName, false))
            {
                if (OperationType == OperationType.Activate)
                {
                    WriteActivateTemplate(writer);
                }
                else if (OperationType == OperationType.Assign)
                {
                    WriteAssignTemplate(writer);
                }
                else if (OperationType == OperationType.Associate)
                {
                    WriteAssociateTemplate(writer);
                }
                else if (OperationType == OperationType.Create)
                {
                    WriteCreateTemplate(writer);
                }
                else if (OperationType == OperationType.Deactivate)
                {
                    WriteDeactivateTemplate(writer);
                }
                else if (OperationType == OperationType.Delete)
                {
                    WriteDeleteTemplate(writer);
                }
                else if (OperationType == OperationType.Disassociate)
                {
                    WriteDisassociateTemplate(writer);
                }
                else if (OperationType == OperationType.Publish)
                {
                    WritePublishTemplate(writer);
                }
                else if (OperationType == OperationType.PublishAll)
                {
                    WritePublishAllTemplate(writer);
                }
                else if (OperationType == OperationType.SetState)
                {
                    WriteSetStateTemplate(writer);
                }
                else if (OperationType == OperationType.Update)
                {
                    WriteUpdateTemplate(writer);
                }
                else if (OperationType == OperationType.Upsert)
                {
                    WriteUpsertTemplate(writer);
                }
            }
        }

        /// <summary>
        /// Writes the generic template header.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteHeader(StreamWriter writer)
        {
            writer.Write(string.Format(TemplateStrings.HeaderSql, Environment.NewLine, CommonStrings.ProductNameShort, DateTime.Now, CommonStrings.HelpSqlJobUrl, CommonStrings.HelpOperationsUrl));
        }

        /// <summary>
        /// Writes the generic template footer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteFooter(StreamWriter writer)
        {
            writer.Write(string.Format(TemplateStrings.FooterSql, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic Operation statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteOperation(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.OperationSql, OperationType, FieldName.OperationType, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic Entity statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteEntity(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.EntitySql, EntityLogicalName, FieldName.EntityLogicalName, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic RecordId statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteRecordId(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.RecordIdSql, PrimaryIdFieldName, FieldName.RecordId, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic user statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteUser(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.UserSql, User, FieldName.User, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic Team statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteTeam(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.TeamSql, Team, FieldName.Team, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic RelatedEntity statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteRelatedEntity(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.RelatedEntitySql, RelatedEntityLogicalName, FieldName.RelatedEntityLogicalName, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic RecordId statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteRelatedRecordId(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.RelatedRecordIdSql, RelatedEntityPrimaryIdFieldName, FieldName.RelatedRecordId, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a generic RelatedRelationship statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="includeTrailingComma">If true, includes a trailing comma.</param>
        private void WriteRelatedRelationship(StreamWriter writer, bool includeTrailingComma)
        {
            string trailingComma = includeTrailingComma ? "," : string.Empty;
            writer.Write(string.Format(TemplateStrings.RelationshipSql, RelationshipName, FieldName.Relationship, trailingComma, Environment.NewLine));
        }

        /// <summary>
        /// Writes a list of fields.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteFields(StreamWriter writer)
        {
            for (int fieldIndex = 0; fieldIndex < Fields.Count; fieldIndex++)
            {
                string trailingComma = fieldIndex < Fields.Count - 1 ? "," : string.Empty;
                writer.Write(string.Format(TemplateStrings.FieldSql, Fields[fieldIndex].LogicalName, trailingComma, Environment.NewLine));
            }
        }

        /// <summary>
        /// Writes a generic From statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteFromStatement(StreamWriter writer)
        {
            writer.Write(string.Format(TemplateStrings.FromStatementSql, OrganisationName, EntityLogicalName, Environment.NewLine));
        }

        /// <summary>
        /// Writes the generic where statement.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteWhereStatement(StreamWriter writer)
        {
            writer.Write(string.Format(TemplateStrings.WhereSql, Environment.NewLine));
        }

        #region Operation Templates

        /// <summary>
        /// Writes the Activate operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteActivateTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, false);
            WriteFromStatement(writer);
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Assign operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteAssignTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, true);

            if (User != null)
            {
                WriteUser(writer, false);
            }
            else if (Team != null)
            {
                WriteTeam(writer, false);
            }

            WriteFromStatement(writer);
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Associate operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteAssociateTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, true);
            WriteRelatedEntity(writer, true);
            WriteRelatedRecordId(writer, true);
            WriteRelatedRelationship(writer, false);
            WriteFromStatement(writer);
            writer.Write(string.Format(TemplateStrings.AssociateJoinSql, OrganisationName, RelatedEntityLogicalName, Environment.NewLine));
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Create operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteCreateTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer,  Fields.Count > 0);
            WriteFields(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Disassociate operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteDisassociateTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, true);
            WriteRelatedEntity(writer, true);
            WriteRelatedRecordId(writer, true);
            WriteRelatedRelationship(writer, false);
            writer.Write(string.Format(TemplateStrings.DisassociateFromSql, OrganisationName, RelationshipName, Environment.NewLine));
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Deactivate operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteDeactivateTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, false);
            WriteFromStatement(writer);
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Delete operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteDeleteTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, false);
            WriteFromStatement(writer);
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Publish operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WritePublishTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, false);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the PublishAll operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WritePublishAllTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, false);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the SetState operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteSetStateTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, true);
            writer.Write(string.Format(TemplateStrings.StateSql, State, FieldName.State, Environment.NewLine));
            writer.Write(string.Format(TemplateStrings.StatusSql, Status, FieldName.Status, Environment.NewLine));
            WriteFromStatement(writer);
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Update operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteUpdateTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, Fields.Count > 0);
            WriteFields(writer);
            WriteFromStatement(writer);
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        /// <summary>
        /// Writes the Upsert operation template.
        /// </summary>
        /// <param name="writer">The writer.</param>
        private void WriteUpsertTemplate(StreamWriter writer)
        {
            WriteHeader(writer);
            WriteOperation(writer, true);
            WriteEntity(writer, true);
            WriteRecordId(writer, Fields.Count > 0);
            WriteFields(writer);
            WriteFromStatement(writer);
            WriteWhereStatement(writer);
            WriteFooter(writer);
        }

        #endregion

        /// <summary>
        /// Gets a unique template file name.
        /// </summary>
        /// <returns></returns>
        protected string GetUniqueFileName()
        {
            return GetUniqueFileName(CommonStrings.SqlFileExtension);
        }

        /// <summary>
        /// Creates a SqlTemplate from the specified template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>The SqlTemplate.</returns>
        public static SqlTemplate FromTemplate(Template template)
        {
            SqlTemplate sqlTemplate = new SqlTemplate();
            sqlTemplate.EntityDisplayName = template.EntityDisplayName;
            sqlTemplate.EntityLogicalName = template.EntityLogicalName;
            sqlTemplate.EntityPluralName = template.EntityPluralName;
            sqlTemplate.Fields = template.Fields;
            sqlTemplate.OperationType = template.OperationType;
            sqlTemplate.OrganisationName = template.OrganisationName;
            sqlTemplate.PrimaryIdFieldName = template.PrimaryIdFieldName;
            sqlTemplate.RelatedEntityLogicalName = template.RelatedEntityLogicalName;
            sqlTemplate.RelatedEntityPluralName = template.RelatedEntityPluralName;
            sqlTemplate.RelatedEntityPrimaryIdFieldName = template.RelatedEntityPrimaryIdFieldName;
            sqlTemplate.RelationshipName = template.RelationshipName;
            sqlTemplate.Team = template.Team;
            sqlTemplate.User = template.User;
            sqlTemplate.State = template.State;
            sqlTemplate.Status = template.Status;
            sqlTemplate.FileName = sqlTemplate.GetUniqueFileName();

            return sqlTemplate;
        }
    }
}
