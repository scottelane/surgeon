﻿using System;
using System.Data.SqlClient;
using System.Net;
using System.Security.Cryptography;
using System.ServiceModel.Description;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm.Sdk.Messages;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Manages CRM and SQL connections.
    /// </summary>
    public class Connection
    {
        public static readonly TimeSpan DefaultCrmTimeout = TimeSpan.FromMinutes(2);
        public static readonly TimeSpan DefaultSqlTimeout = TimeSpan.FromMinutes(10);

        #region Properties

        /// <summary>
        /// Gets or sets the unique connection name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the CRM organisation service uri.
        /// </summary>
        [XmlIgnore]
        public Uri OrganisationServiceUri { get; set; }

        /// <summary>
        /// Gets or sets the organisation service url. For serialisation purposes only.
        /// </summary>
        public string OrganisationServiceUrl
        {
            get
            {
                if (OrganisationServiceUri != null)
                {
                    return OrganisationServiceUri.ToString();
                }

                return null;
            }
            set
            {
                OrganisationServiceUri = new Uri(value);
            }
        }
        
        /// <summary>
        /// Gets or set the CRM connection timeout.
        /// </summary>
        [XmlIgnore]
        public TimeSpan CrmTimeout { get; set; }

        /// <summary>
        /// Gets or sets the CRM connection timeout in ticks. For serialisation purposes only.
        /// </summary>
        public long CrmTimeoutTicks
        {
            get
            {
                if (CrmTimeout != null)
                {
                    return CrmTimeout.Ticks;
                }

                return Connection.DefaultCrmTimeout.Ticks;
            }
            set
            {
                CrmTimeout = TimeSpan.FromTicks(value);
            }
        }

        /// <summary>
        /// Gets the CRM organisation name.
        /// </summary>
        [XmlIgnore]
        public string OrganisationName
        {
            get
            {
                // todo - this works but isn't matching in the way that was expected
                string organisationName = string.Empty;
                Match match = Regex.Match(OrganisationServiceUrl, @"https?://(?:(?<organisationName>[^./]+)(?:.api)?.crm\d*.dynamics.com)|(?<organisationName>[^/]+)/XRMServices/2011/Organization.svc", RegexOptions.Compiled | RegexOptions.IgnoreCase);

                if (match.Success)
                {
                    organisationName = match.Groups["organisationName"].Value;
                }

                return organisationName;
            }
        }

        /// <summary>
        /// Gets or sets the type of authentication used to connect to CRM.
        /// </summary>
        public AuthenticationType AuthenticationType { get; set; }

        /// <summary>
        /// Gets or sets the domain associated with the CRM user name.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets the CRM user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the encrypted CRM password.
        /// </summary>
        public byte[] Password { get; set; }

        /// <summary>
        /// Gets or sets the CRM version number. Populated after connecting to CRM.
        /// </summary>
        [XmlIgnore]
        public string VersionNumber { get; set; }

        /// <summary>
        /// Gets or sets the encrypted SQL connection string of the server to run T-SQL jobs on.
        /// </summary>
        public byte[] ConnectionString { get; set; }

        /// <summary>
        /// Gets or set the SQL connection timeout.
        /// </summary>
        [XmlIgnore]
        public TimeSpan SqlTimeout { get; set; }

        /// <summary>
        /// Gets or sets the SQL connection timeout in ticks. For serialisation purposes only.
        /// </summary>
        public long SqlTimeoutTicks
        {
            get
            {
                if (SqlTimeout != null)
                {
                    return SqlTimeout.Ticks;
                }

                return Connection.DefaultSqlTimeout.Ticks;
            }
            set
            {
                SqlTimeout = TimeSpan.FromTicks(value);
            }
        }

        private IOrganizationService service;

        /// <summary>
        /// Gets the CRM IOrganizationService associated with the connection.
        /// </summary>
        public IOrganizationService Service
        {
            get { return service; }
        }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Connection class.
        /// </summary>
        public Connection()
        {
            CrmTimeout = DefaultCrmTimeout;
            SqlTimeout = DefaultSqlTimeout;
        }

        /// <summary>
        /// Connects to the CRM connection.
        /// </summary>
        /// <returns>The connection result.</returns>
        public ConnectionResult Connect()
        {
            ConnectionResult result = ConnectionResult.None;

            try
            {
                // connect to CRM
                ClientCredentials credentials = new ClientCredentials();

                if (AuthenticationType == AuthenticationType.Custom)
                {
                    if (!string.IsNullOrEmpty(Domain))
                    {
                        credentials.Windows.ClientCredential = new NetworkCredential(UserName, Decrypt(Password), Domain);
                    }
                    else
                    {
                        credentials.UserName.UserName = UserName;
                        credentials.UserName.Password = Decrypt(Password);
                    }
                }
                else
                {
                    credentials.Windows.ClientCredential = CredentialCache.DefaultNetworkCredentials;
                }

                OrganizationServiceProxy proxy = new OrganizationServiceProxy(OrganisationServiceUri, null, credentials, null);
                proxy.Timeout = CrmTimeout;
                service = (IOrganizationService)proxy;
                RetrieveVersionRequest request = new RetrieveVersionRequest();
                RetrieveVersionResponse response = (RetrieveVersionResponse)service.Execute(request);
                VersionNumber = response.Version;

                result = result | ConnectionResult.CrmConnectionSucceeded;
            }
            catch
            {
                result = result | ConnectionResult.CrmConnectionFailed;
            }

            try
            {
                // test sql connectivity
                if (string.IsNullOrEmpty(Decrypt(ConnectionString)))
                {
                    result = result | ConnectionResult.SqlConnectionMissing;
                }
                else
                {
                    SqlConnection connection = new SqlConnection(Decrypt(ConnectionString));
                    connection.Open();
                    connection.Close();
                    result = result | ConnectionResult.SqlConnectionSucceeded;
                }
            }
            catch
            {
                result = result | ConnectionResult.SqlConnectionFailed;
            }

            return result;
        }


        /// <summary>
        /// Closes the CRM connection.
        /// </summary>
        public void Close()
        {
            ((OrganizationServiceProxy)service).Dispose();
        }

        /// <summary>
        /// Publishes all customisations in the CRM instance.
        /// </summary>
        public void PublishAll()
        {
            PublishAllXmlRequest request = new PublishAllXmlRequest();
            service.Execute(request);
        }

        /// <summary>
        /// Gets a value indicating whether the connection is for an organisation that is at or above the specified CRM version.
        /// </summary>
        /// <returns></returns>
        public bool IsVersionOrAbove(CrmVersion version)
        {
            bool result = false;

            if (VersionNumber != null && int.Parse(VersionNumber.Substring(0, VersionNumber.IndexOf("."))) >= (int)version) 
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Compares two Connections by name.
        /// </summary>
        /// <param name="obj">The object to compare.</param>
        /// <returns>True if equal, False otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Connection connection = obj as Connection;

            if ((Object)connection == null)
            {
                return false;
            }

            return Name == connection.Name;
        }

        /// <summary>
        /// Overrides the GetHashCode method.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Encrypts the specified text.
        /// </summary>
        /// <param name="text">The text to encrypt.</param>
        /// <returns>Encrypted text.</returns>
        public static byte[] Encrypt(string text)
        {
            return ProtectedData.Protect(Encoding.Unicode.GetBytes(text), null, DataProtectionScope.CurrentUser);
        }

        /// <summary>
        /// Decrypts the specified encrypted text.
        /// </summary>
        /// <param name="encryptedText">The encrypted text to decrypt.</param>
        /// <returns>Decrypted text.</returns>
        public static string Decrypt(byte[] encryptedText)
        {
            return Encoding.Unicode.GetString(ProtectedData.Unprotect(encryptedText, null, DataProtectionScope.CurrentUser));
        }
    }
}
