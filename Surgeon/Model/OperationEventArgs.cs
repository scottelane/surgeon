﻿namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Arguments passed by operation-related events.
    /// </summary>
    public class OperationEventArgs
    {
        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        public Operation Operation { get; set; }

        /// <summary>
        /// Gets or sets the processing status.
        /// </summary>
        public ProcessingOutcome ProcessingOutcome { get; set; }

        /// <summary>
        /// Gets or sets an error message associated with operation processing.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the processing progress within the batch.
        /// </summary>
        public int Progress { get; set; }

        /// <summary>
        /// Initialises a new instance of the OperationEventArgs class with the specified operation.
        /// </summary>
        /// <param name="operation">The operation.</param>
        public OperationEventArgs(Operation operation)
        {
            Operation = operation;
        }
    }

    /// <summary>
    /// Delegate for operation-related events.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void OperationProcessedEventHandler(object sender, OperationEventArgs e);
}
