﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using ScottLane.Surgeon.Model.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Supports run-time resolution of ID and lookup fields.
    /// </summary>
    public class Lookup
    {
        private const char entityDelimiter = ':';
        private const char equalsDelimiter = '=';
        private const char pairDelimiter = ';';

        #region Properties

        /// <summary>
        /// Gets or sets the lookup type.
        /// </summary>
        [XmlAttribute]
        public LookupType LookupType { get; set; }

        /// <summary>
        /// Gets or sets the original lookup string.
        /// </summary>
        [XmlAttribute]
        public string OriginalString { get; set; }

        /// <summary>
        /// Gets or sets the entity being looked up to.
        /// </summary>
        [XmlAttribute]
        public string EntityLogicalName { get; set; }

        /// <summary>
        /// Gets or sets the guid of the record being looked up to for a Guid type lookup.
        /// </summary>
        [XmlAttribute]
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the logical name of the primary ID field to look up.
        /// </summary>
        [XmlAttribute]
        public string PrimaryIdLogicalName { get; set; }

        /// <summary>
        /// Gets or sets the 'Name' field value to look up.
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the field values being looked up.
        /// </summary>
        public List<Field> Fields { get; set; }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Lookup class.
        /// </summary>
        public Lookup()
        {
            Fields = new List<Field>();
        }

        /// <summary>
        /// Converts the string representation of a lookup into a lookup. A return value indicates whether the conversion succeeded or failed.
        /// </summary>
        /// <param name="lookupString">The string representation of the lookup to convert.</param>
        /// <param name="fieldDefinition">The definition of the field.</param>
        /// <param name="result">The resulting lookup if the conversion succeeded, otherwise null.</param>
        /// <returns>True if the conversion succeeded, false otherwise.</returns>
        /// <remarks>
        /// Lookups can be in the form:
        /// D0FA1912-FFCB-4C4A-AA1C-88DACC78419E
        /// {D0FA1912-FFCB-4C4A-AA1C-88DACC78419E}
        /// namefieldvalue
        /// new_field1=field1value;new_field2=field2value
        /// </remarks>
        public static bool TryParse(string lookupString, AttributeMetadata fieldDefinition, out Lookup result, Connection connection)
        {
            bool success = false;
            result = new Lookup();
            result.OriginalString = lookupString;

            if ((bool)fieldDefinition.IsPrimaryId)
            {
                result.EntityLogicalName = fieldDefinition.EntityLogicalName;
                result.PrimaryIdLogicalName = fieldDefinition.LogicalName;
            }
            else
            {
                LookupAttributeMetadata lookupDefinition = fieldDefinition as LookupAttributeMetadata;
                result.EntityLogicalName = lookupDefinition.Targets[0];

                if (lookupString.Contains(entityDelimiter))
                {
                    result.EntityLogicalName = lookupString.Substring(0, lookupString.IndexOf(entityDelimiter));
                }
            }

            // strip the entity if it exists
            if (lookupString.Contains(entityDelimiter))
            {
                // allow date/times in lookup strings (colon in time would incorrectly cause entity name match)
                if (lookupString.Contains(pairDelimiter) && lookupString.IndexOf(entityDelimiter) < lookupString.IndexOf(pairDelimiter))
                {
                    lookupString = lookupString.Substring(lookupString.IndexOf(entityDelimiter) + entityDelimiter.ToString().Length);
                }
            }

            if (!string.IsNullOrEmpty(lookupString))
            {
                Guid lookupGuid;

                // check for a guid
                if (Guid.TryParse(lookupString, out lookupGuid))
                {
                    result.LookupType = LookupType.Guid;
                    result.Guid = lookupGuid;
                    success = true;
                }
                // check for field-value pairs
                else if (lookupString.Contains(equalsDelimiter))
                {
                    result.LookupType = LookupType.FieldValuePair;
                    string[] pairs = lookupString.Split(pairDelimiter);

                    foreach (string pair in pairs)
                    {
                        if (pair != string.Empty)
                        {
                            string[] fieldValues = pair.Split(equalsDelimiter);

                            Field field = new Field();
                            field.LogicalName = fieldValues[0].ToLower();

                            RetrieveEntityResponse entityDefinition = EntityCache.GetEntityDefinition(result.EntityLogicalName, connection);
                            AttributeMetadata lookupField = entityDefinition.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName == field.LogicalName);

                            if (lookupField == null)
                            {
                                throw new ApplicationException(string.Format(ModelStrings.FieldDoesNotExistText, field.LogicalName, result.EntityLogicalName));
                            }
                            else if (lookupField.AttributeType == AttributeTypeCode.Lookup)
                            {
                                Lookup lookup = null;

                                if (Lookup.TryParse(fieldValues[1], lookupField, out lookup, connection))
                                {
                                    field.Value = lookup;
                                }
                            }
                            else
                            {
                                field.Value = fieldValues[1];
                            }

                            result.Fields.Add(field);
                        }
                    }

                    success = true;
                }
                // assume a value for the 'name' field has been passed
                else
                {
                    result.LookupType = LookupType.NameLookup;
                    result.Name = lookupString;
                    success = true;
                }
            }

            if (!success)
            {
                result = null;
            }

            return success;
        }

        /// <summary>
        /// Resolves a lookup field reference and sets the Guid field of the lookup.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="throwIfNotFound">If True, throws an ApplicationException if the entity reference is not found.</param>
        /// <returns>An entity reference to the looked up record.</returns>
        public EntityReference Resolve(Connection connection, bool throwIfNotFound)
        {
            const string modifiedOnFieldName = "modifiedon";
            EntityReference reference = null;

            if (LookupType == LookupType.FieldValuePair)
            {
                RetrieveEntityResponse entityDefinition = EntityCache.GetEntityDefinition(EntityLogicalName, connection);
                QueryByAttribute query = new QueryByAttribute(EntityLogicalName);

                foreach (Field field in Fields)
                {
                    AttributeMetadata fieldDefinition = entityDefinition.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName == field.LogicalName);
                    object value = Operation.GetCrmFieldValue(entityDefinition, fieldDefinition, field.Value, connection);

                    if (value is Lookup)
                    {
                        EntityReference childReference = ((Lookup)value).Resolve(connection);
                        value = childReference.Id;
                    }
                    else if (value is OptionSetValue)
                    {
                        value = ((OptionSetValue)value).Value;
                    }

                    query.AddAttributeValue(field.LogicalName, value);
                }

                if (ContainsAttribute(entityDefinition.EntityMetadata.Attributes, modifiedOnFieldName))
                {
                    query.AddOrder(modifiedOnFieldName, OrderType.Descending);
                }

                EntityCollection records = connection.Service.RetrieveMultiple(query);

                if (records.Entities.Count > 0)
                {
                    reference = new EntityReference(EntityLogicalName, records.Entities.First().Id);
                }
                else if (throwIfNotFound)
                {
                    throw new ApplicationException(string.Format(ModelStrings.LookupNotResolvedText, OriginalString));
                }
            }
            else if (LookupType == LookupType.Guid)
            {
                reference = new EntityReference(EntityLogicalName, Guid);
            }
            else if (LookupType == LookupType.NameLookup)
            {
                QueryByAttribute query = new QueryByAttribute(EntityLogicalName);

                // get the name of the 'name' field on the target entity
                RetrieveEntityResponse entityDefinition = EntityCache.GetEntityDefinition(EntityLogicalName, connection);
                query.AddAttributeValue(entityDefinition.EntityMetadata.PrimaryNameAttribute, Name);

                if (ContainsAttribute(entityDefinition.EntityMetadata.Attributes, modifiedOnFieldName))
                {
                    query.AddOrder(modifiedOnFieldName, OrderType.Descending);
                }

                EntityCollection records = connection.Service.RetrieveMultiple(query);

                if (records.Entities.Count > 0)
                {
                    reference = new EntityReference(EntityLogicalName, records.Entities.First().Id);
                }
                else if (throwIfNotFound)
                {
                    throw new ApplicationException(string.Format(ModelStrings.LookupNotResolvedText, OriginalString));
                }
            }

            if (reference != null)
            {
                Guid = reference.Id;
            }

            return reference;
        }

        /// <summary>
        /// Resolves a lookup field reference and sets the Guid field of the lookup. Throws an ApplicationException if the entity reference is not found.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>An entity reference to the looked up record.</returns>
        public EntityReference Resolve(Connection connection)
        {
            return Resolve(connection, true);
        }

        /// <summary>
        /// Returns true if the attribute array contains an attribute with the specified attribute name, false otherwise.
        /// </summary>
        /// <param name="attributes">The attribute array.</param>
        /// <param name="attributeName">The attribute to find.</param>
        /// <returns>True if found, false otherwise.</returns>
        private bool ContainsAttribute(AttributeMetadata[] attributes, string attributeName)
        {
            foreach (AttributeMetadata attribute in attributes)
            {
                if (attribute.LogicalName.ToLower() == attributeName.ToLower())
                {
                    return true;
                }
            }

            return false;
        }
    }
}
