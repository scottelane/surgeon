﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using ScottLane.Surgeon.Model.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Supports run-time resolution of ID and lookup fields.
    /// </summary>
    public class Key
    {
        private const char alternateKeyDelimiter = ':';
        private const char equalsDelimiter = '=';
        private const char pairDelimiter = ';';

        #region Properties

        /// <summary>
        /// Gets or sets the original lookup string.
        /// </summary>
        [XmlAttribute]
        public string OriginalString { get; set; }

        /// <summary>
        /// Gets or sets the logical name of the alternate key to be used.
        /// </summary>
        [XmlAttribute]
        public string AlternateKeyLogicalName { get; set; }

        /// <summary>
        /// Gets or sets the field values being looked up.
        /// </summary>
        public List<Field> Fields { get; set; }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Key class.
        /// </summary>
        public Key()
        {
            Fields = new List<Field>();
        }

        /// <summary>
        /// Converts the string representation of a key into a Key. A return value indicates whether the conversion succeeded or failed.
        /// </summary>
        /// <param name="keyString">The string representation of the key to convert.</param>
        /// <param name="entityDefinition">The entity definition.</param>
        /// <param name="result">The resulting key if the conversion succeeded, otherwise null.</param>
        /// <returns>True if the conversion succeeded, false otherwise.</returns>
        /// <remarks>
        /// Keys must be in the form:
        /// new_KeyName:new_field1=field1value;new_field2=field2value
        /// </remarks>
        public static bool TryParse(string keyString, RetrieveEntityResponse entityDefinition, out Key result, Connection connection)
        {
            bool success = false;
            result = new Key();
            result.OriginalString = keyString;

            // strip the entity if it exists
            if (!string.IsNullOrEmpty(keyString) && keyString.Contains(alternateKeyDelimiter) && keyString.Contains(equalsDelimiter))
            {
                result.AlternateKeyLogicalName = keyString.Substring(0, keyString.IndexOf(alternateKeyDelimiter));
                string[] pairs = keyString.Substring(keyString.IndexOf(alternateKeyDelimiter) + alternateKeyDelimiter.ToString().Length).Split(pairDelimiter);

                foreach (string pair in pairs)
                {
                    if (pair != string.Empty)
                    {
                        string[] fieldValues = pair.Split(equalsDelimiter);

                        Field field = new Field();
                        field.LogicalName = fieldValues[0].ToLower();

                        AttributeMetadata keyField = entityDefinition.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName == field.LogicalName);

                        if (keyField == null)
                        {
                            throw new ApplicationException(string.Format(ModelStrings.FieldDoesNotExistText, field.LogicalName, entityDefinition.EntityMetadata.LogicalName));
                        }
                        else if (keyField.AttributeType == AttributeTypeCode.Lookup)
                        {
                            Lookup lookup = null;

                            if (Lookup.TryParse(fieldValues[1], keyField, out lookup, connection))
                            {
                                field.Value = lookup;
                            }
                        }
                        else
                        {
                            field.Value = fieldValues[1];
                        }

                        result.Fields.Add(field);
                    }
                }

                success = true;
            }

            if (!success)
            {
                result = null;
            }

            return success;
        }
    }
}
