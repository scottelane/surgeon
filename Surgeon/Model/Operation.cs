﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using ScottLane.Surgeon.Model.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// An operation that is performed on a CRM instance.
    /// </summary>
    public class Operation
    {
        #region Properties

        /// <summary>
        /// Gets or sets the logical name of the entity associated with the operation.
        /// </summary>
        [XmlAttribute]
        public string EntityLogicalName { get; set; }

        /// <summary>
        /// Gets or sets the error handling behaviour.
        /// </summary>
        [XmlAttribute]
        public ErrorBehaviour ErrorBehaviour { get; set; }

        /// <summary>
        /// Gets or sets the ID associated with the operation.
        /// </summary>
        [XmlElement]
        public Lookup Id { get; set; }

        /// <summary>
        /// Gets or sets the operation that the operation will perform.
        /// </summary>
        [XmlAttribute]
        public OperationType OperationType { get; set; }

        /// <summary>
        /// Gets or sets the alternate key details associated with the Upsert operation.
        /// </summary>
        [XmlElement]
        public Key Key { get; set; }

        /// <summary>
        /// Gets or sets the ID of the related record associated with the Associate operation.
        /// </summary>
        [XmlElement]
        public Lookup RelatedId { get; set; }

        /// <summary>
        /// Gets or sets the logical name of the related entity associated with the Associate operation.
        /// </summary>
        [XmlElement]
        public string RelatedEntityLogicalName { get; set; }

        /// <summary>
        /// Gets or sets the relationship schema name used by Associate operations.
        /// </summary>
        [XmlElement]
        public string RelationshipSchemaName { get; set; }

        /// <summary>
        /// Gets or sets the state used by the SetState operation.
        /// </summary>
        [XmlElement]
        public State State { get; set; }

        /// <summary>
        /// Gets or sets the Team associated with an Assign operation.
        /// </summary>
        [XmlElement]
        public Lookup Team { get; set; }

        /// <summary>
        /// Gets or sets the User ID associated with an Assign operation.
        /// </summary>
        [XmlElement]
        public Lookup User { get; set; }

        /// <summary>
        /// Gets or sets the field updated or created by the operation.
        /// </summary>
        public List<Field> Fields { get; set; }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Operation class.
        /// </summary>
        public Operation()
        {
            Fields = new List<Field>();
        }

        /// <summary>
        /// Validates the operation to ensure all required fields have been included and the entity and fields being updated exist in CRM.
        /// Also sets default values for operation types that have optional parameters.
        /// </summary>
        /// <param name="connection">The CRM connection to validate against.</param>
        public void Validate(Connection connection)
        {
            if (OperationType == OperationType.Unspecified)
            {
                throw new ValidationException(ModelStrings.OperationNotSpecifiedText);
            }

            if (OperationType == OperationType.Invalid)
            {
                throw new ValidationException(ModelStrings.OperationInvalidText);
            }

            if (ErrorBehaviour == ErrorBehaviour.Invalid)
            {
                throw new ValidationException(ModelStrings.ErrorBehaviourInvalidText);
            }

            if (OperationType != OperationType.PublishAll && EntityLogicalName == null)
            {
                throw new ValidationException(ModelStrings.EntityNotSpecifiedText);
            }

            if ((OperationType == OperationType.Activate
                || OperationType == OperationType.Assign
                || OperationType == OperationType.Associate
                || OperationType == OperationType.Deactivate
                || OperationType == OperationType.Delete
                || OperationType == OperationType.Disassociate
                || OperationType == OperationType.Update
                || OperationType == OperationType.SetState)
                && (Id == null))
            {
                throw new ValidationException(string.Format(ModelStrings.IdNotSpecifiedText, OperationType));
            }

            if (OperationType == OperationType.Assign)
            {
                if (User == null && Team == null)
                {
                    throw new ValidationException(ModelStrings.UserOrTeamNotSpecifiedText);
                }
            }

            if (OperationType == OperationType.Associate
                || OperationType == OperationType.Disassociate)
            {
                if (RelatedId == null)
                {
                    throw new ValidationException(ModelStrings.RelatedIdNotSpecifiedText);
                }

                if (RelatedEntityLogicalName == null)
                {
                    throw new ValidationException(ModelStrings.RelatedEntityNotSpecifiedText);
                }

                if (RelationshipSchemaName == null)
                {
                    throw new ValidationException(ModelStrings.RelationshipNotSpecifiedText);
                }
            }

            if (EntityLogicalName != null)
            {
                // retrieve entity metadata from CRM for operations that are valid thus far
                RetrieveEntityResponse response = EntityCache.GetEntityDefinition(EntityLogicalName, connection);

                // for updates and creates, convert all field values to the correct data type
                if (OperationType == OperationType.Create || OperationType == OperationType.Update || OperationType == OperationType.Upsert)
                {
                    foreach (Field field in Fields)
                    {
                        AttributeMetadata fieldMetadata = response.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName.ToLower() == field.LogicalName.ToLower());

                        if (fieldMetadata != null)
                        {
                            field.Value = GetCrmFieldValue(response, fieldMetadata, field.Value, connection);
                        }
                        else
                        {
                            throw new ValidationException(string.Format(ModelStrings.FieldDoesNotExistText, field.LogicalName, EntityLogicalName));
                        }
                    }

                    if (OperationType == OperationType.Upsert)
                    {
                        if (Id == null && Key == null)
                        {
                            throw new ValidationException(ModelStrings.IdOrKeyNotSpecifiedText);
                        }
                        else if (Key != null)
                        {
                            foreach (Field field in Key.Fields)
                            {
                                AttributeMetadata fieldMetadata = response.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName.ToLower() == field.LogicalName.ToLower());

                                if (fieldMetadata != null)
                                {
                                    field.Value = GetCrmFieldValue(response, fieldMetadata, field.Value, connection);
                                }
                                else
                                {
                                    throw new ValidationException(string.Format(ModelStrings.FieldDoesNotExistText, field.LogicalName, EntityLogicalName));
                                }
                            }
                        }
                    }
                }
                else if (OperationType == OperationType.Assign)
                {
                    if (response.EntityMetadata.OwnershipType != OwnershipTypes.UserOwned
                     && response.EntityMetadata.OwnershipType != OwnershipTypes.TeamOwned)
                    {
                        throw new ValidationException(ModelStrings.AssignOwnershipInvalidText);
                    }
                }
                else if (OperationType == OperationType.Activate)
                {
                    State result = null;

                    if (State.TryParse(ModelStrings.ActiveState, ModelStrings.ActiveStatus, response.EntityMetadata, out result))
                    {
                        State = result;
                    }
                    else
                    {
                        throw new ValidationException(string.Format(ModelStrings.ActivateNotSupportedText, EntityLogicalName));
                    }
                }
                else if (OperationType == OperationType.Deactivate)
                {
                    State result = null;

                    if (State.TryParse(ModelStrings.InactiveState, ModelStrings.InactiveStatus, response.EntityMetadata, out result))
                    {
                        State = result;
                    }
                    else
                    {
                        throw new ValidationException(string.Format(ModelStrings.DeactivateNotSupportedText, EntityLogicalName));
                    }
                }
                else if (OperationType == OperationType.SetState && State == null)
                {
                    throw new ValidationException(ModelStrings.StateOrStatusInvalidText);
                }
            }
        }

        /// <summary>
        /// Performs the operation on the specified CRM connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public void Perform(Connection connection)
        {
            if (Id != null)
            {
                // resolve the id lookup to get the record guid
                Id.Resolve(connection, OperationType != OperationType.Upsert);
            }

            if (RelatedId != null)
            {
                // resolve the related id lookup to get the related record guid
                RelatedId.Resolve(connection);
            }

            if (OperationType == OperationType.Delete)
            {
                // delete the record from CRM
                connection.Service.Delete(EntityLogicalName, Id.Guid);
            }
            else if (OperationType == OperationType.Activate
                || OperationType == OperationType.Deactivate
                || OperationType == OperationType.SetState)
            {
                if (EntityLogicalName == EntityName.Case && State.StateCode == StateCode.CaseResolved)
                {
                    CloseIncidentRequest request = new CloseIncidentRequest();
                    Entity resolution = new Entity(EntityName.CaseResolution);
                    resolution["incidentid"] = new EntityReference(EntityLogicalName, Id.Guid);
                    resolution["statecode"] = new OptionSetValue(State.StateCode);
                    request.IncidentResolution = resolution;
                    request.Status = new OptionSetValue(State.StatusCode);
                    connection.Service.Execute(request);
                }
                else if (EntityLogicalName == EntityName.Quote && State.StateCode == StateCode.QuoteDraft)
                {
                    ReviseQuoteRequest request = new ReviseQuoteRequest();
                    request.QuoteId = Id.Guid;
                    request.ColumnSet = new ColumnSet(true);
                    connection.Service.Execute(request);
                }
                else if (EntityLogicalName == EntityName.Quote && State.StateCode == StateCode.QuoteWon)
                {
                    WinQuoteRequest request = new WinQuoteRequest();
                    Entity quoteClose = new Entity(EntityName.QuoteClose);
                    quoteClose["quoteid"] = new EntityReference(EntityLogicalName, Id.Guid);
                    quoteClose["statecode"] = new OptionSetValue(State.StateCode);
                    request.QuoteClose = quoteClose;
                    request.Status = new OptionSetValue(State.StatusCode);
                    connection.Service.Execute(request);
                }
                else if (EntityLogicalName == EntityName.Quote && State.StateCode == StateCode.QuoteClosed)
                {
                    CloseQuoteRequest request = new CloseQuoteRequest();
                    Entity quoteClose = new Entity(EntityName.QuoteClose);
                    quoteClose["quoteid"] = new EntityReference(EntityLogicalName, Id.Guid);
                    quoteClose["statecode"] = new OptionSetValue(State.StateCode);
                    request.QuoteClose = quoteClose;
                    request.Status = new OptionSetValue(State.StatusCode);
                    connection.Service.Execute(request);
                }
                else
                {
                    SetStateRequest request = new SetStateRequest();
                    request.EntityMoniker = new EntityReference(EntityLogicalName, Id.Guid);
                    request.State = new OptionSetValue(State.StateCode);
                    request.Status = new OptionSetValue(State.StatusCode);
                    connection.Service.Execute(request);
                }
            }
            else if (OperationType == OperationType.Associate)
            {
                // associate the records
                AssociateRequest request = new AssociateRequest();
                request.Target = new EntityReference(EntityLogicalName, Id.Guid);
                EntityReferenceCollection relatedEntities = new EntityReferenceCollection();
                relatedEntities.Add(new EntityReference(RelatedEntityLogicalName, RelatedId.Guid));
                request.RelatedEntities = relatedEntities;
                request.Relationship = new Relationship(RelationshipSchemaName);
                connection.Service.Execute(request);
            }
            else if (OperationType == OperationType.Disassociate)
            {
                // disassociate the records
                DisassociateRequest request = new DisassociateRequest();
                request.Target = new EntityReference(EntityLogicalName, Id.Guid);
                EntityReferenceCollection relatedEntities = new EntityReferenceCollection();
                relatedEntities.Add(new EntityReference(RelatedEntityLogicalName, RelatedId.Guid));
                request.RelatedEntities = relatedEntities;
                request.Relationship = new Relationship(RelationshipSchemaName);
                connection.Service.Execute(request);
            }
            else if (OperationType == OperationType.Create || OperationType == OperationType.Update || OperationType == OperationType.Upsert)
            {
                Entity record = new Entity(EntityLogicalName);

                foreach (Field field in Fields)
                {
                    // set each field value and resolve lookups
                    if (field.Value != null && field.Value.GetType() == typeof(Lookup))
                    {
                        record[field.LogicalName] = ((Lookup)field.Value).Resolve(connection);
                    }
                    else
                    {
                        record[field.LogicalName] = field.Value;
                    }
                }

                if (OperationType == OperationType.Update)
                {
                    // update the record in CRM
                    record.Id = Id.Guid;
                    connection.Service.Update(record);
                }
                else if (OperationType == OperationType.Create)
                {
                    // create the record in CRM
                    connection.Service.Create(record);
                }
                else if (OperationType == OperationType.Upsert)
                {
                    // ensure the correct primary or alternate keys are applied
                    if (Id != null)
                    {
                        if (Id.Guid != null && Id.Guid != Guid.Empty)
                        {
                            record.KeyAttributes.Add(Id.PrimaryIdLogicalName, Id.Guid);
                        }
                    }
                    else if (Key != null)
                    {
                        foreach (Field field in Key.Fields)
                        {
                            record.KeyAttributes.Add(field.LogicalName, field.Value);
                        }
                    }

                    // upsert the record in CRM
                    UpsertRequest request = new UpsertRequest();
                    request.Target = record;
                    connection.Service.Execute(request);
                }
            }
            else if (OperationType == OperationType.Publish)
            {
                // publish an entity
                PublishXmlRequest request = new PublishXmlRequest();
                request.ParameterXml = string.Format("<importexportxml><entities><entity>{0}</entity></entities></importexportxml>", EntityLogicalName);
                connection.Service.Execute(request);
            }
            else if (OperationType == OperationType.PublishAll)
            {
                // publish all customisations
                connection.PublishAll();
            }
            else if (OperationType == OperationType.Assign)
            {
                // assign a record to a user
                AssignRequest request = new AssignRequest();
                request.Assignee = User != null ? User.Resolve(connection) : Team.Resolve(connection);
                request.Target = new EntityReference(EntityLogicalName, Id.Guid);
                connection.Service.Execute(request);
            }
        }

        /// <summary>
        /// Converts a field value into the correct data type based on the field definition.
        /// </summary>
        /// <param name="entityDefinition">The entity definition.</param>
        /// <param name="fieldDefinition">The field definition.</param>
        /// <param name="value">The field value to convert.</param>
        /// <returns>A field value converted into a form that can be loaded into CRM.</returns>
        public static object GetCrmFieldValue(RetrieveEntityResponse entityDefinition, AttributeMetadata fieldDefinition, object value, Connection connection)
        {
            const string nullString = "(null)";
            object fieldValue = value;

            // handle nulls
            if (DBNull.Value.Equals(value))
            {
                fieldValue = null;
            }
            else if (value != null && value.ToString().ToLower() == nullString)
            {
                fieldValue = null;
            }

            // handle non-null fields
            if (fieldValue != null)
            {
                // handle id's
                if ((bool)fieldDefinition.IsPrimaryId)
                {
                    fieldValue = new Guid(fieldValue.ToString());
                }
                // handle option sets
                else if (fieldDefinition.AttributeType == AttributeTypeCode.State || fieldDefinition.AttributeType == AttributeTypeCode.Status || fieldDefinition.AttributeType == AttributeTypeCode.Picklist)
                {
                    int optionSetValue = 0;

                    if (fieldValue is string)
                    {
                        bool success = int.TryParse(fieldValue.ToString(), out optionSetValue);

                        if (!success)
                        {
                            optionSetValue = (int)((PicklistAttributeMetadata)fieldDefinition).OptionSet.Options.First(option => option.Label.UserLocalizedLabel.Label.ToLower() == fieldValue.ToString().ToLower()).Value;
                        }
                    }
                    else
                    {
                        optionSetValue = Convert.ToInt32(fieldValue);
                    }

                    fieldValue = new OptionSetValue(optionSetValue);
                }
                // handle lookups
                else if ((fieldDefinition.AttributeType == AttributeTypeCode.Lookup
                    || fieldDefinition.AttributeType == AttributeTypeCode.Customer)
                    && !(value is Lookup))
                {
                    string lookupString = fieldValue.ToString();
                    Lookup lookup = null;

                    if (Lookup.TryParse(lookupString, fieldDefinition, out lookup, connection))
                    {
                        fieldValue = lookup;
                    }
                    else
                    {
                        throw new ValidationException(string.Format(ModelStrings.LookupInvalidText, lookupString, fieldDefinition.LogicalName));
                    }
                }
                // handle booleans
                else if (fieldDefinition.AttributeType == AttributeTypeCode.Boolean)
                {
                    bool booleanValue;

                    if (bool.TryParse(fieldValue.ToString(), out booleanValue))
                    {
                        fieldValue = booleanValue;
                    }
                    else
                    {
                        BooleanAttributeMetadata booleanField = (BooleanAttributeMetadata)fieldDefinition;
                        int integerValue;

                        if (int.TryParse(fieldValue.ToString(), out integerValue))
                        {
                            if (integerValue == booleanField.OptionSet.TrueOption.Value)
                            {
                                fieldValue = true;
                            }
                            else if (integerValue == booleanField.OptionSet.FalseOption.Value)
                            {
                                fieldValue = false;
                            }
                        }
                        else if (fieldValue.ToString().ToLower() == booleanField.OptionSet.TrueOption.Label.UserLocalizedLabel.Label.ToString().ToLower())
                        {
                            fieldValue = true;
                        }
                        else if (fieldValue.ToString().ToLower() == booleanField.OptionSet.FalseOption.Label.UserLocalizedLabel.Label.ToString().ToLower())
                        {
                            fieldValue = false;
                        }
                    }
                }
                // handle dates and date/times
                else if (fieldDefinition.AttributeType == AttributeTypeCode.DateTime)
                {
                    fieldValue = Convert.ToDateTime(fieldValue);
                }
                // handle money
                else if (fieldDefinition.AttributeType == AttributeTypeCode.Money)
                {
                    fieldValue = new Money(Convert.ToDecimal(fieldValue));
                }
                // handle decimals
                else if (fieldDefinition.AttributeType == AttributeTypeCode.Decimal)
                {
                    fieldValue = Convert.ToDecimal(fieldValue);
                }
                // handle whole numbers
                else if (fieldDefinition.AttributeType == AttributeTypeCode.Integer)
                {
                    fieldValue = Convert.ToInt32(fieldValue);
                }
                // handle strings
                else if (fieldDefinition.AttributeType == AttributeTypeCode.String)
                {
                    fieldValue = fieldValue.ToString();
                }
            }

            return fieldValue;
        }
    }
}
