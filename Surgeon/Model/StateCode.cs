﻿namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Specifies state codes used by CRM entities.
    /// </summary>
    public class StateCode
    {
        public static readonly int CaseResolved = 1;
        public static readonly int QuoteDraft = 0;
        public static readonly int QuoteWon = 2;
        public static readonly int QuoteClosed = 3;
    }
}
