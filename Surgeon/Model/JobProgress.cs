﻿namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Job import and execution progress.
    /// </summary>
    public class JobProgress
    {
        /// <summary>
        /// The current operation index.
        /// </summary>
        public int OperationIndex { get; set; }

        /// <summary>
        /// The operation count.
        /// </summary>
        public int OperationCount { get; set; }

        /// <summary>
        /// Initialises a new instance of the BatchProgress class with the specified operation index and count.
        /// </summary>
        /// <param name="operationIndex">The current operation index.</param>
        /// <param name="operationCount">The operation count.</param>
        public JobProgress(int operationIndex, int operationCount)
        {
            OperationIndex = operationIndex;
            OperationCount = operationCount;
        }
    }
}
