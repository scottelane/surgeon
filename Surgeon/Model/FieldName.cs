﻿namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Special field names used by operations to control core behaviour.
    /// </summary>
    public class FieldName
    {
        /// <summary>
        /// The prefix of fields that will be ignored.
        /// </summary>
        public static readonly string IgnorePrefix = "#";

        /// <summary>
        /// The error behaviour.
        /// </summary>
        public static readonly string ErrorBehaviour = "#Errors";

        /// <summary>
        /// The logical name of the CRM entity involved in the operation. Required for Activate, Assign, Associate, Create, Deactivate, Delete, Disassociate, Publish and Update operations.
        /// </summary>
        public static readonly string EntityLogicalName = "#Entity";

        /// <summary>
        /// The guid of the record being affected or a lookup to the guid of the record being affected by Activate, Assign, Associate, Deactivate, Delete, Disassociate and Update operations.
        /// </summary>
        public static readonly string RecordId = "#ID";

        /// <summary>
        /// The operation to be performed. See the <see cref="OperationType" /> enumeration for valid types. Required for all operations.
        /// </summary>
        public static readonly string OperationType = "#Operation";

        /// <summary>
        /// The alternate key to use for Upsert operations. Optional for the Upsert operation.
        /// </summary>
        public static readonly string Key = "#Key";

        /// <summary>
        /// The guid of the user to assign ownership of the record to. Optional for the Assign operation.
        /// </summary>
        public static readonly string User = "#User";

        /// <summary>
        /// The schema name of the related entity to associate. Required for Associate and Disassociate operations.
        /// </summary>
        public static readonly string RelatedEntityLogicalName = "#RelatedEntity";

        /// <summary>
        /// The guid of the user to assign ownership of the record to. Required for Associate and Disassociate operations.
        /// </summary>
        public static readonly string RelatedRecordId = "#RelatedID";

        /// <summary>
        /// Relationship. Required for Associate and Disassociate operations.
        /// </summary>
        public static readonly string Relationship = "#Relationship";

        /// <summary>
        /// State. Optional for Activate and Deactivate operations.
        /// </summary>
        public static readonly string State = "#State";

        /// <summary>
        /// Status. Optional for Activate and Deactivate operations.
        /// </summary>
        public static readonly string Status = "#Status";

        /// <summary>
        /// The guid of the team to assign ownership of the record to. Optional for the Assign operation.
        /// </summary>
        public static readonly string Team = "#Team";
    }
}
