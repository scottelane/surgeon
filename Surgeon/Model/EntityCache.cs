﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// A cache of entity metadata objects.
    /// </summary>
    public class EntityCache
    {
        private static List<RetrieveEntityResponse> entities;

        /// <summary>
        /// Initialises a static instance of the EntityCache class.
        /// </summary>
        static EntityCache()
        {
            entities = new List<RetrieveEntityResponse>();
        }

        /// <summary>
        /// Gets the entity definition from the cache if it exists, otherwise gets the entity definition from CRM.
        /// </summary>
        /// <param name="entityLogicalName">The entity logical name.</param>
        /// <param name="connection">The connection.</param>
        /// <returns>The entity definition.</returns>
        public static RetrieveEntityResponse GetEntityDefinition(string entityLogicalName, Connection connection)
        {
            RetrieveEntityResponse entityDefinition = entities.Find(entity => entity.EntityMetadata.LogicalName.ToLower() == entityLogicalName.ToLower());

            if (entityDefinition == null)
            {
                RetrieveEntityRequest request = new RetrieveEntityRequest();
                request.EntityFilters = EntityFilters.Attributes;
                request.LogicalName = entityLogicalName;
                entityDefinition = (RetrieveEntityResponse)connection.Service.Execute(request);
                entities.Add(entityDefinition);
            }

            return entityDefinition;
        }
    }
}
