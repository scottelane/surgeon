﻿using System;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Microsoft.Xrm.Sdk;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// A CRM entity field that may be used by a lookup or an update or create operation.
    /// </summary>
    public sealed class Field : IXmlSerializable
    {
        private const string fieldElement = "Field";
        private const string logicalNameAttribute = "LogicalName";
        private const string valueAttribute = "Value";
        private const string valueTypeAttribute = "ValueType";

        /// <summary>
        /// Gets or sets the logical name.
        /// </summary>
        public string LogicalName { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the Field class.
        /// </summary>
        public Field()
        {
        }

        /// <summary>
        /// Reads the field details from an XmlReader.
        /// </summary>
        /// <param name="reader">The xml reader.</param>
        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            LogicalName = reader[logicalNameAttribute];
            Type valueType = Type.GetType(reader[valueTypeAttribute]);

            if (valueType == typeof(Lookup))
            {
                XmlSerializer lookupSerializer = new XmlSerializer(valueType);
                reader.ReadToDescendant("Lookup");
                Value = lookupSerializer.Deserialize(reader);
            }
            else
            {
                string valueString = reader[valueAttribute];

                // todo - test for nulls?
                if (valueType == typeof(OptionSetValue))
                {
                    Value = new OptionSetValue(int.Parse(valueString));
                }
                else if (valueType == typeof(string))
                {
                    Value = valueString;
                }
                else
                {
                    Value = Activator.CreateInstance(valueType);
                    PropertyInfo property = GetType().GetProperty("Value");

                    if (valueType == typeof(Guid))
                    {
                        Value = Guid.Parse(valueString);
                    }
                    else if (valueType == typeof(Money))
                    {
                        Value = new Money(Convert.ToDecimal(valueString));
                    }
                    else
                    {
                        property.SetValue(this, Convert.ChangeType(valueString, valueType));
                    }
                }
            }

            reader.Read();
        }

        /// <summary>
        /// Writes the field details with an XmlWriter.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString(logicalNameAttribute, LogicalName.ToString());

            if (Value != null)
            {
                Type valueType = Value.GetType();
                writer.WriteAttributeString(valueTypeAttribute, valueType.AssemblyQualifiedName);

                if (valueType == typeof(Lookup))
                {
                    XmlSerializer lookupSerializer = new XmlSerializer(valueType);
                    lookupSerializer.Serialize(writer, Value);
                }
                else
                {
                    string valueString = Value.ToString();

                    if (valueType == typeof(OptionSetValue))
                    {
                        valueString = ((OptionSetValue)Value).Value.ToString();
                    }
                    else if (valueType == typeof(Money))
                    {
                        valueString = ((Money)Value).Value.ToString();
                    }

                    writer.WriteAttributeString(valueAttribute, valueString);
                }
            }
            else
            {
                writer.WriteAttributeString(valueTypeAttribute, typeof(object).AssemblyQualifiedName);
            }
        }

        /// <summary>
        /// Gets the serialization schema.
        /// </summary>
        /// <returns>Schema.</returns>
        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }
    }
}
