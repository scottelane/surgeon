﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ScottLane.Surgeon.Model.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// A batch job sourced from a Microsoft Excel spreadsheet.
    /// </summary>
    /// <remarks>
    /// http://fczaja.blogspot.com.au/2013/05/how-to-read-and-write-excel-cells-with.html
    /// http://stackoverflow.com/questions/3837981/reading-excel-open-xml-is-ignoring-blank-cells
    /// http://msdn.microsoft.com/en-us/library/office/cc822064(v=office.15).aspx
    /// http://openxmldeveloper.org/blog/b/openxmldeveloper/archive/2012/02/16/dates-in-spreadsheetml.aspx
    /// </remarks>
    public class ExcelJob : Job
    {
        Dictionary<string, string> headerCoordinates;
        SharedStringTable sharedStrings;

        /// <summary>
        /// Not required for Excel jobs.
        /// </summary>
        public override void LoadFileContents()
        {
        }

        /// <summary>
        /// Adds operations from a Microsoft Excel spreadsheet.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The job progress.</param>
        public override void AddOperations(Connection connection, CancellationToken cancel, Progress<JobProgress> progress)
        {
            try
            {
                ReportStatus(JobStatus.Creating, null, progress);
                Operations = new List<Operation>();

                // todo - fails if malformed uri exists. may need to do pre-processing with packaging api
                //https://social.msdn.microsoft.com/Forums/office/en-US/8147fa29-6e55-4f4e-9d60-c07ea3779042/how-to-validate-a-word-document-using-openxml-sdk-25
                SpreadsheetDocument document = SpreadsheetDocument.Open(FileName, false);
                WorkbookPart workbookPart = document.WorkbookPart;
                SharedStringTablePart sharedStringTablePart = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                sharedStrings = sharedStringTablePart != null ? sharedStringTablePart.SharedStringTable : null;

                int operationCount = GetOperationCount(workbookPart);
                int operationIndex = 0;

                foreach (Sheet sheet in workbookPart.Workbook.Sheets)
                {
                    WorksheetPart worksheetPart = workbookPart.GetPartById(sheet.Id) as WorksheetPart;
                    SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                    IEnumerable<Row> rows = sheetData.Elements<Row>();

                    if (rows.Count() > 0)
                    {
                        UpdateHeaderCoordinates(rows.First(), workbookPart);

                        for (int rowIndex = 0; rowIndex < rows.Skip(1).Count() && !cancel.IsCancellationRequested; rowIndex++)
                        {
                            Row row = rows.Skip(1).ElementAt(rowIndex);
                            Operation operation = ReadOperation(row, connection, workbookPart);
                            Operations.Add(operation);
                            operationIndex++;
                            //ReportStatus(JobStatus.Creating, new JobProgress(operationIndex, operationCount), progress);
                        }
                    }
                }

                document.Close();
                ReportStatus(JobStatus.Completed, null, progress);
            }
            catch
            {
                ReportStatus(JobStatus.Failed, null, progress);
                throw;
            }
        }

        /// <summary>
        /// Gets the total number of operations in a workbook.
        /// </summary>
        /// <param name="workbookPart">The workbook.</param>
        /// <returns>The operation count.</returns>
        private int GetOperationCount(WorkbookPart workbookPart)
        {
            int operationCount = 0;

            foreach (WorksheetPart worksheetPart in workbookPart.WorksheetParts)
            {
                SheetData data = worksheetPart.Worksheet.Elements<SheetData>().First();
                int rows = data.Elements<Row>().Count();
                operationCount += rows > 1 ? rows - 1 : 0;
            }

            return operationCount;
        }

        /// <summary>
        /// Updates the coordinates of header fields.
        /// </summary>
        /// <param name="headerRow">The header row containing field names.</param>
        private void UpdateHeaderCoordinates(Row headerRow, WorkbookPart workbookPart)
        {
            headerCoordinates = new Dictionary<string, string>();
            IEnumerable<Cell> cells = headerRow.Descendants<Cell>();

            foreach (Cell cell in cells)
            {
                // todo - how to handle comment columns? get rid of that concept?
                string columnName = GetCellValue(cell, workbookPart).ToString();

                if (!headerCoordinates.ContainsKey(columnName))
                {
                    headerCoordinates.Add(columnName, cell.CellReference);
                }
                else
                {
                    throw new ApplicationException(string.Format(ModelStrings.DuplicateHeaderErrorMessage, columnName));
                }
            }
        }

        /// <summary>
        /// Reads an operation from a worksheet row.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="connection">The connection.</param>
        /// <returns>The operation.</returns>
        private Operation ReadOperation(Row row, Connection connection, WorkbookPart workbookPart)
        {
            Operation operation = new Operation();

            if (headerCoordinates.ContainsKey(FieldName.OperationType))
            {
                OperationType operationType = OperationType.Unspecified;
                object value = GetCellValueFromColumnName(row, FieldName.OperationType, workbookPart);

                if (value != default(object))
                {
                    if (!Enum.TryParse(value.ToString(), true, out operationType))
                    {
                        operationType = OperationType.Invalid;
                    }
                }

                operation.OperationType = operationType;
            }

            if (headerCoordinates.ContainsKey(FieldName.EntityLogicalName))
            {
                operation.EntityLogicalName = GetCellValueFromColumnName(row, FieldName.EntityLogicalName, workbookPart).ToString();
            }

            if (headerCoordinates.ContainsKey(FieldName.RecordId) && operation.EntityLogicalName != null)
            {
                RetrieveEntityResponse response = EntityCache.GetEntityDefinition(operation.EntityLogicalName, connection);
                AttributeMetadata fieldMetadata = response.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName.ToLower() == response.EntityMetadata.PrimaryIdAttribute.ToLower());

                string recordId = GetCellValueFromColumnName(row, FieldName.RecordId, workbookPart).ToString();
                Lookup result = null;
                Lookup.TryParse(recordId, fieldMetadata, out result, connection);
                operation.Id = result;
            }

            if (headerCoordinates.ContainsKey(FieldName.Key) && operation.EntityLogicalName != null)
            {
                RetrieveEntityResponse response = EntityCache.GetEntityDefinition(operation.EntityLogicalName, connection);
                string key = GetCellValueFromColumnName(row, FieldName.Key, workbookPart).ToString();
                Key result = null;
                Key.TryParse(key, response, out result, connection);
                operation.Key = result;
            }

            if (headerCoordinates.ContainsKey(FieldName.User))
            {
                RetrieveEntityResponse response = EntityCache.GetEntityDefinition(EntityName.User, connection);
                AttributeMetadata fieldMetadata = response.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName == "systemuserid");

                string recordId = GetCellValueFromColumnName(row, FieldName.User, workbookPart).ToString();
                Lookup result = null;
                Lookup.TryParse(recordId, fieldMetadata, out result, connection);
                operation.User = result;
            }

            if (headerCoordinates.ContainsKey(FieldName.Team))
            {
                RetrieveEntityResponse response = EntityCache.GetEntityDefinition(EntityName.Team, connection);
                AttributeMetadata fieldMetadata = response.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName == "teamid");

                string recordId = GetCellValueFromColumnName(row, FieldName.Team, workbookPart).ToString();
                Lookup result = null;
                Lookup.TryParse(recordId, fieldMetadata, out result, connection);
                operation.Team = result;
            }

            if (headerCoordinates.ContainsKey(FieldName.RelatedEntityLogicalName))
            {
                operation.RelatedEntityLogicalName = GetCellValueFromColumnName(row, FieldName.RelatedEntityLogicalName, workbookPart).ToString();
            }

            if (headerCoordinates.ContainsKey(FieldName.Relationship))
            {
                operation.RelationshipSchemaName = GetCellValueFromColumnName(row, FieldName.Relationship, workbookPart).ToString();
            }

            if (headerCoordinates.ContainsKey(FieldName.RelatedRecordId) && operation.RelatedEntityLogicalName != null)
            {
                RetrieveEntityResponse response = EntityCache.GetEntityDefinition(operation.RelatedEntityLogicalName, connection);
                AttributeMetadata fieldMetadata = response.EntityMetadata.Attributes.FirstOrDefault(findField => findField.LogicalName.ToLower() == response.EntityMetadata.PrimaryIdAttribute.ToLower());

                string relatedId = GetCellValueFromColumnName(row, FieldName.RelatedRecordId, workbookPart).ToString();
                Lookup result = null;
                Lookup.TryParse(relatedId, fieldMetadata, out result, connection);
                operation.RelatedId = result;
            }

            if (headerCoordinates.ContainsKey(FieldName.State) && headerCoordinates.ContainsKey(FieldName.Status) && operation.EntityLogicalName != null)
            {
                RetrieveEntityResponse response = EntityCache.GetEntityDefinition(operation.EntityLogicalName, connection);
                State state = null;
                object stateValue = GetCellValueFromColumnName(row, FieldName.State, workbookPart);
                object statusValue = GetCellValueFromColumnName(row, FieldName.Status, workbookPart);
                State.TryParse(stateValue, statusValue, response.EntityMetadata, out state);
                operation.State = state;
            }

            if (headerCoordinates.ContainsKey(FieldName.ErrorBehaviour))
            {
                ErrorBehaviour behaviour = ErrorBehaviour.Unspecified;
                object value = GetCellValueFromColumnName(row, FieldName.ErrorBehaviour, workbookPart);

                if (value != default(object))
                {
                    if (!Enum.TryParse(value.ToString(), true, out behaviour))
                    {
                        behaviour = ErrorBehaviour.Invalid;
                    }
                }

                operation.ErrorBehaviour = behaviour;
            }

            foreach (KeyValuePair<string, string> pair in headerCoordinates)
            {
                // ignore any predefined column
                if (pair.Key != FieldName.EntityLogicalName &&
                    pair.Key != FieldName.ErrorBehaviour &&
                    pair.Key != FieldName.RecordId &&
                    pair.Key != FieldName.OperationType &&
                    pair.Key != FieldName.User &&
                    pair.Key != FieldName.Team &&
                    pair.Key != FieldName.RelatedEntityLogicalName &&
                    pair.Key != FieldName.RelatedRecordId &&
                    pair.Key != FieldName.Relationship &&
                    pair.Key != FieldName.State &&
                    pair.Key != FieldName.Status &&
                    !pair.Key.StartsWith(FieldName.IgnorePrefix))
                {
                    // create a field for each non-predefined column
                    Field field = new Field();
                    field.LogicalName = pair.Key;
                    field.Value = GetCellValueFromColumnName(row, pair.Key, workbookPart);
                    operation.Fields.Add(field);
                }
            }

            return operation;
        }

        /// <summary>
        /// Gets a cell's value from the cell's row and column name
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="columnName">The column name.</param>
        /// <returns>The cell's value.</returns>
        private object GetCellValueFromColumnName(Row row, string columnName, WorkbookPart workbookPart)
        {
            object cellValue = null;
            string coordinates = headerCoordinates[columnName];
            string columnIndex = Regex.Match(coordinates, "[A-Za-z]+").Value;
            string cellCoordinates = string.Format("{0}{1}", columnIndex, row.RowIndex);

            Cell cell = row.Elements<Cell>().FirstOrDefault(c => cellCoordinates.Equals(c.CellReference.Value));

            if (cell != null)
            {
                cellValue = GetCellValue(cell, workbookPart);
            }

            return cellValue;
        }

        /// <summary>
        /// Gets a cell's value.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <returns>The cell's value.</returns>
        private object GetCellValue(Cell cell, WorkbookPart workbookPart)
        {
            object value = null;
            string stringValue = null;

            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                int ssid = int.Parse(cell.CellValue.Text);
                stringValue = sharedStrings.ChildElements[ssid].InnerText;
            }
            else if (cell.CellValue != null)
            {
                stringValue = cell.CellValue.Text;
            }

            DateTime dateValue;
            Decimal decimalValue;

            if (TryParseDate(cell, stringValue, out dateValue, workbookPart))
            {
                value = dateValue;
            }
            else if (TryParseDecimal(cell, stringValue, out decimalValue))
            {
                value = decimalValue;
            }
            else
            {
                value = stringValue;
            }

            return value;
        }

        /// <summary>
        /// Parses a cell for a date/time value and handles the conversion from the internal Excel date storage format.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <param name="valueString">The cell value as a string.</param>
        /// <param name="value">The resulting DateTime value.</param>
        /// <param name="workbookPart">The workbook.</param>
        /// <returns>True if the cell contains a date, false otherwise.</returns>
        /// <remarks>
        /// http://stackoverflow.com/questions/4730152/what-indicates-an-office-open-xml-cell-contains-a-date-time-value
        /// http://openxmldeveloper.org/blog/b/openxmldeveloper/archive/2012/02/16/dates-in-spreadsheetml.aspx
        /// </remarks>
        private bool TryParseDate(Cell cell, string valueString, out DateTime value, WorkbookPart workbookPart)
        {
            bool result = false;
            value = DateTime.MinValue;

            if (cell.StyleIndex != null)
            {
                Stylesheet stylesheet = workbookPart.WorkbookStylesPart.Stylesheet;
                CellFormat format = (CellFormat)stylesheet.CellFormats.ElementAt(Convert.ToInt32(cell.StyleIndex.Value));

                if (format.NumberFormatId == 14 ||
                    format.NumberFormatId == 15 ||
                    format.NumberFormatId == 16 ||
                    format.NumberFormatId == 17 ||
                    format.NumberFormatId == 18 ||
                    format.NumberFormatId == 19 ||
                    format.NumberFormatId == 20 ||
                    format.NumberFormatId == 21 ||
                    format.NumberFormatId == 22 ||
                    format.NumberFormatId == 49)
                {
                    double doubleDate;

                    if (double.TryParse(valueString, out doubleDate))
                    {
                        value = DateTime.FromOADate(doubleDate);
                        result = true;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Parses a cell for a decimal value and handles doubles with exponents.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <param name="valueString">The cell value as a string.</param>
        /// <param name="value">The resulting Decimal value.</param>
        /// <returns>True if the cell contains a decimal, false otherwise.</returns>
        /// <remarks>
        /// http://stackoverflow.com/questions/26123217/openxml-sdk-returns-scientific-values-for-large-numbers
        /// </remarks>
        private bool TryParseDecimal(Cell cell, string valueString, out Decimal value)
        {
            bool result = false;
            value = Decimal.MinValue;
            
            if (cell.StyleIndex != null)
            {
                if (Regex.IsMatch(valueString, @"^\d+\.\d*E[\+\-]\d+$", RegexOptions.IgnoreCase | RegexOptions.Compiled))
                {
                    double doubleValue;

                    if (double.TryParse(valueString, NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out doubleValue))
                    {
                        value = Convert.ToDecimal(doubleValue);
                        result = true;
                    }
                }
            }

            return result;
        }
    }
}