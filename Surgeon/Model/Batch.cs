﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using ScottLane.Surgeon.Model.Forms;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Runs jobs that perform operations on a CRM instance.
    /// </summary>
    public class Batch
    {
        #region Properties

        private DateTime startTime;

        /// <summary>
        /// Gets the execution start time.
        /// </summary>
        [XmlIgnore]
        public DateTime StartTime
        {
            get { return startTime; }
        }

        private DateTime finishTime;

        /// <summary>
        /// Gets the execution finish time.
        /// </summary>
        [XmlIgnore]
        public DateTime FinishTime
        {
            get { return finishTime; }
        }

        /// <summary>
        /// Gets the execution duration.
        /// </summary>
        [XmlIgnore]
        public TimeSpan Duration
        {
            get { return this.finishTime - this.startTime; }
        }

        /// <summary>
        /// Gets a human-readable duration.
        /// </summary>
        [XmlIgnore]
        public string DurationString
        {
            get
            {
                TimeSpan duration = Duration;
                string durationString = string.Empty;

                if (duration.TotalHours >= 1)
                {
                    int hours = Convert.ToInt32(Math.Round(duration.TotalHours, 0));
                    string hoursPlural = hours == 1 ? string.Empty : "s";
                    int minutes = Convert.ToInt32(Math.Round(duration.TotalMinutes % 60, 0));
                    string minutesPlural = minutes == 1 ? string.Empty : "s";

                    durationString = string.Format("{0} hour{1}, {2} minute{3}", hours, hoursPlural, minutes, minutesPlural);
                }
                else if (duration.TotalMinutes >= 1)
                {
                    int minutes = Convert.ToInt32(Math.Round(duration.TotalMinutes, 0));
                    string minutesPlural = minutes == 1 ? string.Empty : "s";
                    int seconds = Convert.ToInt32(Math.Round(duration.TotalSeconds % 60, 0));
                    string secondsPlural = seconds == 1 ? string.Empty : "s";

                    durationString = string.Format("{0} minute{1}, {2} second{3}", minutes, minutesPlural, seconds, secondsPlural);
                }
                else
                {
                    int seconds = Convert.ToInt32(Math.Round(duration.TotalSeconds, 0));
                    string secondsPlural = seconds == 1 ? string.Empty : "s";

                    durationString = string.Format("{0} second{1}", seconds, secondsPlural);
                }

                return durationString;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the batch contains valid batches.
        /// </summary>
        [XmlIgnore]
        public bool IsValid
        {
            get { return !Jobs.Exists(batch => batch.Status == JobStatus.Invalid); }
        }

        /// <summary>
        /// Gets the batch status.
        /// </summary>
        [XmlIgnore]
        public BatchStatus Status
        {
            get
            {
                if (Jobs.Exists(batch => batch.Status == JobStatus.Running) || Jobs.Exists(batch => batch.Status == JobStatus.Creating))
                {
                    return BatchStatus.Running;
                }

                if (Jobs.Exists(batch => batch.Status == JobStatus.Stopped))
                {
                    return BatchStatus.Stopped;
                }

                if (Jobs.Exists(batch => batch.Status == JobStatus.Failed))
                {
                    return BatchStatus.Failed;
                }

                if (Jobs.Exists(batch => batch.Status == JobStatus.Invalid))
                {
                    return BatchStatus.Invalid;
                }

                return BatchStatus.Ready;
            }
        }

        /// <summary>
        /// Gets or sets jobs run in the batch.
        /// </summary>
        public List<Job> Jobs { get; set; }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Batch class.
        /// </summary>
        public Batch()
        {
            Jobs = new List<Job>();
        }

        /// <summary>
        /// Runs the batch on the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="cancel">Indicates whether the batch has been cancelled.</param>
        /// <param name="batchProgress">The batch progress.</param>
        public void Run(Connection connection, CancellationToken cancel, Progress<JobProgress> batchProgress, ErrorDetailsForm form)
        {
            startTime = DateTime.Now;

            foreach (Job job in Jobs)
            {
                // reset the job status to allow re-run after an error occurred
                job.Status = JobStatus.Ready;
            }

            for (int jobIndex = 0; jobIndex < Jobs.Count && Status != BatchStatus.Failed && Status != BatchStatus.Invalid; jobIndex++)
            {
                Job job = Jobs[jobIndex];
                job.Run(connection, cancel, batchProgress, form);
            }

            finishTime = DateTime.Now;
        }

        /// <summary>
        /// Saves the batch to a file with the specified file name.
        /// </summary>
        /// <param name="fileName">The file name.</param>
        public void Save(string fileName)
        {
            // clear operations from any dynamics batch jobs before saving
            foreach (Job job in Jobs)
            {
                if (job.CompilationType == CompilationType.Dynamic)
                {
                    job.Operations.Clear();
                }
            }

            XmlSerializer serializer = new XmlSerializer(typeof(Batch));
            FileStream stream = new FileStream(fileName, FileMode.Create);
            serializer.Serialize(stream, this);
            stream.Close();
        }

        /// <summary>
        /// Loads a batch from a file with the specified file name.
        /// </summary>
        /// <param name="fileName">The file name.</param>
        /// <returns>The batch.</returns>
        public static Batch Load(string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Batch));
            FileStream stream = new FileStream(fileName, FileMode.Open);
            Batch batch = (Batch)serializer.Deserialize(stream);
            stream.Close();

            return batch;
        }
    }
}
