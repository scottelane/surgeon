﻿using System;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Defines operation operation types.
    /// </summary>
    public enum OperationType
    {
        Unspecified,
        Invalid,
        Activate,
        Assign,
        Associate,
        Create,
        Deactivate,
        Delete,
        Disassociate,
        Update,
        Upsert,
        Publish,
        PublishAll,
        SetState
    }

    /// <summary>
    /// Defines how the application should behaviour if an operation processing error is encountered.
    /// </summary>
    public enum ErrorBehaviour
    {
        Unspecified,
        Invalid,
        IgnoreOnce,
        IgnoreType,
        IgnoreAll,
        Stop
    }

    /// <summary>
    /// Defines operation processing outcomes.
    /// </summary>
    public enum ProcessingOutcome
    {
        Success,
        Failure,
        Warning
    }

    /// <summary>
    /// Defines job compilation types.
    /// </summary>
    public enum CompilationType
    {
        Static,
        Dynamic
    }

    /// <summary>
    /// Defines job statuses.
    /// </summary>
    public enum JobStatus
    {
        Creating,
        Validating,
        Invalid,
        Ready,
        Running,
        Stopped,
        Failed,
        Completed
    }

    /// <summary>
    /// Defines batch statuses.
    /// </summary>
    public enum BatchStatus
    {
        Invalid,
        Ready,
        Running,
        Stopped,
        Failed,
        Completed,
        Preparing
    }

    /// <summary>
    /// Specifies the types of authentication used to connect to CRM.
    /// </summary>
    public enum AuthenticationType
    {
        WindowsIntegrated,
        Custom
    }

    /// <summary>
    /// Specifies lookup types.
    /// </summary>
    public enum LookupType
    {
        FieldValuePair,
        Guid,
        NameLookup
    }

    /// <summary>
    /// Specifies the results from a connection test.
    /// </summary>
    [Flags]
    public enum ConnectionResult
    {
        None = 0,
        CrmConnectionSucceeded = 1,
        CrmConnectionFailed = 2,
        SqlConnectionMissing = 4,
        SqlConnectionSucceeded = 8,
        SqlConnectionFailed = 16
    }

    /// <summary>
    /// Specifies the types of editing modes on the connection details form.
    /// </summary>
    public enum ConnectionEditMode
    {
        Create,
        Edit
    }

    /// <summary>
    /// Specifies the CRM major versions.
    /// </summary>
    public enum CrmVersion
    {
        Crm2011 = 5,
        Crm2013 = 6,
        Crm2015 = 7
    }
}
