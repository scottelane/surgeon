﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Serialization;
using ScottLane.Surgeon.Model.Forms;
using ScottLane.Surgeon.Model.Resources;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// A batch job containing a series of operations that are run against a CRM instance.
    /// </summary>
    [XmlInclude(typeof(ExcelJob))]
    [XmlInclude(typeof(SqlJob))]
    public abstract class Job
    {
        #region Properties

        /// <summary>
        /// Gets or sets the compilation type.
        /// </summary>
        [XmlAttribute]
        public CompilationType CompilationType { get; set; }

        /// <summary>
        /// Gets the job status.
        /// </summary>
        [XmlIgnore]
        public JobStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the job name.
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the operations associated with the job.
        /// </summary>
        public List<Operation> Operations { get; set; }

        /// <summary>
        /// Gets or sets a description of the job import or running progress.
        /// </summary>
        [XmlIgnore]
        public string Progress { get; set; }

        /// <summary>
        /// Gets or sets the job file name.
        /// </summary>
        [XmlAttribute]
        public string FileName { get; set; }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Job class.
        /// </summary>
        public Job()
        {
            Operations = new List<Operation>();
            Status = JobStatus.Ready;
        }

        /// <summary>
        /// Validates operations in the batch.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The progress.</param>
        public virtual void ValidateOperations(Connection connection, CancellationToken cancel, Progress<JobProgress> progress)
        {
            ReportStatus(JobStatus.Validating, null, progress);

            for (int operationIndex = 0; operationIndex < Operations.Count && !cancel.IsCancellationRequested && Status != JobStatus.Invalid; operationIndex++)
            {
                Operation operation = Operations[operationIndex];

                try
                {
                    operation.Validate(connection);
                    //ReportStatus(JobStatus.Validating, new JobProgress(operationIndex + 1, Operations.Count), progress);
                }
                catch (ValidationException)
                {
                    ReportStatus(JobStatus.Invalid, new JobProgress(operationIndex + 1, Operations.Count), progress);
                    throw;
                }
            }

            ReportStatus(JobStatus.Ready, new JobProgress(Operations.Count, Operations.Count), progress);
        }

        /// <summary>
        /// Loads file contents.
        /// </summary>
        public abstract void LoadFileContents();

        /// <summary>
        /// Adds operations to the job.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The progress.</param>
        public abstract void AddOperations(Connection connection, CancellationToken cancel, Progress<JobProgress> progress);

        /// <summary>
        /// Runs the job on the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="cancel">Indicates whether the batch has been cancelled.</param>
        /// <param name="progress">The batch progress.</param>
        public virtual void Run(Connection connection, CancellationToken cancel, Progress<JobProgress> progress, ErrorDetailsForm form)
        {
            try
            {
                if (CompilationType == CompilationType.Dynamic)
                {
                    // dynamic job operations are generated at runtime
                    AddOperations(connection, cancel, progress);
                    ValidateOperations(connection, cancel, progress);
                }

                ReportStatus(JobStatus.Running, new JobProgress(0, Operations.Count), progress);
                Dictionary<string, string> ignoredTypes = new Dictionary<string, string>();
                bool ignoreErrors = false;
                int operationIndex;

                for (operationIndex = 0; operationIndex < Operations.Count && !cancel.IsCancellationRequested; operationIndex++)
                {
                    Operation operation = Operations[operationIndex];

                    try
                    {
                        operation.Perform(connection);
                    }
                    catch (Exception ex)
                    {
                        // apply error behaviour specified against the operation, or if not specified apply behaviour captured by error details form
                        ErrorBehaviour behaviour = ErrorBehaviour.Unspecified;

                        if (operation.ErrorBehaviour != ErrorBehaviour.Unspecified)
                        {
                            behaviour = operation.ErrorBehaviour;
                        }
                        else
                        {
                            if (!ignoreErrors)
                            {
                                if (!ignoredTypes.ContainsKey(ex.Message))
                                {
                                    ShowErrorDialog dialog = new ShowErrorDialog(form.ShowErrorDialog);
                                    behaviour = (ErrorBehaviour)form.Invoke(dialog, ex, operation);
                                }
                            }
                        }

                        if (behaviour == ErrorBehaviour.Stop)
                        {
                            form.Stop();
                        }
                        else if (behaviour == ErrorBehaviour.IgnoreAll && !ignoreErrors)
                        {
                            ignoreErrors = true;
                        }
                        else if (behaviour == ErrorBehaviour.IgnoreType && !ignoredTypes.ContainsKey(ex.Message))
                        {
                            ignoredTypes.Add(ex.Message, null);
                        }
                    }
                    finally
                    {
                        ReportStatus(JobStatus.Running, new JobProgress(operationIndex + 1, Operations.Count), progress);
                    }
                }

                if (cancel.IsCancellationRequested)
                {
                    ReportStatus(JobStatus.Stopped, new JobProgress(operationIndex, Operations.Count), progress);
                }
                else
                {
                    ReportStatus(JobStatus.Completed, new JobProgress(operationIndex, Operations.Count), progress);
                }
            }
            catch (ValidationException)
            {
                ReportStatus(JobStatus.Invalid, null, progress);
                throw;
            }
            catch (Exception)
            {
                ReportStatus(JobStatus.Failed, null, progress);
                throw;
            }
        }

        /// <summary>
        /// Reports the status of the job.
        /// </summary>
        /// <param name="status">The job status.</param>
        /// <param name="value">The job progress.</param>
        /// <param name="progress">The job progress.</param>
        protected virtual void ReportStatus(JobStatus status, JobProgress value, IProgress<JobProgress> progress)
        {
            Status = status;
            Progress = string.Empty;

            if (value != null)
            {
                int percentage = 0;

                if (value.OperationCount > 0)
                {
                    percentage = value.OperationIndex * 100 / value.OperationCount;
                }

                if (Status == JobStatus.Ready)
                {
                    progress = null;
                }
                else
                {
                    Progress = string.Format(ModelStrings.JobProgressText, value.OperationIndex, value.OperationCount, percentage);
                }
            }

            if (progress != null)
            {
                progress.Report(value);
            }
        }

        protected virtual int ReadOptionSetValue(Connection connection, object value)
        {
            return Convert.ToInt32(value);
        }
    }
}
