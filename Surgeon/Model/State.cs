﻿using System.Linq;
using Microsoft.Xrm.Sdk.Metadata;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Encapsulates a state and status pair and supports resolution of state and status values from their names.
    /// </summary>
    public class State
    {
        public const int MissingStateValue = int.MinValue;
        public const int MissingStatusValue = int.MinValue;

        /// <summary>
        /// Gets or sets the state code.
        /// </summary>
        public int StateCode { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Parses state and status names or values and makes a State object if successful.
        /// </summary>
        /// <param name="state">The state name or value.</param>
        /// <param name="status">The status name or value.</param>
        /// <param name="entityMetadata">The entity metadata.</param>
        /// <param name="result">The State object.</param>
        /// <returns>True if successful otherwise false.</returns>
        public static bool TryParse(object state, object status, EntityMetadata entityMetadata, out State result)
        {
            bool success = false;
            result = new State();
            result.StateCode = MissingStateValue;
            result.StatusCode = MissingStatusValue;

            int stateValue;      

            if (int.TryParse(state.ToString(), out stateValue))
            {
                result.StateCode = stateValue;
            }
            else
            {
                if (entityMetadata.Attributes != null)
                {
                    StateAttributeMetadata stateMetadata = (StateAttributeMetadata)entityMetadata.Attributes.FirstOrDefault(findField => findField is StateAttributeMetadata);

                    if (stateMetadata != null)
                    {
                        StateOptionMetadata optionMetadata = (StateOptionMetadata)stateMetadata.OptionSet.Options.FirstOrDefault(findOption => findOption.Label.UserLocalizedLabel.Label.ToLower() == state.ToString().ToLower());

                        if (optionMetadata != null)
                        {
                            result.StateCode = (int)optionMetadata.Value;
                        }
                    }
                }
            }

            int statusValue;

            if (int.TryParse(status.ToString(), out statusValue))
            {
                result.StatusCode = statusValue;
            }
            else
            {
                if (entityMetadata.Attributes != null)
                {
                    StatusAttributeMetadata statusMetadata = (StatusAttributeMetadata)entityMetadata.Attributes.FirstOrDefault(findField => findField is StatusAttributeMetadata);

                    if (statusMetadata != null)
                    {
                        StatusOptionMetadata optionMetadata = (StatusOptionMetadata)statusMetadata.OptionSet.Options.FirstOrDefault(findOption => findOption.Label.UserLocalizedLabel.Label.ToLower() == status.ToString().ToLower());

                        if (optionMetadata != null)
                        {
                            result.StatusCode = (int)optionMetadata.Value;
                        }
                    }
                }
            }

            success = result.StateCode != MissingStateValue && result.StatusCode != MissingStatusValue;

            if (!success)
            {
                result = null;
            }

            return success;
        }
    }
}
