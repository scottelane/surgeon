﻿using System;
using System.Windows.Forms;

namespace ScottLane.Surgeon.Model.Forms
{
    /// <summary>
    /// Base class for a form that launches an error details form.
    /// </summary>
    public class ErrorDetailsForm : Form
    {
        /// <summary>
        /// Shows an error dialog and returns the error behaviour selected in the dialog.
        /// </summary>
        /// <param name="exception">The exception details.</param>
        /// <param name="operation">The operation details.</param>
        /// <returns>The error behaviour.</returns>
        public virtual ErrorBehaviour ShowErrorDialog(Exception exception, Operation operation)
        {
            return ErrorBehaviour.Unspecified;
        }

        /// <summary>
        /// Stops any currently running operations.
        /// </summary>
        public virtual void Stop()
        { 
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="exception"></param>
    /// <param name="operation"></param>
    /// <returns></returns>
    public delegate ErrorBehaviour ShowErrorDialog(Exception exception, Operation operation);
}
