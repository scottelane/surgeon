﻿namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Specifies the logical names of CRM entities.
    /// </summary>
    public static class EntityName
    {
        public static readonly string Case = "incident";
        public static readonly string CaseResolution = "incidentresolution";
        public static readonly string Quote = "quote";
        public static readonly string QuoteClose = "quoteclose";
        public static readonly string Team = "team";
        public static readonly string User = "systemuser";
    }
}
