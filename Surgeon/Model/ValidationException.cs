﻿using System;

namespace ScottLane.Surgeon.Model
{
    /// <summary>
    /// Exception thrown if an operation is invalid.
    /// </summary>
    [Serializable]
    public class ValidationException : ApplicationException
    {
        /// <summary>
        /// Initialises a new instance of the ValidationException class with the specified error message.
        /// </summary>
        /// <param name="message">The error message.</param>
        public ValidationException(string message)
            : base(message)
        {
        }
    }
}
