﻿using System;
using System.Windows.Forms;
using ScottLane.Surgeon.Client.Forms;
using ScottLane.Surgeon.Client.Helpers;
using ScottLane.Surgeon.Model;

namespace ScottLane.Surgeon.Client.Controls
{
    /// <summary>
    /// A custom tool strip menu item that has the ability to connect to a connection, delete a connection or edit a connection.
    /// </summary>
    public class ConnectionToolStripMenuItem : ToolStripMenuItem
    {
        private ToolStripMenuItem connectToolStripMenuItem;
        private ToolStripMenuItem editToolStripMenuItem;
        private ToolStripMenuItem deleteToolStripMenuItem;

        #region Properties

        private Connection connection;

        /// <summary>
        /// Gets the connection.
        /// </summary>
        public Connection Connection
        {
            get { return connection; }
        }

        private bool setAsCurrentConnection;

        /// <summary>
        /// Gets a value indicating whether the connection just edited should be set as the current connection.
        /// </summary>
        public bool SetAsCurrentConnection
        {
            get { return setAsCurrentConnection; }
        }

        /// <summary>
        /// Event raised when a connection is connected to.
        /// </summary>
        public event EventHandler Connect;

        /// <summary>
        /// Event raised when a connection is deleted.
        /// </summary>
        public event EventHandler Delete;

        /// <summary>
        /// Event raised when a connection is edited.
        /// </summary>
        public event EventHandler Edited;

        #endregion

        /// <summary>
        /// Initialises a new instance of the ConnectionToolStripMenuItem class with the specified connection details.
        /// </summary>
        /// <param name="connection"></param>
        public ConnectionToolStripMenuItem(Connection connection)
        {
            this.connection = connection;

            Name = string.Format("{0}ToolStripMenuItem", this.connection.Name); ;
            Size = new System.Drawing.Size(196, 22);
            Text = connection.Name;

            connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            connectToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.ConnectionConnect;
            connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            connectToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            connectToolStripMenuItem.Text = "Connect";
            connectToolStripMenuItem.Click += connectToolStripMenuItem_Click;

            editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            editToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.ConnectionEdit;
            editToolStripMenuItem.Name = "editToolStripMenuItem";
            editToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            editToolStripMenuItem.Text = "Edit";
            editToolStripMenuItem.Click += editToolStripMenuItem_Click;

            deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            deleteToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.ConnectionDelete;
            deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            deleteToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            deleteToolStripMenuItem.Text = "Delete";
            deleteToolStripMenuItem.Click += deleteToolStripMenuItem_Click;

            DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            connectToolStripMenuItem,
            editToolStripMenuItem,
            deleteToolStripMenuItem});
        }

        /// <summary>
        /// Deletes a connection.
        /// </summary>
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OnDelete(null);
        }

        /// <summary>
        /// Raises the delete event.
        /// </summary>
        protected virtual void OnDelete(EventArgs e)
        {
            if (Delete != null)
            {
                Delete(this, e);
            }
        }

        /// <summary>
        /// Launches the connection edit form.
        /// </summary>
        private void editToolStripMenuItem_Click(object sender, EventArgs e)    // todo - create custom event args to pass 'setAsCurrent'
        {
            try
            {
                ConnectionDetailsForm form = new ConnectionDetailsForm(connection);
                DialogResult result = form.ShowDialog();

                if (result == DialogResult.OK)
                {
                    setAsCurrentConnection = form.SetAsCurrentConnection;
                    OnEdited(null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connection edit failed: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Raises the Edited event.
        /// </summary>
        protected virtual void OnEdited(EventArgs e)
        {
            if (Edited != null)
            {
                Edited(this, e);
            }
        }

        /// <summary>
        /// Connects to a connection.
        /// </summary>
        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OnConnect(null);
        }

        /// <summary>
        /// Raises the Connect event.
        /// </summary>
        protected virtual void OnConnect(EventArgs e)
        {
            if (Connect != null)
            {
                Connect(this, e);
            }
        }
    }
}
