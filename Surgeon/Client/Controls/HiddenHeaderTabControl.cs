﻿using System;
using System.Windows.Forms;

namespace ScottLane.Surgeon.Client.Controls
{
    /// <summary>
    /// A tab control with hidden headers.
    /// </summary>
    public class HiddenHeaderTabControl : TabControl
    {
        /// <summary>
        /// Overrides the draw event to hide the tab headers.
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x1328 && !DesignMode)
            {
                m.Result = (IntPtr)1;
            }
            else
            {
                base.WndProc(ref m);
            }
        }
    }
}
