﻿using System;
using System.Windows.Forms;

namespace ScottLane.Surgeon.Client.Helpers
{
    /// <summary>
    /// Utility methods for the Client project.
    /// </summary>
    public class ClientUtility
    {
        /// <summary>
        /// Handles form exceptions by logging the error and displaying an error message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public static void HandleFormException(Exception ex)
        {
            try
            {
                Logger.WriteLine(ex.Message);
                Logger.WriteLine(ex.StackTrace);
            }
            catch { }

            try
            {
                MessageBox.Show(ex.Message, Resources.ClientStrings.ErrorMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch { }
        }
    }
}
