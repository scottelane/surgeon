﻿using System;
using System.Diagnostics;
using System.IO;

namespace ScottLane.Surgeon.Client.Helpers
{
    /// <summary>
    /// Provides basic logging functionality.
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Initialises a new instance of the Logger class.
        /// </summary>
        static Logger()
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Path.Combine(Directory.GetCurrentDirectory(), "Log.txt")));
            Trace.AutoFlush = true;
        }

        /// <summary>
        /// Writes a message to the log.
        /// </summary>
        /// <param name="message">The message to log.</param>
        public static void WriteLine(string message)
        {
            try
            {
                Trace.WriteLine(string.Format("{0} {1}", DateTime.Now, message));
            }
            catch { }
        }
    }
}
