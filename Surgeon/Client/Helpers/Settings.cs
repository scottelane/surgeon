﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Forms;
using ScottLane.Surgeon.Model;

namespace ScottLane.Surgeon.Client.Helpers
{
    /// <summary>
    /// Manages user settings.
    /// </summary>
    public class Settings
    {
        private static string settingsFileName;
        private static Settings instance;

        /// <summary>
        /// Gets a singleton instance of the settings class and loads settings from a file.
        /// </summary>
        public static Settings Default
        {
            get
            {
                if (instance == null)
                {
                    if (File.Exists(settingsFileName))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                        FileStream stream = new FileStream(settingsFileName, FileMode.Open);
                        instance = (Settings)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    else
                    {
                        instance = new Settings();
                    }
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets or sets CRM connections.
        /// </summary>
        public List<Connection> Connections { get; set; }

        /// <summary>
        /// Gets or sets the current connection.
        /// </summary>
        public Connection CurrentConnection { get; set; }

        /// <summary>
        /// Initialises a private instance of the Settings class.
        /// </summary>
        private Settings()
        {
            Connections = new List<Connection>();
        }

        /// <summary>
        /// Initialises a static instance of the Settings class.
        /// </summary>
        static Settings()
        {
            settingsFileName = Path.Combine(Application.UserAppDataPath, "Settings.xml");
        }

        /// <summary>
        /// Saves settings to a file.
        /// </summary>
        public void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));
            FileStream stream = new FileStream(settingsFileName, FileMode.Create);
            serializer.Serialize(stream, instance);
            stream.Close();
        }
    }
}
