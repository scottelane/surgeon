﻿namespace ScottLane.Surgeon.Client.Forms
{
    partial class ConnectionDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sqlTimeoutTextBox = new System.Windows.Forms.TextBox();
            this.sqlConnectionStringTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.crmTimeoutTextBox = new System.Windows.Forms.TextBox();
            this.customAuthenticationRadioButton = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.windowsIntegratedAuthenticationRadioButton = new System.Windows.Forms.RadioButton();
            this.organisationServiceUrlTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.domainTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.userNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.testConnectionButton = new System.Windows.Forms.Button();
            this.setAsCurrentConnection = new System.Windows.Forms.CheckBox();
            this.formHelpProvider = new System.Windows.Forms.HelpProvider();
            this.testingPictureBox = new System.Windows.Forms.PictureBox();
            this.formErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testingPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 28);
            this.label8.Name = "label8";
            this.formHelpProvider.SetShowHelp(this.label8, true);
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Connection String";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.sqlTimeoutTextBox);
            this.groupBox2.Controls.Add(this.sqlConnectionStringTextBox);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 241);
            this.groupBox2.Name = "groupBox2";
            this.formHelpProvider.SetShowHelp(this.groupBox2, true);
            this.groupBox2.Size = new System.Drawing.Size(467, 88);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SQL Server Connection Settings";
            // 
            // sqlTimeoutTextBox
            // 
            this.sqlTimeoutTextBox.Location = new System.Drawing.Point(111, 52);
            this.sqlTimeoutTextBox.MaxLength = 5;
            this.sqlTimeoutTextBox.Name = "sqlTimeoutTextBox";
            this.sqlTimeoutTextBox.Size = new System.Drawing.Size(60, 20);
            this.sqlTimeoutTextBox.TabIndex = 19;
            this.sqlTimeoutTextBox.TextChanged += new System.EventHandler(this.sqlTimeoutTextBox_TextChanged);
            // 
            // sqlConnectionStringTextBox
            // 
            this.sqlConnectionStringTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sqlConnectionStringTextBox.Location = new System.Drawing.Point(111, 25);
            this.sqlConnectionStringTextBox.Name = "sqlConnectionStringTextBox";
            this.formHelpProvider.SetShowHelp(this.sqlConnectionStringTextBox, true);
            this.sqlConnectionStringTextBox.Size = new System.Drawing.Size(350, 20);
            this.sqlConnectionStringTextBox.TabIndex = 18;
            this.sqlConnectionStringTextBox.TextChanged += new System.EventHandler(this.sqlConnectionStringTextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Timeout (sec)";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.crmTimeoutTextBox);
            this.groupBox1.Controls.Add(this.customAuthenticationRadioButton);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.windowsIntegratedAuthenticationRadioButton);
            this.groupBox1.Controls.Add(this.organisationServiceUrlTextBox);
            this.groupBox1.Controls.Add(this.passwordTextBox);
            this.groupBox1.Controls.Add(this.domainTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.userNameTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 46);
            this.groupBox1.Name = "groupBox1";
            this.formHelpProvider.SetShowHelp(this.groupBox1, true);
            this.groupBox1.Size = new System.Drawing.Size(467, 188);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CRM Connection Settings";
            // 
            // crmTimeoutTextBox
            // 
            this.crmTimeoutTextBox.Location = new System.Drawing.Point(111, 51);
            this.crmTimeoutTextBox.MaxLength = 5;
            this.crmTimeoutTextBox.Name = "crmTimeoutTextBox";
            this.crmTimeoutTextBox.Size = new System.Drawing.Size(60, 20);
            this.crmTimeoutTextBox.TabIndex = 12;
            this.crmTimeoutTextBox.TextChanged += new System.EventHandler(this.crmTimeoutTextBox_TextChanged);
            // 
            // customAuthenticationRadioButton
            // 
            this.customAuthenticationRadioButton.AutoSize = true;
            this.customAuthenticationRadioButton.Location = new System.Drawing.Point(237, 77);
            this.customAuthenticationRadioButton.Name = "customAuthenticationRadioButton";
            this.formHelpProvider.SetShowHelp(this.customAuthenticationRadioButton, true);
            this.customAuthenticationRadioButton.Size = new System.Drawing.Size(60, 17);
            this.customAuthenticationRadioButton.TabIndex = 14;
            this.customAuthenticationRadioButton.Text = "Custom";
            this.customAuthenticationRadioButton.UseVisualStyleBackColor = true;
            this.customAuthenticationRadioButton.CheckedChanged += new System.EventHandler(this.customAuthenticationRadioButton_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Timeout (sec)";
            // 
            // windowsIntegratedAuthenticationRadioButton
            // 
            this.windowsIntegratedAuthenticationRadioButton.AutoSize = true;
            this.windowsIntegratedAuthenticationRadioButton.Location = new System.Drawing.Point(111, 77);
            this.windowsIntegratedAuthenticationRadioButton.Name = "windowsIntegratedAuthenticationRadioButton";
            this.formHelpProvider.SetShowHelp(this.windowsIntegratedAuthenticationRadioButton, true);
            this.windowsIntegratedAuthenticationRadioButton.Size = new System.Drawing.Size(120, 17);
            this.windowsIntegratedAuthenticationRadioButton.TabIndex = 13;
            this.windowsIntegratedAuthenticationRadioButton.Text = "Windows Integrated";
            this.windowsIntegratedAuthenticationRadioButton.UseVisualStyleBackColor = true;
            this.windowsIntegratedAuthenticationRadioButton.CheckedChanged += new System.EventHandler(this.windowsIntegratedAuthenticationRadioButton_CheckedChanged);
            // 
            // organisationServiceUrlTextBox
            // 
            this.organisationServiceUrlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.organisationServiceUrlTextBox.Location = new System.Drawing.Point(111, 25);
            this.organisationServiceUrlTextBox.Name = "organisationServiceUrlTextBox";
            this.formHelpProvider.SetShowHelp(this.organisationServiceUrlTextBox, true);
            this.organisationServiceUrlTextBox.Size = new System.Drawing.Size(350, 20);
            this.organisationServiceUrlTextBox.TabIndex = 11;
            this.organisationServiceUrlTextBox.TextChanged += new System.EventHandler(this.organisationServiceUrlTextBox_TextChanged);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordTextBox.Location = new System.Drawing.Point(111, 154);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.formHelpProvider.SetShowHelp(this.passwordTextBox, true);
            this.passwordTextBox.Size = new System.Drawing.Size(350, 20);
            this.passwordTextBox.TabIndex = 17;
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            // 
            // domainTextBox
            // 
            this.domainTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.domainTextBox.Location = new System.Drawing.Point(111, 102);
            this.domainTextBox.Name = "domainTextBox";
            this.formHelpProvider.SetShowHelp(this.domainTextBox, true);
            this.domainTextBox.Size = new System.Drawing.Size(350, 20);
            this.domainTextBox.TabIndex = 15;
            this.domainTextBox.TextChanged += new System.EventHandler(this.domainTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 79);
            this.label2.Name = "label2";
            this.formHelpProvider.SetShowHelp(this.label2, true);
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Authentication";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 105);
            this.label3.Name = "label3";
            this.formHelpProvider.SetShowHelp(this.label3, true);
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Domain";
            // 
            // userNameTextBox
            // 
            this.userNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userNameTextBox.Location = new System.Drawing.Point(111, 128);
            this.userNameTextBox.Name = "userNameTextBox";
            this.formHelpProvider.SetShowHelp(this.userNameTextBox, true);
            this.userNameTextBox.Size = new System.Drawing.Size(350, 20);
            this.userNameTextBox.TabIndex = 16;
            this.userNameTextBox.TextChanged += new System.EventHandler(this.userNameTextBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 131);
            this.label4.Name = "label4";
            this.formHelpProvider.SetShowHelp(this.label4, true);
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "User Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 157);
            this.label7.Name = "label7";
            this.formHelpProvider.SetShowHelp(this.label7, true);
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 28);
            this.label5.Name = "label5";
            this.formHelpProvider.SetShowHelp(this.label5, true);
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Service URL";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point(323, 370);
            this.cancelButton.Name = "cancelButton";
            this.formHelpProvider.SetShowHelp(this.cancelButton, true);
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 22;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.Location = new System.Drawing.Point(404, 370);
            this.saveButton.Name = "saveButton";
            this.formHelpProvider.SetShowHelp(this.saveButton, true);
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 23;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.Location = new System.Drawing.Point(123, 16);
            this.nameTextBox.Name = "nameTextBox";
            this.formHelpProvider.SetShowHelp(this.nameTextBox, true);
            this.nameTextBox.Size = new System.Drawing.Size(350, 20);
            this.nameTextBox.TabIndex = 10;
            this.nameTextBox.TextChanged += new System.EventHandler(this.nameTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 19);
            this.label1.Name = "label1";
            this.formHelpProvider.SetShowHelp(this.label1, true);
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Connection Name";
            // 
            // testConnectionButton
            // 
            this.testConnectionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.testConnectionButton.Location = new System.Drawing.Point(217, 370);
            this.testConnectionButton.Name = "testConnectionButton";
            this.formHelpProvider.SetShowHelp(this.testConnectionButton, true);
            this.testConnectionButton.Size = new System.Drawing.Size(100, 23);
            this.testConnectionButton.TabIndex = 21;
            this.testConnectionButton.Text = "Test Connection";
            this.testConnectionButton.UseVisualStyleBackColor = true;
            this.testConnectionButton.Click += new System.EventHandler(this.testConnectionButton_Click);
            // 
            // setAsCurrentConnection
            // 
            this.setAsCurrentConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.setAsCurrentConnection.AutoSize = true;
            this.setAsCurrentConnection.Location = new System.Drawing.Point(123, 340);
            this.setAsCurrentConnection.Name = "setAsCurrentConnection";
            this.formHelpProvider.SetShowHelp(this.setAsCurrentConnection, true);
            this.setAsCurrentConnection.Size = new System.Drawing.Size(148, 17);
            this.setAsCurrentConnection.TabIndex = 20;
            this.setAsCurrentConnection.Text = "Set as current connection";
            this.setAsCurrentConnection.UseVisualStyleBackColor = true;
            // 
            // formHelpProvider
            // 
            this.formHelpProvider.HelpNamespace = "";
            // 
            // testingPictureBox
            // 
            this.testingPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.testingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.testingPictureBox.Location = new System.Drawing.Point(195, 374);
            this.testingPictureBox.Name = "testingPictureBox";
            this.testingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.testingPictureBox.TabIndex = 22;
            this.testingPictureBox.TabStop = false;
            this.testingPictureBox.Visible = false;
            // 
            // formErrorProvider
            // 
            this.formErrorProvider.ContainerControl = this;
            // 
            // ConnectionDetailsForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(492, 403);
            this.Controls.Add(this.testingPictureBox);
            this.Controls.Add(this.setAsCurrentConnection);
            this.Controls.Add(this.testConnectionButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(508, 389);
            this.Name = "ConnectionDetailsForm";
            this.formHelpProvider.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Connection Details";
            this.Load += new System.EventHandler(this.ConnectionForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testingPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox sqlConnectionStringTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox organisationServiceUrlTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox domainTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox userNameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.RadioButton customAuthenticationRadioButton;
        private System.Windows.Forms.RadioButton windowsIntegratedAuthenticationRadioButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.HelpProvider formHelpProvider;
        private System.Windows.Forms.Button testConnectionButton;
        private System.Windows.Forms.CheckBox setAsCurrentConnection;
        private System.Windows.Forms.PictureBox testingPictureBox;
        private System.Windows.Forms.ErrorProvider formErrorProvider;
        private System.Windows.Forms.TextBox sqlTimeoutTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox crmTimeoutTextBox;
        private System.Windows.Forms.Label label9;
    }
}