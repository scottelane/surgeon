﻿namespace ScottLane.Surgeon.Client.Forms
{
    partial class ErrorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.stopButton = new System.Windows.Forms.Button();
            this.errorMessageLabel = new System.Windows.Forms.Label();
            this.iconPictureBox = new System.Windows.Forms.PictureBox();
            this.continueMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ignoreOnceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ignoreTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ignoreAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDetailsButton = new System.Windows.Forms.Button();
            this.errorDetailsTreeView = new System.Windows.Forms.TreeView();
            this.copyMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyDetailsToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.continueButton = new wyDay.Controls.SplitButton();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).BeginInit();
            this.continueMenuStrip.SuspendLayout();
            this.copyMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.Location = new System.Drawing.Point(282, 12);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(90, 23);
            this.stopButton.TabIndex = 1;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // errorMessageLabel
            // 
            this.errorMessageLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorMessageLabel.Location = new System.Drawing.Point(63, 31);
            this.errorMessageLabel.Name = "errorMessageLabel";
            this.errorMessageLabel.Size = new System.Drawing.Size(309, 35);
            this.errorMessageLabel.TabIndex = 1;
            this.errorMessageLabel.Text = "An operation error has occurred. Do you want to continue?";
            // 
            // iconPictureBox
            // 
            this.iconPictureBox.Location = new System.Drawing.Point(25, 25);
            this.iconPictureBox.Name = "iconPictureBox";
            this.iconPictureBox.Size = new System.Drawing.Size(32, 32);
            this.iconPictureBox.TabIndex = 6;
            this.iconPictureBox.TabStop = false;
            // 
            // continueMenuStrip
            // 
            this.continueMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ignoreOnceToolStripMenuItem,
            this.ignoreTypeToolStripMenuItem,
            this.ignoreAllToolStripMenuItem});
            this.continueMenuStrip.Name = "continueMenuStrip";
            this.continueMenuStrip.Size = new System.Drawing.Size(163, 70);
            this.continueMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.continueMenuStrip_ItemClicked);
            // 
            // ignoreOnceToolStripMenuItem
            // 
            this.ignoreOnceToolStripMenuItem.Name = "ignoreOnceToolStripMenuItem";
            this.ignoreOnceToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.ignoreOnceToolStripMenuItem.Text = "Ignore once";
            this.ignoreOnceToolStripMenuItem.ToolTipText = "Ignore this error and continue processing";
            // 
            // ignoreTypeToolStripMenuItem
            // 
            this.ignoreTypeToolStripMenuItem.Name = "ignoreTypeToolStripMenuItem";
            this.ignoreTypeToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.ignoreTypeToolStripMenuItem.Text = "Ignore error type";
            this.ignoreTypeToolStripMenuItem.ToolTipText = "Ignore all errors of this type in this batch and continue processing";
            // 
            // ignoreAllToolStripMenuItem
            // 
            this.ignoreAllToolStripMenuItem.Name = "ignoreAllToolStripMenuItem";
            this.ignoreAllToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.ignoreAllToolStripMenuItem.Text = "Ignore all";
            this.ignoreAllToolStripMenuItem.ToolTipText = "Ignore all errors in this batch and continue processing";
            // 
            // showDetailsButton
            // 
            this.showDetailsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showDetailsButton.Location = new System.Drawing.Point(75, 12);
            this.showDetailsButton.Name = "showDetailsButton";
            this.showDetailsButton.Size = new System.Drawing.Size(105, 23);
            this.showDetailsButton.TabIndex = 2;
            this.showDetailsButton.Text = "Show Details...";
            this.showDetailsButton.UseVisualStyleBackColor = true;
            this.showDetailsButton.Click += new System.EventHandler(this.showDetailsButton_Click);
            // 
            // errorDetailsTreeView
            // 
            this.errorDetailsTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorDetailsTreeView.ContextMenuStrip = this.copyMenuStrip;
            this.errorDetailsTreeView.Location = new System.Drawing.Point(12, 48);
            this.errorDetailsTreeView.Name = "errorDetailsTreeView";
            this.errorDetailsTreeView.Size = new System.Drawing.Size(360, 284);
            this.errorDetailsTreeView.TabIndex = 9;
            this.errorDetailsTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.errorDetailsTreeView_NodeMouseClick);
            this.errorDetailsTreeView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.errorDetailsTreeView_KeyDown);
            // 
            // copyMenuStrip
            // 
            this.copyMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyDetailsToClipboardToolStripMenuItem});
            this.copyMenuStrip.Name = "copyMenuStrip";
            this.copyMenuStrip.Size = new System.Drawing.Size(103, 26);
            // 
            // copyDetailsToClipboardToolStripMenuItem
            // 
            this.copyDetailsToClipboardToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Copy;
            this.copyDetailsToClipboardToolStripMenuItem.Name = "copyDetailsToClipboardToolStripMenuItem";
            this.copyDetailsToClipboardToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.copyDetailsToClipboardToolStripMenuItem.Text = "Copy";
            this.copyDetailsToClipboardToolStripMenuItem.Click += new System.EventHandler(this.copyDetailsToClipboardToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.iconPictureBox);
            this.splitContainer1.Panel1.Controls.Add(this.errorMessageLabel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splitContainer1.Panel2.Controls.Add(this.continueButton);
            this.splitContainer1.Panel2.Controls.Add(this.errorDetailsTreeView);
            this.splitContainer1.Panel2.Controls.Add(this.stopButton);
            this.splitContainer1.Panel2.Controls.Add(this.showDetailsButton);
            this.splitContainer1.Size = new System.Drawing.Size(384, 432);
            this.splitContainer1.SplitterDistance = 80;
            this.splitContainer1.TabIndex = 9;
            this.splitContainer1.TabStop = false;
            // 
            // continueButton
            // 
            this.continueButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.continueButton.AutoSize = true;
            this.continueButton.ContextMenuStrip = this.continueMenuStrip;
            this.continueButton.Location = new System.Drawing.Point(186, 12);
            this.continueButton.Name = "continueButton";
            this.continueButton.Size = new System.Drawing.Size(90, 23);
            this.continueButton.SplitMenuStrip = this.continueMenuStrip;
            this.continueButton.TabIndex = 0;
            this.continueButton.Text = "Continue";
            this.continueButton.UseVisualStyleBackColor = true;
            this.continueButton.Click += new System.EventHandler(this.continueButton_Click);
            // 
            // ErrorDialog
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(384, 432);
            this.Controls.Add(this.splitContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 170);
            this.Name = "ErrorDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Operation Error";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ErrorDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).EndInit();
            this.continueMenuStrip.ResumeLayout(false);
            this.copyMenuStrip.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label errorMessageLabel;
        private System.Windows.Forms.PictureBox iconPictureBox;
        private System.Windows.Forms.ContextMenuStrip continueMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ignoreOnceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ignoreAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ignoreTypeToolStripMenuItem;
        private System.Windows.Forms.Button showDetailsButton;
        private System.Windows.Forms.TreeView errorDetailsTreeView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private wyDay.Controls.SplitButton continueButton;
        private System.Windows.Forms.ContextMenuStrip copyMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyDetailsToClipboardToolStripMenuItem;
    }
}