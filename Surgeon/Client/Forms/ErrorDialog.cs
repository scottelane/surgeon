﻿using System;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Xrm.Sdk;
using ScottLane.Surgeon.Client.Resources;
using ScottLane.Surgeon.Client.Helpers;
using ScottLane.Surgeon.Model;

namespace ScottLane.Surgeon.Client.Forms
{
    /// <summary>
    /// Displays an error dialog and provides the ability for the user to determine how the application should continue.
    /// </summary>
    public partial class ErrorDialog : Form
    {
        private bool errorDetailsVisible;

        #region Properties

        /// <summary>
        /// Gets or sets the error behaviour captured on the form.
        /// </summary>
        public ErrorBehaviour ErrorBehaviour { get; set; }

        /// <summary>
        /// Gets or sets the Exception raised when processing an operation.
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Gets or sets the Operation that failed.
        /// </summary>
        public Operation Operation { get; set; }

        #endregion

        /// <summary>
        /// Initialises a new instance of the ErrorDialog class.
        /// </summary>
        public ErrorDialog()
        {
            InitializeComponent();
            ErrorBehaviour = ErrorBehaviour.Stop;
            iconPictureBox.Image = SystemIcons.Question.ToBitmap();
            HideErrorDetails();
        }

        /// <summary>
        /// Populates and expands the error details on load.
        /// </summary>
        private void ErrorDialog_Load(object sender, EventArgs e)
        {
            try
            {
                Logger.WriteLine(Exception.Message);
                TreeNode rootNode = new TreeNode();
                rootNode.Text = ClientStrings.ErrorDialogRootNodeText;
                errorDetailsTreeView.Nodes.Add(rootNode);
                CreateChildNode(Exception, rootNode, ClientStrings.ErrorDialogExceptionNodeText);
                CreateChildNode(Operation, rootNode, null);
                errorDetailsTreeView.ExpandAll();
                BringToFront();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Creates a child node in the treeview.
        /// </summary>
        /// <param name="item">The item to display.</param>
        /// <param name="parentNode">The parent node.</param>
        private void CreateChildNode(object item, TreeNode parentNode)
        {
            CreateChildNode(item, parentNode, null);
        }

        /// <summary>
        /// Creates a child node in the treeview with the specified name.
        /// </summary>
        /// <param name="item">The item to display.</param>
        /// <param name="parentNode">The parent node.</param>
        /// <param name="name">The name of the child node.</param>
        private void CreateChildNode(object item, TreeNode parentNode, string name)
        {
            TreeNode node = new TreeNode();
            node.Text = name == null ? item.GetType().Name : name;
            parentNode.Nodes.Add(node);

            foreach (PropertyInfo property in item.GetType().GetProperties())
            {
                object value = property.GetValue(item);

                if (property.PropertyType.UnderlyingSystemType != typeof(String)
                    && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
                {
                    // manage display of collections
                    int itemCount = 0;

                    foreach (object childItem in (IEnumerable)property.GetValue(item))
                    {
                        itemCount++;
                    }

                    TreeNode collectionNode = new TreeNode();
                    collectionNode.Text = itemCount > 0 ? property.Name : string.Format("{0}: ({1})", property.Name, ClientStrings.ErrorDialogEmptyText);
                    node.Nodes.Add(collectionNode);

                    foreach (object childItem in (IEnumerable)property.GetValue(item))
                    {
                        CreateChildNode(childItem, collectionNode);
                    }
                }
                else if (value != null &&
                    (typeof(Lookup).IsAssignableFrom(property.GetValue(item).GetType()) ||
                    typeof(OptionSetValue).IsAssignableFrom(property.GetValue(item).GetType()) ||
                    typeof(State).IsAssignableFrom(property.GetValue(item).GetType())))
                {
                    // manage display of objects
                    TreeNode objectNode = new TreeNode();
                    objectNode.Text = property.Name;
                    node.Nodes.Add(objectNode);
                    CreateChildNode(value, objectNode);
                }
                else
                {
                    // manage display of value types
                    TreeNode propertyNode = new TreeNode();
                    string valueText = value != null ? value.ToString() : string.Format("({0})", ClientStrings.ErrorDialogNullText);
                    propertyNode.Text = string.Format("{0}: {1}", property.Name, valueText);
                    node.Nodes.Add(propertyNode);
                }
            }
        }

        /// <summary>
        /// Toggles the display of error details.
        /// </summary>
        private void showDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (errorDetailsVisible)
                {
                    HideErrorDetails();
                }
                else
                {
                    ShowErrorDetails();
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Hides the error details.
        /// </summary>
        private void HideErrorDetails()
        {
            showDetailsButton.Text = ClientStrings.ErrorDialogShowDetailsText;
            errorDetailsTreeView.Visible = false;
            Size = new Size(Size.Width, 170);
            errorDetailsVisible = false;
        }

        /// <summary>
        /// Shows the error details.
        /// </summary>
        private void ShowErrorDetails()
        {
            Size = new Size(Size.Width, 470);
            errorDetailsTreeView.Visible = true;
            showDetailsButton.Text = ClientStrings.ErrorDialogHideDetailsText;
            errorDetailsVisible = true;
        }

        /// <summary>
        /// Stops the batch.
        /// </summary>
        private void stopButton_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorBehaviour = ErrorBehaviour.Stop;
                Close();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Sets the ErrorBehaviour corresponding with the selected continue behaviour.
        /// </summary>
        private void continueMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Name == ignoreOnceToolStripMenuItem.Name)
                {
                    SetErrorBehaviourAndClose(ErrorBehaviour.IgnoreOnce);
                }
                else if (e.ClickedItem.Name == ignoreTypeToolStripMenuItem.Name)
                {
                    SetErrorBehaviourAndClose(ErrorBehaviour.IgnoreType);
                }
                else if (e.ClickedItem.Name == ignoreAllToolStripMenuItem.Name)
                {
                    SetErrorBehaviourAndClose(ErrorBehaviour.IgnoreAll);
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        private void continueButton_Click(object sender, EventArgs e)
        {
            try
            {
                SetErrorBehaviourAndClose(ErrorBehaviour.IgnoreOnce);
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Sets the application error handling behaviour and closes the form.
        /// </summary>
        /// <param name="behaviour">The error behaviour.</param>
        private void SetErrorBehaviourAndClose(ErrorBehaviour behaviour)
        {
            ErrorBehaviour = behaviour;
            Close();
        }

        /// <summary>
        /// Copies the selected node's details to the clipboard.
        /// </summary>
        private void errorDetailsTreeView_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == (Keys.Control | Keys.C))
                {
                    if (errorDetailsTreeView.SelectedNode != null)
                    {
                        Clipboard.SetText(errorDetailsTreeView.SelectedNode.Text);
                    }

                    e.SuppressKeyPress = true;
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Copies the selected node's details to the clipboard.
        /// </summary>
        private void copyDetailsToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (errorDetailsTreeView.SelectedNode != null)
                {
                    Clipboard.SetText(errorDetailsTreeView.SelectedNode.Text);
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Selects the node that receives focus from a right-mouse click.
        /// </summary>
        private void errorDetailsTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    errorDetailsTreeView.SelectedNode = e.Node;
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }
    }
}
