﻿namespace ScottLane.Surgeon.Client.Forms
{
    partial class JobImportWizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.backButton = new System.Windows.Forms.Button();
            this.finishButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.wizardTabControl = new ScottLane.Surgeon.Client.Controls.HiddenHeaderTabControl();
            this.operationTabPage = new System.Windows.Forms.TabPage();
            this.operationComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.entityTabPage = new System.Windows.Forms.TabPage();
            this.entityLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.entityComboBox = new System.Windows.Forms.ComboBox();
            this.recordsLabel = new System.Windows.Forms.Label();
            this.fieldTabPage = new System.Windows.Forms.TabPage();
            this.selectNoneButton = new System.Windows.Forms.Button();
            this.selectAllButton = new System.Windows.Forms.Button();
            this.fieldsLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.fieldsCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.fieldsLabel = new System.Windows.Forms.Label();
            this.ownerTabPage = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.teamLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.userLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.teamRadioButton = new System.Windows.Forms.RadioButton();
            this.userRadioButton = new System.Windows.Forms.RadioButton();
            this.teamComboBox = new System.Windows.Forms.ComboBox();
            this.userComboBox = new System.Windows.Forms.ComboBox();
            this.relatedEntityTabPage = new System.Windows.Forms.TabPage();
            this.relatedEntityLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.relationshipLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.relationshipComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.relatedEntityComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.stateTabPage = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.statusLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.stateLoadingPictureBox = new System.Windows.Forms.PictureBox();
            this.statusComboBox = new System.Windows.Forms.ComboBox();
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.templateTabPage = new System.Windows.Forms.TabPage();
            this.fetchXmlStaticRadioButton = new System.Windows.Forms.RadioButton();
            this.fetchXmlDynamicRadioButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.excelRadioButton = new System.Windows.Forms.RadioButton();
            this.sqlStaticRadioButton = new System.Windows.Forms.RadioButton();
            this.sqlDynamicRadioButton = new System.Windows.Forms.RadioButton();
            this.fetchXmlTabPage = new System.Windows.Forms.TabPage();
            this.fetchXmlTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.finishTabPage = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.wizardTabControl.SuspendLayout();
            this.operationTabPage.SuspendLayout();
            this.entityTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.entityLoadingPictureBox)).BeginInit();
            this.fieldTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fieldsLoadingPictureBox)).BeginInit();
            this.ownerTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamLoadingPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLoadingPictureBox)).BeginInit();
            this.relatedEntityTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.relatedEntityLoadingPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relationshipLoadingPictureBox)).BeginInit();
            this.stateTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoadingPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateLoadingPictureBox)).BeginInit();
            this.templateTabPage.SuspendLayout();
            this.fetchXmlTabPage.SuspendLayout();
            this.finishTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(482, 329);
            this.splitContainer1.SplitterDistance = 60;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 8;
            this.splitContainer1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(341, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Follow the steps below to create and import a job into the current batch";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Job Import Wizard";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.wizardTabControl);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel2.Controls.Add(this.backButton);
            this.splitContainer2.Panel2.Controls.Add(this.finishButton);
            this.splitContainer2.Panel2.Controls.Add(this.cancelButton);
            this.splitContainer2.Panel2.Controls.Add(this.nextButton);
            this.splitContainer2.Size = new System.Drawing.Size(482, 268);
            this.splitContainer2.SplitterDistance = 220;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.TabStop = false;
            // 
            // backButton
            // 
            this.backButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.backButton.Location = new System.Drawing.Point(152, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 7;
            this.backButton.Text = "<  Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // finishButton
            // 
            this.finishButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.finishButton.Location = new System.Drawing.Point(314, 12);
            this.finishButton.Name = "finishButton";
            this.finishButton.Size = new System.Drawing.Size(75, 23);
            this.finishButton.TabIndex = 9;
            this.finishButton.Text = "Finish";
            this.finishButton.UseVisualStyleBackColor = true;
            this.finishButton.Click += new System.EventHandler(this.finishButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point(395, 12);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nextButton.Location = new System.Drawing.Point(233, 12);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 8;
            this.nextButton.Text = "Next  >";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // wizardTabControl
            // 
            this.wizardTabControl.Controls.Add(this.operationTabPage);
            this.wizardTabControl.Controls.Add(this.entityTabPage);
            this.wizardTabControl.Controls.Add(this.fieldTabPage);
            this.wizardTabControl.Controls.Add(this.ownerTabPage);
            this.wizardTabControl.Controls.Add(this.relatedEntityTabPage);
            this.wizardTabControl.Controls.Add(this.stateTabPage);
            this.wizardTabControl.Controls.Add(this.templateTabPage);
            this.wizardTabControl.Controls.Add(this.fetchXmlTabPage);
            this.wizardTabControl.Controls.Add(this.finishTabPage);
            this.wizardTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardTabControl.Location = new System.Drawing.Point(0, 0);
            this.wizardTabControl.Multiline = true;
            this.wizardTabControl.Name = "wizardTabControl";
            this.wizardTabControl.Padding = new System.Drawing.Point(0, 0);
            this.wizardTabControl.SelectedIndex = 0;
            this.wizardTabControl.Size = new System.Drawing.Size(482, 220);
            this.wizardTabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.wizardTabControl.TabIndex = 6;
            // 
            // operationTabPage
            // 
            this.operationTabPage.BackColor = System.Drawing.Color.Transparent;
            this.operationTabPage.Controls.Add(this.operationComboBox);
            this.operationTabPage.Controls.Add(this.label1);
            this.operationTabPage.Location = new System.Drawing.Point(4, 58);
            this.operationTabPage.Name = "operationTabPage";
            this.operationTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.operationTabPage.Size = new System.Drawing.Size(474, 158);
            this.operationTabPage.TabIndex = 1;
            this.operationTabPage.Text = "Operation";
            // 
            // operationComboBox
            // 
            this.operationComboBox.FormattingEnabled = true;
            this.operationComboBox.Items.AddRange(new object[] {
            "Activate",
            "Assign",
            "Associate",
            "Create",
            "Deactivate",
            "Delete",
            "Disassociate",
            "Publish",
            "Publish All",
            "Set State",
            "Update",
            "Upsert"});
            this.operationComboBox.Location = new System.Drawing.Point(23, 50);
            this.operationComboBox.Name = "operationComboBox";
            this.operationComboBox.Size = new System.Drawing.Size(100, 21);
            this.operationComboBox.TabIndex = 0;
            this.operationComboBox.Text = "Activate";
            this.operationComboBox.SelectedIndexChanged += new System.EventHandler(this.operationComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "What operation do you want to perform?";
            // 
            // entityTabPage
            // 
            this.entityTabPage.BackColor = System.Drawing.Color.Transparent;
            this.entityTabPage.Controls.Add(this.entityLoadingPictureBox);
            this.entityTabPage.Controls.Add(this.entityComboBox);
            this.entityTabPage.Controls.Add(this.recordsLabel);
            this.entityTabPage.Location = new System.Drawing.Point(4, 58);
            this.entityTabPage.Name = "entityTabPage";
            this.entityTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.entityTabPage.Size = new System.Drawing.Size(474, 158);
            this.entityTabPage.TabIndex = 2;
            this.entityTabPage.Text = "Entity";
            // 
            // entityLoadingPictureBox
            // 
            this.entityLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.entityLoadingPictureBox.Location = new System.Drawing.Point(224, 52);
            this.entityLoadingPictureBox.Name = "entityLoadingPictureBox";
            this.entityLoadingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.entityLoadingPictureBox.TabIndex = 3;
            this.entityLoadingPictureBox.TabStop = false;
            // 
            // entityComboBox
            // 
            this.entityComboBox.FormattingEnabled = true;
            this.entityComboBox.Location = new System.Drawing.Point(23, 50);
            this.entityComboBox.Name = "entityComboBox";
            this.entityComboBox.Size = new System.Drawing.Size(195, 21);
            this.entityComboBox.TabIndex = 0;
            this.entityComboBox.SelectedIndexChanged += new System.EventHandler(this.entityComboBox_SelectedIndexChanged);
            // 
            // recordsLabel
            // 
            this.recordsLabel.AutoSize = true;
            this.recordsLabel.Location = new System.Drawing.Point(20, 20);
            this.recordsLabel.Name = "recordsLabel";
            this.recordsLabel.Size = new System.Drawing.Size(258, 13);
            this.recordsLabel.TabIndex = 2;
            this.recordsLabel.Text = "What entity do you want to perform the operation on?";
            // 
            // fieldTabPage
            // 
            this.fieldTabPage.BackColor = System.Drawing.Color.Transparent;
            this.fieldTabPage.Controls.Add(this.selectNoneButton);
            this.fieldTabPage.Controls.Add(this.selectAllButton);
            this.fieldTabPage.Controls.Add(this.fieldsLoadingPictureBox);
            this.fieldTabPage.Controls.Add(this.fieldsCheckedListBox);
            this.fieldTabPage.Controls.Add(this.fieldsLabel);
            this.fieldTabPage.Location = new System.Drawing.Point(4, 58);
            this.fieldTabPage.Name = "fieldTabPage";
            this.fieldTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.fieldTabPage.Size = new System.Drawing.Size(474, 158);
            this.fieldTabPage.TabIndex = 8;
            this.fieldTabPage.Text = "Field";
            // 
            // selectNoneButton
            // 
            this.selectNoneButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectNoneButton.Location = new System.Drawing.Point(310, 120);
            this.selectNoneButton.Name = "selectNoneButton";
            this.selectNoneButton.Size = new System.Drawing.Size(75, 23);
            this.selectNoneButton.TabIndex = 7;
            this.selectNoneButton.Text = "Select None";
            this.selectNoneButton.UseVisualStyleBackColor = true;
            this.selectNoneButton.Click += new System.EventHandler(this.selectNoneButton_Click);
            // 
            // selectAllButton
            // 
            this.selectAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectAllButton.Location = new System.Drawing.Point(310, 91);
            this.selectAllButton.Name = "selectAllButton";
            this.selectAllButton.Size = new System.Drawing.Size(75, 23);
            this.selectAllButton.TabIndex = 6;
            this.selectAllButton.Text = "Select All";
            this.selectAllButton.UseVisualStyleBackColor = true;
            this.selectAllButton.Click += new System.EventHandler(this.selectAllButton_Click);
            // 
            // fieldsLoadingPictureBox
            // 
            this.fieldsLoadingPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fieldsLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.fieldsLoadingPictureBox.Location = new System.Drawing.Point(310, 50);
            this.fieldsLoadingPictureBox.Name = "fieldsLoadingPictureBox";
            this.fieldsLoadingPictureBox.Size = new System.Drawing.Size(16, 0);
            this.fieldsLoadingPictureBox.TabIndex = 5;
            this.fieldsLoadingPictureBox.TabStop = false;
            // 
            // fieldsCheckedListBox
            // 
            this.fieldsCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fieldsCheckedListBox.CheckOnClick = true;
            this.fieldsCheckedListBox.FormattingEnabled = true;
            this.fieldsCheckedListBox.Location = new System.Drawing.Point(23, 50);
            this.fieldsCheckedListBox.Name = "fieldsCheckedListBox";
            this.fieldsCheckedListBox.Size = new System.Drawing.Size(281, 94);
            this.fieldsCheckedListBox.TabIndex = 4;
            // 
            // fieldsLabel
            // 
            this.fieldsLabel.AutoSize = true;
            this.fieldsLabel.Location = new System.Drawing.Point(20, 20);
            this.fieldsLabel.Name = "fieldsLabel";
            this.fieldsLabel.Size = new System.Drawing.Size(133, 13);
            this.fieldsLabel.TabIndex = 3;
            this.fieldsLabel.Text = "What fields do you want to";
            // 
            // ownerTabPage
            // 
            this.ownerTabPage.BackColor = System.Drawing.Color.Transparent;
            this.ownerTabPage.Controls.Add(this.label2);
            this.ownerTabPage.Controls.Add(this.teamLoadingPictureBox);
            this.ownerTabPage.Controls.Add(this.userLoadingPictureBox);
            this.ownerTabPage.Controls.Add(this.teamRadioButton);
            this.ownerTabPage.Controls.Add(this.userRadioButton);
            this.ownerTabPage.Controls.Add(this.teamComboBox);
            this.ownerTabPage.Controls.Add(this.userComboBox);
            this.ownerTabPage.Location = new System.Drawing.Point(4, 58);
            this.ownerTabPage.Name = "ownerTabPage";
            this.ownerTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ownerTabPage.Size = new System.Drawing.Size(474, 158);
            this.ownerTabPage.TabIndex = 3;
            this.ownerTabPage.Text = "Owner";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(269, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "What user or team do you want to assign ownership to?";
            // 
            // teamLoadingPictureBox
            // 
            this.teamLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.teamLoadingPictureBox.Location = new System.Drawing.Point(208, 84);
            this.teamLoadingPictureBox.Name = "teamLoadingPictureBox";
            this.teamLoadingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.teamLoadingPictureBox.TabIndex = 9;
            this.teamLoadingPictureBox.TabStop = false;
            // 
            // userLoadingPictureBox
            // 
            this.userLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.userLoadingPictureBox.Location = new System.Drawing.Point(209, 52);
            this.userLoadingPictureBox.Name = "userLoadingPictureBox";
            this.userLoadingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.userLoadingPictureBox.TabIndex = 8;
            this.userLoadingPictureBox.TabStop = false;
            // 
            // teamRadioButton
            // 
            this.teamRadioButton.AutoSize = true;
            this.teamRadioButton.Location = new System.Drawing.Point(23, 83);
            this.teamRadioButton.Name = "teamRadioButton";
            this.teamRadioButton.Size = new System.Drawing.Size(52, 17);
            this.teamRadioButton.TabIndex = 2;
            this.teamRadioButton.Text = "Team";
            this.teamRadioButton.UseVisualStyleBackColor = true;
            this.teamRadioButton.CheckedChanged += new System.EventHandler(this.teamRadioButton_CheckedChanged);
            // 
            // userRadioButton
            // 
            this.userRadioButton.AutoSize = true;
            this.userRadioButton.Checked = true;
            this.userRadioButton.Location = new System.Drawing.Point(23, 51);
            this.userRadioButton.Name = "userRadioButton";
            this.userRadioButton.Size = new System.Drawing.Size(47, 17);
            this.userRadioButton.TabIndex = 2;
            this.userRadioButton.TabStop = true;
            this.userRadioButton.Text = "User";
            this.userRadioButton.UseVisualStyleBackColor = true;
            this.userRadioButton.CheckedChanged += new System.EventHandler(this.userRadioButton_CheckedChanged);
            // 
            // teamComboBox
            // 
            this.teamComboBox.Enabled = false;
            this.teamComboBox.FormattingEnabled = true;
            this.teamComboBox.Location = new System.Drawing.Point(81, 82);
            this.teamComboBox.Name = "teamComboBox";
            this.teamComboBox.Size = new System.Drawing.Size(121, 21);
            this.teamComboBox.TabIndex = 0;
            // 
            // userComboBox
            // 
            this.userComboBox.FormattingEnabled = true;
            this.userComboBox.Location = new System.Drawing.Point(82, 50);
            this.userComboBox.Name = "userComboBox";
            this.userComboBox.Size = new System.Drawing.Size(121, 21);
            this.userComboBox.TabIndex = 0;
            // 
            // relatedEntityTabPage
            // 
            this.relatedEntityTabPage.BackColor = System.Drawing.Color.Transparent;
            this.relatedEntityTabPage.Controls.Add(this.relatedEntityLoadingPictureBox);
            this.relatedEntityTabPage.Controls.Add(this.relationshipLoadingPictureBox);
            this.relatedEntityTabPage.Controls.Add(this.relationshipComboBox);
            this.relatedEntityTabPage.Controls.Add(this.label7);
            this.relatedEntityTabPage.Controls.Add(this.relatedEntityComboBox);
            this.relatedEntityTabPage.Controls.Add(this.label6);
            this.relatedEntityTabPage.Location = new System.Drawing.Point(4, 58);
            this.relatedEntityTabPage.Name = "relatedEntityTabPage";
            this.relatedEntityTabPage.Size = new System.Drawing.Size(474, 158);
            this.relatedEntityTabPage.TabIndex = 5;
            this.relatedEntityTabPage.Text = "Related Entity";
            // 
            // relatedEntityLoadingPictureBox
            // 
            this.relatedEntityLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.relatedEntityLoadingPictureBox.Location = new System.Drawing.Point(224, 52);
            this.relatedEntityLoadingPictureBox.Name = "relatedEntityLoadingPictureBox";
            this.relatedEntityLoadingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.relatedEntityLoadingPictureBox.TabIndex = 7;
            this.relatedEntityLoadingPictureBox.TabStop = false;
            // 
            // relationshipLoadingPictureBox
            // 
            this.relationshipLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.relationshipLoadingPictureBox.Location = new System.Drawing.Point(224, 122);
            this.relationshipLoadingPictureBox.Name = "relationshipLoadingPictureBox";
            this.relationshipLoadingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.relationshipLoadingPictureBox.TabIndex = 7;
            this.relationshipLoadingPictureBox.TabStop = false;
            // 
            // relationshipComboBox
            // 
            this.relationshipComboBox.FormattingEnabled = true;
            this.relationshipComboBox.Items.AddRange(new object[] {
            "account",
            "contact",
            "new_entityname1",
            "new_entityname2"});
            this.relationshipComboBox.Location = new System.Drawing.Point(23, 120);
            this.relationshipComboBox.Name = "relationshipComboBox";
            this.relationshipComboBox.Size = new System.Drawing.Size(195, 21);
            this.relationshipComboBox.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "What relationship should be used?";
            // 
            // relatedEntityComboBox
            // 
            this.relatedEntityComboBox.FormattingEnabled = true;
            this.relatedEntityComboBox.Location = new System.Drawing.Point(23, 50);
            this.relatedEntityComboBox.Name = "relatedEntityComboBox";
            this.relatedEntityComboBox.Size = new System.Drawing.Size(195, 21);
            this.relatedEntityComboBox.TabIndex = 3;
            this.relatedEntityComboBox.SelectedIndexChanged += new System.EventHandler(this.relatedEntitiyComboBox_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(283, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "What entity do you want to associate or disassociate with?";
            // 
            // stateTabPage
            // 
            this.stateTabPage.BackColor = System.Drawing.Color.Transparent;
            this.stateTabPage.Controls.Add(this.label11);
            this.stateTabPage.Controls.Add(this.label10);
            this.stateTabPage.Controls.Add(this.label8);
            this.stateTabPage.Controls.Add(this.statusLoadingPictureBox);
            this.stateTabPage.Controls.Add(this.stateLoadingPictureBox);
            this.stateTabPage.Controls.Add(this.statusComboBox);
            this.stateTabPage.Controls.Add(this.stateComboBox);
            this.stateTabPage.Location = new System.Drawing.Point(4, 58);
            this.stateTabPage.Name = "stateTabPage";
            this.stateTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.stateTabPage.Size = new System.Drawing.Size(474, 158);
            this.stateTabPage.TabIndex = 7;
            this.stateTabPage.Text = "State";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Status";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "State";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(207, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "What state and status do you want to set?";
            // 
            // statusLoadingPictureBox
            // 
            this.statusLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.statusLoadingPictureBox.Location = new System.Drawing.Point(189, 84);
            this.statusLoadingPictureBox.Name = "statusLoadingPictureBox";
            this.statusLoadingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.statusLoadingPictureBox.TabIndex = 16;
            this.statusLoadingPictureBox.TabStop = false;
            // 
            // stateLoadingPictureBox
            // 
            this.stateLoadingPictureBox.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Loading;
            this.stateLoadingPictureBox.Location = new System.Drawing.Point(190, 52);
            this.stateLoadingPictureBox.Name = "stateLoadingPictureBox";
            this.stateLoadingPictureBox.Size = new System.Drawing.Size(16, 16);
            this.stateLoadingPictureBox.TabIndex = 15;
            this.stateLoadingPictureBox.TabStop = false;
            // 
            // statusComboBox
            // 
            this.statusComboBox.FormattingEnabled = true;
            this.statusComboBox.Location = new System.Drawing.Point(63, 82);
            this.statusComboBox.Name = "statusComboBox";
            this.statusComboBox.Size = new System.Drawing.Size(121, 21);
            this.statusComboBox.TabIndex = 12;
            // 
            // stateComboBox
            // 
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Location = new System.Drawing.Point(63, 50);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(121, 21);
            this.stateComboBox.TabIndex = 11;
            this.stateComboBox.SelectedIndexChanged += new System.EventHandler(this.stateComboBox_SelectedIndexChanged);
            // 
            // templateTabPage
            // 
            this.templateTabPage.BackColor = System.Drawing.Color.Transparent;
            this.templateTabPage.Controls.Add(this.fetchXmlStaticRadioButton);
            this.templateTabPage.Controls.Add(this.fetchXmlDynamicRadioButton);
            this.templateTabPage.Controls.Add(this.label3);
            this.templateTabPage.Controls.Add(this.excelRadioButton);
            this.templateTabPage.Controls.Add(this.sqlStaticRadioButton);
            this.templateTabPage.Controls.Add(this.sqlDynamicRadioButton);
            this.templateTabPage.Location = new System.Drawing.Point(4, 58);
            this.templateTabPage.Name = "templateTabPage";
            this.templateTabPage.Size = new System.Drawing.Size(474, 158);
            this.templateTabPage.TabIndex = 6;
            this.templateTabPage.Text = "Template";
            // 
            // fetchXmlStaticRadioButton
            // 
            this.fetchXmlStaticRadioButton.AutoSize = true;
            this.fetchXmlStaticRadioButton.Location = new System.Drawing.Point(23, 171);
            this.fetchXmlStaticRadioButton.Name = "fetchXmlStaticRadioButton";
            this.fetchXmlStaticRadioButton.Size = new System.Drawing.Size(312, 17);
            this.fetchXmlStaticRadioButton.TabIndex = 9;
            this.fetchXmlStaticRadioButton.Text = "FetchXML (Static) - for CRM Online or On-Premise operations";
            this.fetchXmlStaticRadioButton.UseVisualStyleBackColor = true;
            this.fetchXmlStaticRadioButton.Visible = false;
            // 
            // fetchXmlDynamicRadioButton
            // 
            this.fetchXmlDynamicRadioButton.AutoSize = true;
            this.fetchXmlDynamicRadioButton.Location = new System.Drawing.Point(23, 141);
            this.fetchXmlDynamicRadioButton.Name = "fetchXmlDynamicRadioButton";
            this.fetchXmlDynamicRadioButton.Size = new System.Drawing.Size(326, 17);
            this.fetchXmlDynamicRadioButton.TabIndex = 9;
            this.fetchXmlDynamicRadioButton.Text = "FetchXML (Dynamic) - for CRM Online or On-Premise operations";
            this.fetchXmlDynamicRadioButton.UseVisualStyleBackColor = true;
            this.fetchXmlDynamicRadioButton.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "What type of job do you want to create?";
            // 
            // excelRadioButton
            // 
            this.excelRadioButton.AutoSize = true;
            this.excelRadioButton.Location = new System.Drawing.Point(23, 111);
            this.excelRadioButton.Name = "excelRadioButton";
            this.excelRadioButton.Size = new System.Drawing.Size(281, 17);
            this.excelRadioButton.TabIndex = 7;
            this.excelRadioButton.Text = "Excel - for basic CRM Online or On-Premise operations";
            this.excelRadioButton.UseVisualStyleBackColor = true;
            // 
            // sqlStaticRadioButton
            // 
            this.sqlStaticRadioButton.AutoSize = true;
            this.sqlStaticRadioButton.Location = new System.Drawing.Point(23, 81);
            this.sqlStaticRadioButton.Name = "sqlStaticRadioButton";
            this.sqlStaticRadioButton.Size = new System.Drawing.Size(346, 17);
            this.sqlStaticRadioButton.TabIndex = 6;
            this.sqlStaticRadioButton.Text = "T-SQL (Static) - for basic CRM On-Premise data migration operations";
            this.sqlStaticRadioButton.UseVisualStyleBackColor = true;
            // 
            // sqlDynamicRadioButton
            // 
            this.sqlDynamicRadioButton.AutoSize = true;
            this.sqlDynamicRadioButton.Checked = true;
            this.sqlDynamicRadioButton.Location = new System.Drawing.Point(23, 51);
            this.sqlDynamicRadioButton.Name = "sqlDynamicRadioButton";
            this.sqlDynamicRadioButton.Size = new System.Drawing.Size(305, 17);
            this.sqlDynamicRadioButton.TabIndex = 5;
            this.sqlDynamicRadioButton.TabStop = true;
            this.sqlDynamicRadioButton.Text = "T-SQL (Dynamic) - for complex CRM On-Premise operations\r\n";
            this.sqlDynamicRadioButton.UseVisualStyleBackColor = true;
            // 
            // fetchXmlTabPage
            // 
            this.fetchXmlTabPage.BackColor = System.Drawing.Color.Transparent;
            this.fetchXmlTabPage.Controls.Add(this.fetchXmlTextBox);
            this.fetchXmlTabPage.Controls.Add(this.label13);
            this.fetchXmlTabPage.Location = new System.Drawing.Point(4, 58);
            this.fetchXmlTabPage.Name = "fetchXmlTabPage";
            this.fetchXmlTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.fetchXmlTabPage.Size = new System.Drawing.Size(474, 158);
            this.fetchXmlTabPage.TabIndex = 9;
            this.fetchXmlTabPage.Text = "Fetch XML";
            // 
            // fetchXmlTextBox
            // 
            this.fetchXmlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fetchXmlTextBox.Location = new System.Drawing.Point(23, 50);
            this.fetchXmlTextBox.Multiline = true;
            this.fetchXmlTextBox.Name = "fetchXmlTextBox";
            this.fetchXmlTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.fetchXmlTextBox.Size = new System.Drawing.Size(430, 94);
            this.fetchXmlTextBox.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(195, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "What FetchXML query should be used?";
            // 
            // finishTabPage
            // 
            this.finishTabPage.BackColor = System.Drawing.Color.Transparent;
            this.finishTabPage.Controls.Add(this.label12);
            this.finishTabPage.Controls.Add(this.label9);
            this.finishTabPage.Location = new System.Drawing.Point(4, 58);
            this.finishTabPage.Name = "finishTabPage";
            this.finishTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.finishTabPage.Size = new System.Drawing.Size(474, 158);
            this.finishTabPage.TabIndex = 4;
            this.finishTabPage.Text = "Finish";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(446, 26);
            this.label12.TabIndex = 0;
            this.label12.Text = "Wizard Complete!";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Location = new System.Drawing.Point(20, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(446, 64);
            this.label9.TabIndex = 0;
            this.label9.Text = "Update and close the job import file then select \'Finish\' to import the job into " +
    "the current batch.";
            // 
            // JobImportWizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 329);
            this.Controls.Add(this.splitContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "JobImportWizardForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Import Wizard";
            this.Load += new System.EventHandler(this.JobImportWizardForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.wizardTabControl.ResumeLayout(false);
            this.operationTabPage.ResumeLayout(false);
            this.operationTabPage.PerformLayout();
            this.entityTabPage.ResumeLayout(false);
            this.entityTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.entityLoadingPictureBox)).EndInit();
            this.fieldTabPage.ResumeLayout(false);
            this.fieldTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fieldsLoadingPictureBox)).EndInit();
            this.ownerTabPage.ResumeLayout(false);
            this.ownerTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamLoadingPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLoadingPictureBox)).EndInit();
            this.relatedEntityTabPage.ResumeLayout(false);
            this.relatedEntityTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.relatedEntityLoadingPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relationshipLoadingPictureBox)).EndInit();
            this.stateTabPage.ResumeLayout(false);
            this.stateTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusLoadingPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateLoadingPictureBox)).EndInit();
            this.templateTabPage.ResumeLayout(false);
            this.templateTabPage.PerformLayout();
            this.fetchXmlTabPage.ResumeLayout(false);
            this.fetchXmlTabPage.PerformLayout();
            this.finishTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button finishButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Controls.HiddenHeaderTabControl wizardTabControl;
        private System.Windows.Forms.TabPage operationTabPage;
        private System.Windows.Forms.ComboBox operationComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage entityTabPage;
        private System.Windows.Forms.ComboBox entityComboBox;
        private System.Windows.Forms.Label recordsLabel;
        private System.Windows.Forms.TabPage ownerTabPage;
        private System.Windows.Forms.ComboBox teamComboBox;
        private System.Windows.Forms.ComboBox userComboBox;
        private System.Windows.Forms.TabPage relatedEntityTabPage;
        private System.Windows.Forms.TabPage templateTabPage;
        private System.Windows.Forms.TabPage finishTabPage;
        private System.Windows.Forms.ComboBox relationshipComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox relatedEntityComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton excelRadioButton;
        private System.Windows.Forms.RadioButton sqlStaticRadioButton;
        private System.Windows.Forms.RadioButton sqlDynamicRadioButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton userRadioButton;
        private System.Windows.Forms.RadioButton teamRadioButton;
        private System.Windows.Forms.PictureBox entityLoadingPictureBox;
        private System.Windows.Forms.PictureBox relationshipLoadingPictureBox;
        private System.Windows.Forms.PictureBox relatedEntityLoadingPictureBox;
        private System.Windows.Forms.PictureBox teamLoadingPictureBox;
        private System.Windows.Forms.PictureBox userLoadingPictureBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage stateTabPage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox statusLoadingPictureBox;
        private System.Windows.Forms.PictureBox stateLoadingPictureBox;
        private System.Windows.Forms.ComboBox statusComboBox;
        private System.Windows.Forms.ComboBox stateComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage fieldTabPage;
        private System.Windows.Forms.Label fieldsLabel;
        private System.Windows.Forms.CheckedListBox fieldsCheckedListBox;
        private System.Windows.Forms.PictureBox fieldsLoadingPictureBox;
        private System.Windows.Forms.Button selectNoneButton;
        private System.Windows.Forms.Button selectAllButton;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton fetchXmlStaticRadioButton;
        private System.Windows.Forms.RadioButton fetchXmlDynamicRadioButton;
        private System.Windows.Forms.TabPage fetchXmlTabPage;
        private System.Windows.Forms.TextBox fetchXmlTextBox;
        private System.Windows.Forms.Label label13;
    }
}