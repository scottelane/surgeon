﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScottLane.Surgeon.Client.Helpers;
using ScottLane.Surgeon.Client.Resources;
using ScottLane.Surgeon.Common.Resources;
using ScottLane.Surgeon.Model;

namespace ScottLane.Surgeon.Client.Forms
{
    /// <summary>
    /// Allows CRM and SQL connections to be created and edited.
    /// </summary>
    public partial class ConnectionDetailsForm : Form
    {
        private bool asyncTestingInProgress;
        private ConnectionEditMode editMode;
        private Connection connection;
        private bool validationEnabled;

        /// <summary>
        /// Gets the Connection being displayed on the form.
        /// </summary>
        public Connection Connection
        {
            get { return connection; }
        }

        /// <summary>
        /// Gets a value indicating whether the connection just created or edited should be set as the current connection.
        /// </summary>
        public bool SetAsCurrentConnection
        {
            get { return setAsCurrentConnection.Checked; }
        }

        /// <summary>
        /// Initialises a new instance of the ConnectionForm class.
        /// </summary>
        public ConnectionDetailsForm()
        {
            InitializeComponent();

            asyncTestingInProgress = false;
            formHelpProvider.HelpNamespace = CommonStrings.HelpConnectionDetailsFormUrl;
            editMode = ConnectionEditMode.Create;
            validationEnabled = false;
        }

        /// <summary>
        /// Initializes a new instance of the ConnectionDetailsForm class to edit the specified connection.
        /// </summary>
        /// <param name="connection">The conncetion.</param>
        public ConnectionDetailsForm(Connection connection)
            : this()
        {
            this.connection = connection;
            editMode = ConnectionEditMode.Edit;
        }

        /// <summary>
        /// Popuulates form default values;
        /// </summary>
        private void ConnectionForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (connection != null)
                {
                    // load existing connection details
                    Icon = Properties.Resources.ConnectionEditIcon;
                    nameTextBox.Text = connection.Name;
                    organisationServiceUrlTextBox.Text = connection.OrganisationServiceUri.ToString();
                    crmTimeoutTextBox.Text = connection.CrmTimeout.TotalSeconds.ToString();
                    windowsIntegratedAuthenticationRadioButton.Checked = (connection.AuthenticationType == AuthenticationType.WindowsIntegrated);
                    customAuthenticationRadioButton.Checked = (connection.AuthenticationType == AuthenticationType.Custom);
                    domainTextBox.Text = connection.Domain;
                    userNameTextBox.Text = connection.UserName;
                    passwordTextBox.Text = Connection.Decrypt(connection.Password);
                    sqlConnectionStringTextBox.Text = Connection.Decrypt(connection.ConnectionString);
                    sqlTimeoutTextBox.Text = connection.SqlTimeout.TotalSeconds.ToString();
                }
                else
                {
                    // show default values for new connection
                    Icon = Properties.Resources.ConnectionCreateIcon;
                    crmTimeoutTextBox.Text = Connection.DefaultCrmTimeout.TotalSeconds.ToString();
                    windowsIntegratedAuthenticationRadioButton.Checked = true;
                    setAsCurrentConnection.Checked = true;
                    sqlTimeoutTextBox.Text = Connection.DefaultSqlTimeout.TotalSeconds.ToString();
                }

                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Validates the organisation name.
        /// </summary>
        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ValidateConnectionName();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Validates and enables controls appropriate for the organisation service url.
        /// </summary>
        private void organisationServiceUrlTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ValidateOrganisationServiceUrl();
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Validates and enables controls appropriate for the CRM timeout.
        /// </summary>
        private void crmTimeoutTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ValidateCrmTimeout();
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Validates and enables controls appropriate for the windows authentication method.
        /// </summary>
        private void windowsIntegratedAuthenticationRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ValidateUserName();
                ValidatePassword();
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Validates and enables controls appropriate for the custom authentication method.
        /// </summary>
        private void customAuthenticationRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ValidateUserName();
                ValidatePassword();
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Enables controls appropriate for the domain.
        /// </summary>
        private void domainTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Valiates and enables controls appropriate for the user name.
        /// </summary>
        private void userNameTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ValidateUserName();
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Validates and enables controls appropriate for the password.
        /// </summary>
        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ValidatePassword();
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Enables controls appropriate for the SQL connection string.
        /// </summary>
        private void sqlConnectionStringTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Validates and enables controls appropriate for the SQL timeout.
        /// </summary>
        private void sqlTimeoutTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ValidateSqlTimeout();
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Tests the connection.
        /// </summary>
        private void testConnectionButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateForm())
                {
                    TestConnection();
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Asynchronously tests the connection.
        /// </summary>
        private async void TestConnection()
        {
            try
            {
                validationEnabled = true;

                if (ValidateForm())
                {
                    asyncTestingInProgress = true;
                    RefreshFormControls();
                    Connection newConnection = CreateNewConnection();
                    ConnectionResult result = await Task.Run(() => newConnection.Connect());
                    asyncTestingInProgress = false;
                    RefreshFormControls();

                    MessageBoxIcon icon = result.HasFlag(ConnectionResult.CrmConnectionFailed) ? MessageBoxIcon.Error : MessageBoxIcon.Information;
                    string caption = result.HasFlag(ConnectionResult.CrmConnectionFailed) ? ClientStrings.ConnectionFormTestFailedCaption : ClientStrings.ConnectionFormTestSuccessful;
                    string crmMessage = result.HasFlag(ConnectionResult.CrmConnectionFailed) ? ClientStrings.ConnectionFormTestCrmFailedText : ClientStrings.ConnectionFormTestCrmSuccessfulText;
                    string sqlMessage = result.HasFlag(ConnectionResult.SqlConnectionFailed) ? ClientStrings.ConnectionFormTestSqlFailedText : result.HasFlag(ConnectionResult.SqlConnectionSucceeded) ? ClientStrings.ConnectionFormTestSqlSuccessfulText : ClientStrings.ConnectionFormTestSqlSkippedText;
                    string message = string.Format("{0}{1}{2}", crmMessage, Environment.NewLine, sqlMessage);
                    MessageBox.Show(message, caption, MessageBoxButtons.OK, icon);
                }
            }
            catch { }
            finally
            {
                asyncTestingInProgress = false;
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Cancels any changes and closes the form.
        /// </summary>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Saves the connection details and closes the form.
        /// </summary>
        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                validationEnabled = true;

                if (ValidateForm())
                {
                    Connection newConnection = CreateNewConnection();

                    if (editMode == ConnectionEditMode.Create)
                    {
                        connection = newConnection;
                    }
                    else
                    {
                        // todo - find better way to do this
                        connection.AuthenticationType = newConnection.AuthenticationType;
                        connection.ConnectionString = newConnection.ConnectionString;
                        connection.CrmTimeout = newConnection.CrmTimeout;
                        connection.Domain = newConnection.Domain;
                        connection.Name = newConnection.Name;
                        connection.OrganisationServiceUri = newConnection.OrganisationServiceUri;
                        connection.Password = newConnection.Password;
                        connection.UserName = newConnection.UserName;
                        connection.SqlTimeout = newConnection.SqlTimeout;
                    }

                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Creates a new connection using the form control values.
        /// </summary>
        /// <returns>The new connection.</returns>
        private Connection CreateNewConnection()
        {
            Connection newConnection = new Connection();
            newConnection.Name = nameTextBox.Text;
            newConnection.OrganisationServiceUri = new Uri(organisationServiceUrlTextBox.Text);
            newConnection.CrmTimeout = TimeSpan.FromSeconds(double.Parse(crmTimeoutTextBox.Text));

            if (windowsIntegratedAuthenticationRadioButton.Checked)
            {
                newConnection.AuthenticationType = AuthenticationType.WindowsIntegrated;
            }
            else if (customAuthenticationRadioButton.Checked)
            {
                newConnection.AuthenticationType = AuthenticationType.Custom;
            }

            newConnection.Domain = domainTextBox.Text;
            newConnection.UserName = userNameTextBox.Text;
            newConnection.Password = Connection.Encrypt(passwordTextBox.Text);
            newConnection.ConnectionString = Connection.Encrypt(sqlConnectionStringTextBox.Text);
            newConnection.SqlTimeout = TimeSpan.FromSeconds(double.Parse(sqlTimeoutTextBox.Text));

            return newConnection;
        }

        #region Validation

        /// <summary>
        /// Validates the CRM timeout.
        /// </summary>
        private bool ValidateCrmTimeout()
        {
            bool result = true;

            formErrorProvider.SetError(crmTimeoutTextBox, string.Empty);
            int value;

            if (validationEnabled && (!int.TryParse(crmTimeoutTextBox.Text, out value) || value <= 0 || value > 3600))
            {
                formErrorProvider.SetIconPadding(crmTimeoutTextBox, -20);
                formErrorProvider.SetError(crmTimeoutTextBox, ClientStrings.ConnectionFormCrmTimeoutInvalidMessage);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Validates the organisation name.
        /// </summary>
        private bool ValidateConnectionName()
        {
            bool result = true;

            formErrorProvider.SetError(nameTextBox, string.Empty);

            if (validationEnabled && (nameTextBox.Text.Length == 0))
            {
                formErrorProvider.SetIconPadding(nameTextBox, -20);
                formErrorProvider.SetError(nameTextBox, ClientStrings.ConnectionFormConnectionNameErrorMessage);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Validates the organisation service url.
        /// </summary>
        private bool ValidateOrganisationServiceUrl()
        {
            bool result = true;

            formErrorProvider.SetError(organisationServiceUrlTextBox, string.Empty);

            if (validationEnabled && (!Uri.IsWellFormedUriString(organisationServiceUrlTextBox.Text, UriKind.Absolute)))
            {
                formErrorProvider.SetIconPadding(organisationServiceUrlTextBox, -20);
                formErrorProvider.SetError(organisationServiceUrlTextBox, ClientStrings.ConnectionFormServiceUrlErrorMessage);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Validates the user name.
        /// </summary>
        private bool ValidateUserName()
        {
            bool result = true;

            formErrorProvider.SetError(userNameTextBox, string.Empty);

            if (validationEnabled && (customAuthenticationRadioButton.Checked && userNameTextBox.Text == string.Empty))
            {
                formErrorProvider.SetIconPadding(userNameTextBox, -20);
                formErrorProvider.SetError(userNameTextBox, ClientStrings.ConnectionFormUserNameBlankMessage);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Validates the password.
        /// </summary>
        private bool ValidatePassword()
        {
            bool result = true;

            formErrorProvider.SetError(passwordTextBox, string.Empty);

            if (validationEnabled && (customAuthenticationRadioButton.Checked && passwordTextBox.Text == string.Empty))
            {
                formErrorProvider.SetIconPadding(passwordTextBox, -20);
                formErrorProvider.SetError(passwordTextBox, ClientStrings.ConnectionFormPasswordBlankMessage);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Validates the SQL timeout.
        /// </summary>
        private bool ValidateSqlTimeout()
        {
            bool result = true;

            formErrorProvider.SetError(sqlTimeoutTextBox, string.Empty);
            int value;

            if (validationEnabled && (!int.TryParse(sqlTimeoutTextBox.Text, out value) || value <= 0 || value > 86400))
            {
                formErrorProvider.SetIconPadding(sqlTimeoutTextBox, -20);
                formErrorProvider.SetError(sqlTimeoutTextBox, ClientStrings.ConnectionFormSqlTimeoutInvalidMessage);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Validates all form controls.
        /// </summary>
        /// <returns>True if the validation is successful, false otherwise.</returns>
        private bool ValidateForm()
        {
            bool result = true;

            if (!ValidateConnectionName()) { result = false; }
            if (!ValidateOrganisationServiceUrl()) { result = false; }
            if (!ValidateCrmTimeout()) { result = false; }
            if (!ValidateUserName()) { result = false; }
            if (!ValidatePassword()) { result = false; }
            if (!ValidateSqlTimeout()) { result = false; }

            return result;
        }

        #endregion

        /// <summary>
        /// Refreshes all form controls.
        /// </summary>
        private void RefreshFormControls()
        {
            try
            {
                nameTextBox.Enabled = !asyncTestingInProgress;
                organisationServiceUrlTextBox.Enabled = !asyncTestingInProgress;
                crmTimeoutTextBox.Enabled = !asyncTestingInProgress;
                windowsIntegratedAuthenticationRadioButton.Enabled = !asyncTestingInProgress;
                customAuthenticationRadioButton.Enabled = !asyncTestingInProgress;
                domainTextBox.Enabled = customAuthenticationRadioButton.Checked && !asyncTestingInProgress;
                userNameTextBox.Enabled = customAuthenticationRadioButton.Checked && !asyncTestingInProgress;
                passwordTextBox.Enabled = customAuthenticationRadioButton.Checked && !asyncTestingInProgress;
                sqlConnectionStringTextBox.Enabled = !asyncTestingInProgress;
                sqlTimeoutTextBox.Enabled = !asyncTestingInProgress;
                setAsCurrentConnection.Enabled = !asyncTestingInProgress;
                testingPictureBox.Visible = asyncTestingInProgress;
                testConnectionButton.Enabled = !asyncTestingInProgress;
                cancelButton.Enabled = !asyncTestingInProgress;
                saveButton.Enabled = !asyncTestingInProgress && ValidateForm();
            }
            catch { }
        }
    }
}
