﻿namespace ScottLane.Surgeon.Client.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.progressToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatusStrip = new System.Windows.Forms.StatusStrip();
            this.connectionDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.statusRichTextBox = new System.Windows.Forms.RichTextBox();
            this.jobDataGridView = new System.Windows.Forms.DataGridView();
            this.typeImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.batchStatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.progressColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newBatchToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.openBatchToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveBatchToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.importJobToolStripItem = new System.Windows.Forms.ToolStripDropDownButton();
            this.importJobFromSqlDynamicFromFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importJobFromSqlStaticFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importJobFromExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.jobImportWizardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stopToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.publishAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gettingStartedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formHelpProvider = new System.Windows.Forms.HelpProvider();
            this.connectionStatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jobDataGridView)).BeginInit();
            this.jobContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressToolStripStatusLabel
            // 
            this.progressToolStripStatusLabel.Name = "progressToolStripStatusLabel";
            this.progressToolStripStatusLabel.Size = new System.Drawing.Size(519, 17);
            this.progressToolStripStatusLabel.Spring = true;
            this.progressToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // connectionStatusStrip
            // 
            this.connectionStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionDropDownButton,
            this.progressToolStripStatusLabel});
            this.connectionStatusStrip.Location = new System.Drawing.Point(0, 390);
            this.connectionStatusStrip.Name = "connectionStatusStrip";
            this.connectionStatusStrip.Size = new System.Drawing.Size(649, 22);
            this.connectionStatusStrip.TabIndex = 17;
            this.connectionStatusStrip.Text = "statusStrip1";
            // 
            // connectionDropDownButton
            // 
            this.connectionDropDownButton.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Connection;
            this.connectionDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.connectionDropDownButton.Name = "connectionDropDownButton";
            this.connectionDropDownButton.Size = new System.Drawing.Size(115, 20);
            this.connectionDropDownButton.Text = "Not connected";
            // 
            // statusRichTextBox
            // 
            this.statusRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.statusRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.statusRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusRichTextBox.Location = new System.Drawing.Point(-2, -1);
            this.statusRichTextBox.Name = "statusRichTextBox";
            this.statusRichTextBox.ReadOnly = true;
            this.statusRichTextBox.Size = new System.Drawing.Size(652, 129);
            this.statusRichTextBox.TabIndex = 0;
            this.statusRichTextBox.Text = "";
            // 
            // jobDataGridView
            // 
            this.jobDataGridView.AllowUserToAddRows = false;
            this.jobDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.jobDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.jobDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.jobDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.typeImageColumn,
            this.nameColumn,
            this.batchStatusColumn,
            this.progressColumn});
            this.jobDataGridView.ContextMenuStrip = this.jobContextMenuStrip;
            this.jobDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jobDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.jobDataGridView.Location = new System.Drawing.Point(0, 0);
            this.jobDataGridView.Name = "jobDataGridView";
            this.jobDataGridView.ReadOnly = true;
            this.jobDataGridView.RowHeadersWidth = 20;
            this.jobDataGridView.RowTemplate.ReadOnly = true;
            this.jobDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.jobDataGridView.Size = new System.Drawing.Size(651, 230);
            this.jobDataGridView.TabIndex = 2;
            this.jobDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.jobDataGridView_CellFormatting);
            this.jobDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.jobDataGridView_DataError);
            this.jobDataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.jobDataGridView_UserDeletedRow);
            this.jobDataGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.jobDataGridView_MouseDown);
            // 
            // typeImageColumn
            // 
            this.typeImageColumn.DataPropertyName = "CompilationType";
            this.typeImageColumn.Frozen = true;
            this.typeImageColumn.HeaderText = "";
            this.typeImageColumn.MinimumWidth = 28;
            this.typeImageColumn.Name = "typeImageColumn";
            this.typeImageColumn.ReadOnly = true;
            this.typeImageColumn.Width = 28;
            // 
            // nameColumn
            // 
            this.nameColumn.DataPropertyName = "Name";
            this.nameColumn.Frozen = true;
            this.nameColumn.HeaderText = "Job Name";
            this.nameColumn.MinimumWidth = 300;
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.ReadOnly = true;
            this.nameColumn.Width = 400;
            // 
            // batchStatusColumn
            // 
            this.batchStatusColumn.DataPropertyName = "Status";
            this.batchStatusColumn.Frozen = true;
            this.batchStatusColumn.HeaderText = "Status";
            this.batchStatusColumn.Name = "batchStatusColumn";
            this.batchStatusColumn.ReadOnly = true;
            // 
            // progressColumn
            // 
            this.progressColumn.DataPropertyName = "Progress";
            this.progressColumn.Frozen = true;
            this.progressColumn.HeaderText = "Progress";
            this.progressColumn.Name = "progressColumn";
            this.progressColumn.ReadOnly = true;
            // 
            // jobContextMenuStrip
            // 
            this.jobContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.jobContextMenuStrip.Name = "jobContextMenuStrip";
            this.jobContextMenuStrip.Size = new System.Drawing.Size(153, 92);
            this.jobContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.jobContextMenuStrip_Opening);
            // 
            // moveUpToolStripMenuItem
            // 
            this.moveUpToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobMoveUp;
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.moveUpToolStripMenuItem.Text = "Move Up";
            this.moveUpToolStripMenuItem.Click += new System.EventHandler(this.moveUpToolStripMenuItem_Click);
            // 
            // moveDownToolStripMenuItem
            // 
            this.moveDownToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobMoveDown;
            this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
            this.moveDownToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.moveDownToolStripMenuItem.Text = "Move Down";
            this.moveDownToolStripMenuItem.Click += new System.EventHandler(this.moveDownToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobRemove;
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitContainer1.Panel1.Controls.Add(this.jobDataGridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.statusRichTextBox);
            this.splitContainer1.Size = new System.Drawing.Size(651, 380);
            this.splitContainer1.SplitterDistance = 230;
            this.splitContainer1.TabIndex = 16;
            this.splitContainer1.TabStop = false;
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBatchToolStripButton,
            this.toolStripSeparator7,
            this.openBatchToolStripButton,
            this.toolStripSeparator3,
            this.saveBatchToolStripButton,
            this.toolStripSeparator5,
            this.importJobToolStripItem,
            this.toolStripSeparator4,
            this.runToolStripButton,
            this.stopToolStripButton,
            this.toolStripSeparator2,
            this.publishAllToolStripButton,
            this.toolStripSeparator1,
            this.helpToolStripDropDownButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(649, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // newBatchToolStripButton
            // 
            this.newBatchToolStripButton.Image = global::ScottLane.Surgeon.Client.Properties.Resources.BatchNew;
            this.newBatchToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newBatchToolStripButton.Name = "newBatchToolStripButton";
            this.newBatchToolStripButton.Size = new System.Drawing.Size(84, 22);
            this.newBatchToolStripButton.Text = "New Batch";
            this.newBatchToolStripButton.ToolTipText = "Create a new batch";
            this.newBatchToolStripButton.Click += new System.EventHandler(this.newBatchToolStripButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // openBatchToolStripButton
            // 
            this.openBatchToolStripButton.Image = global::ScottLane.Surgeon.Client.Properties.Resources.BatchOpen;
            this.openBatchToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openBatchToolStripButton.Name = "openBatchToolStripButton";
            this.openBatchToolStripButton.Size = new System.Drawing.Size(89, 22);
            this.openBatchToolStripButton.Text = "Open Batch";
            this.openBatchToolStripButton.ToolTipText = "Open a batch";
            this.openBatchToolStripButton.Click += new System.EventHandler(this.openBatchToolStripButton_Click);
            // 
            // saveBatchToolStripButton
            // 
            this.saveBatchToolStripButton.Image = global::ScottLane.Surgeon.Client.Properties.Resources.BatchSave;
            this.saveBatchToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveBatchToolStripButton.Name = "saveBatchToolStripButton";
            this.saveBatchToolStripButton.Size = new System.Drawing.Size(84, 22);
            this.saveBatchToolStripButton.Text = "Save Batch";
            this.saveBatchToolStripButton.ToolTipText = "Save the current batch";
            this.saveBatchToolStripButton.Click += new System.EventHandler(this.saveBatchToolStripButton_Click);
            // 
            // importJobToolStripItem
            // 
            this.importJobToolStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importJobFromSqlDynamicFromFileToolStripMenuItem,
            this.importJobFromSqlStaticFileToolStripMenuItem,
            this.importJobFromExcelToolStripMenuItem,
            this.toolStripSeparator6,
            this.jobImportWizardToolStripMenuItem});
            this.importJobToolStripItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobImport;
            this.importJobToolStripItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.importJobToolStripItem.Name = "importJobToolStripItem";
            this.importJobToolStripItem.Size = new System.Drawing.Size(93, 22);
            this.importJobToolStripItem.Text = "Import Job";
            this.importJobToolStripItem.ToolTipText = "Import a job";
            // 
            // importJobFromSqlDynamicFromFileToolStripMenuItem
            // 
            this.importJobFromSqlDynamicFromFileToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobImportSqlDynamic;
            this.importJobFromSqlDynamicFromFileToolStripMenuItem.Name = "importJobFromSqlDynamicFromFileToolStripMenuItem";
            this.importJobFromSqlDynamicFromFileToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.importJobFromSqlDynamicFromFileToolStripMenuItem.Text = "From T-SQL (Dynamic)...";
            this.importJobFromSqlDynamicFromFileToolStripMenuItem.ToolTipText = "Import a job from a sql script that generates operations at run time";
            this.importJobFromSqlDynamicFromFileToolStripMenuItem.Click += new System.EventHandler(this.fromSqlDynamicToolStripMenuItem_Click);
            // 
            // importJobFromSqlStaticFileToolStripMenuItem
            // 
            this.importJobFromSqlStaticFileToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobImportSqlStatic;
            this.importJobFromSqlStaticFileToolStripMenuItem.Name = "importJobFromSqlStaticFileToolStripMenuItem";
            this.importJobFromSqlStaticFileToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.importJobFromSqlStaticFileToolStripMenuItem.Text = "From T-SQL (Static)...";
            this.importJobFromSqlStaticFileToolStripMenuItem.ToolTipText = "Import a job from a sql script that generates operations immediately";
            this.importJobFromSqlStaticFileToolStripMenuItem.Click += new System.EventHandler(this.fromSqlStaticToolStripMenuItem_Click);
            // 
            // importJobFromExcelToolStripMenuItem
            // 
            this.importJobFromExcelToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobImportExcel;
            this.importJobFromExcelToolStripMenuItem.Name = "importJobFromExcelToolStripMenuItem";
            this.importJobFromExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.importJobFromExcelToolStripMenuItem.Text = "From Excel...";
            this.importJobFromExcelToolStripMenuItem.ToolTipText = "Import a job from a Microsoft Excel spreadsheet";
            this.importJobFromExcelToolStripMenuItem.Click += new System.EventHandler(this.fromExcelToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(202, 6);
            // 
            // jobImportWizardToolStripMenuItem
            // 
            this.jobImportWizardToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.JobImportWizard;
            this.jobImportWizardToolStripMenuItem.Name = "jobImportWizardToolStripMenuItem";
            this.jobImportWizardToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.jobImportWizardToolStripMenuItem.Text = "Job Import Wizard...";
            this.jobImportWizardToolStripMenuItem.ToolTipText = "Open the job import wizard";
            this.jobImportWizardToolStripMenuItem.Click += new System.EventHandler(this.jobImportWizardToolStripMenuItem_Click);
            // 
            // runToolStripButton
            // 
            this.runToolStripButton.Image = global::ScottLane.Surgeon.Client.Properties.Resources.BatchRun;
            this.runToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.runToolStripButton.Name = "runToolStripButton";
            this.runToolStripButton.Size = new System.Drawing.Size(48, 22);
            this.runToolStripButton.Text = "Run";
            this.runToolStripButton.ToolTipText = "Run the current batch";
            this.runToolStripButton.Click += new System.EventHandler(this.runBatchToolStripButton_Click);
            // 
            // stopToolStripButton
            // 
            this.stopToolStripButton.Image = global::ScottLane.Surgeon.Client.Properties.Resources.BatchStop;
            this.stopToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopToolStripButton.Name = "stopToolStripButton";
            this.stopToolStripButton.Size = new System.Drawing.Size(51, 22);
            this.stopToolStripButton.Text = "Stop";
            this.stopToolStripButton.ToolTipText = "Stop the current batch";
            this.stopToolStripButton.Click += new System.EventHandler(this.stopBatchToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // publishAllToolStripButton
            // 
            this.publishAllToolStripButton.Image = global::ScottLane.Surgeon.Client.Properties.Resources.PublishAll;
            this.publishAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.publishAllToolStripButton.Name = "publishAllToolStripButton";
            this.publishAllToolStripButton.Size = new System.Drawing.Size(83, 22);
            this.publishAllToolStripButton.Text = "Publish All";
            this.publishAllToolStripButton.ToolTipText = "Publish all customisations";
            this.publishAllToolStripButton.Click += new System.EventHandler(this.publishAllToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripDropDownButton1
            // 
            this.helpToolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.gettingStartedToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripDropDownButton1.Image = global::ScottLane.Surgeon.Client.Properties.Resources.Help;
            this.helpToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripDropDownButton1.Name = "helpToolStripDropDownButton1";
            this.helpToolStripDropDownButton1.Size = new System.Drawing.Size(61, 22);
            this.helpToolStripDropDownButton1.Text = "Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.HelpContents;
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.contentsToolStripMenuItem.Text = "Contents...";
            this.contentsToolStripMenuItem.Click += new System.EventHandler(this.contentsToolStripMenuItem_Click);
            // 
            // gettingStartedToolStripMenuItem
            // 
            this.gettingStartedToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.HelpGettingStarted;
            this.gettingStartedToolStripMenuItem.Name = "gettingStartedToolStripMenuItem";
            this.gettingStartedToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.gettingStartedToolStripMenuItem.Text = "Getting Started...";
            this.gettingStartedToolStripMenuItem.ToolTipText = "Open the getting started guide";
            this.gettingStartedToolStripMenuItem.Click += new System.EventHandler(this.gettingStartedToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.HelpAbout;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.aboutToolStripMenuItem.Text = "About...";
            this.aboutToolStripMenuItem.ToolTipText = "Open the about page";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 412);
            this.Controls.Add(this.connectionStatusStrip);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.MainForm_DragOver);
            this.connectionStatusStrip.ResumeLayout(false);
            this.connectionStatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jobDataGridView)).EndInit();
            this.jobContextMenuStrip.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openDialog;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.StatusStrip connectionStatusStrip;
        private System.Windows.Forms.ToolStripDropDownButton connectionDropDownButton;
        private System.Windows.Forms.ToolStripStatusLabel progressToolStripStatusLabel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView jobDataGridView;
        private System.Windows.Forms.RichTextBox statusRichTextBox;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton openBatchToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton saveBatchToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripDropDownButton importJobToolStripItem;
        private System.Windows.Forms.ToolStripButton stopToolStripButton;
        private System.Windows.Forms.ToolStripButton runToolStripButton;
        private System.Windows.Forms.ToolStripButton newBatchToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem importJobFromSqlStaticFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importJobFromSqlDynamicFromFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton publishAllToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem importJobFromExcelToolStripMenuItem;
        private System.Windows.Forms.DataGridViewImageColumn typeImageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn batchStatusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn progressColumn;
        private System.Windows.Forms.ToolStripDropDownButton helpToolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem gettingStartedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem jobImportWizardToolStripMenuItem;
        private System.Windows.Forms.HelpProvider formHelpProvider;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip jobContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem moveUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
    }
}

