﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScottLane.Surgeon.Client.Controls;
using ScottLane.Surgeon.Client.Helpers;
using ScottLane.Surgeon.Client.Resources;
using ScottLane.Surgeon.Common.Resources;
using ScottLane.Surgeon.Model;
using ScottLane.Surgeon.Model.Forms;

namespace ScottLane.Surgeon.Client.Forms
{
    /// <summary>
    /// The main user interface form.
    /// </summary>
    public partial class MainForm : ErrorDetailsForm
    {
        const int defaultJobIndex = -1;
        private readonly Color defaultErrorColor = Color.Red;

        public Batch Batch { get; set; }
        public RunMode RunMode { get; set; }
        public Connection Connection { get; set; }

        private CancellationTokenSource cancel;
        private bool requiresSave = false;
        private bool asyncOperationRunning = false;

        /// <summary>
        /// Initialises a new instance of the MainForm class and sets default property values.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            Icon = Properties.Resources.MainFormIcon;
            formHelpProvider.HelpNamespace = CommonStrings.HelpMainFormUrl;
            jobDataGridView.AutoGenerateColumns = false;
            RunMode = RunMode.Manual;
        }

        /// <summary>
        /// Sets the initial state of form data based on supplied command line arguments and user settings.
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (RunMode == RunMode.Automatic)
                {
                    ConnectToCurrentConnection();
                    RunBatch(false, true);
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormLoadFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Creates a new batch.
        /// </summary>
        private void newBatchToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckAndPromptForSave(ClientStrings.MainFormBatchOpenSavePrompt) != DialogResult.Cancel)
                {
                    statusRichTextBox.Clear();
                    Batch = new Batch();
                    saveDialog.FileName = string.Empty;
                    requiresSave = true;
                    DisplayMessage(ClientStrings.MainFormBatchCreateSuccessfulText);
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormBatchLoadFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Opens a batch from a file.
        /// </summary>
        private void openBatchToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckAndPromptForSave(ClientStrings.MainFormBatchLoadSavePrompt) != DialogResult.Cancel)
                {
                    openDialog.FileName = null;
                    openDialog.Filter = string.Format(ClientStrings.MainFormBatchFileFilter, CommonStrings.BatchFileExtension);
                    DialogResult result = openDialog.ShowDialog();

                    if (result == DialogResult.OK)
                    {
                        LoadBatchFromFile(openDialog.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormBatchLoadFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Helper method to load a batch from a file.
        /// </summary>
        /// <param name="fileName">The batch file name.</param>
        private void LoadBatchFromFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            Batch = Batch.Load(fileName);
            DisplayMessage(ClientStrings.MainFormBatchLoadSucceededText);
            saveDialog.FileName = fileName;
        }

        /// <summary>
        /// Saves the current batch to a file.
        /// </summary>
        private void saveBatchToolStripButton_Click(object sender, EventArgs e)
        {
            SaveBatch();
        }

        /// <summary>
        /// Saves the current batch to a file.
        /// </summary>
        private void SaveBatch()
        {
            try
            {
                bool doSave = true;

                if (saveDialog.FileName == string.Empty)
                {
                    saveDialog.FileName = string.Format(ClientStrings.MainFormBatchFileDefaultName, CommonStrings.BatchFileExtension);
                    saveDialog.Filter = string.Format(ClientStrings.MainFormBatchFileFilter, CommonStrings.BatchFileExtension);
                    DialogResult result = saveDialog.ShowDialog();
                    doSave = result == DialogResult.OK;
                }

                if (doSave)
                {
                    FileInfo file = new FileInfo(saveDialog.FileName);
                    Batch.Save(file.FullName);
                    requiresSave = false;
                    DisplayMessage(ClientStrings.MainFormBatchSaveSuccessfulText);
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormBatchSaveFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Imports a job from a dynamically executed t-sql script.
        /// </summary>
        private void fromSqlDynamicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // todo - after running a batch that failed then adding another batch, the subsequent batch fails too saying that the entity has not been specified
            SqlJob batch = new SqlJob();
            batch.CompilationType = CompilationType.Dynamic;
            ImportJobFromFile(batch, string.Format(ClientStrings.SqlFileFilter, CommonStrings.SqlFileExtension));
        }

        /// <summary>
        /// Imports a job from a statically executed t-sql script.
        /// </summary>
        private void fromSqlStaticToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlJob batch = new SqlJob();
            batch.CompilationType = CompilationType.Static;
            ImportJobFromFile(batch, string.Format(ClientStrings.SqlFileFilter, CommonStrings.SqlFileExtension));
        }

        /// <summary>
        /// Imports a job from a Microsoft Excel spreadsheet.
        /// </summary>
        private void fromExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExcelJob batch = new ExcelJob();
            batch.CompilationType = CompilationType.Static;
            ImportJobFromFile(batch, string.Format(ClientStrings.ExcelFileFilter, CommonStrings.ExcelFileExtension));
        }

        /// <summary>
        /// Imports a job from a file with the specified file filter.
        /// </summary>
        /// <param name="job">The job.</param>
        /// <param name="filter">The file filter.</param>
        private void ImportJobFromFile(Job job, string filter)
        {
            try
            {
                openDialog.FileName = null;
                openDialog.Filter = filter;
                DialogResult result = openDialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    job.FileName = openDialog.FileName;
                    job.Name = new FileInfo(job.FileName).Name;
                    ImportJob(job);
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormJobImportFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Asynchronously imports a job.
        /// </summary>
        /// <param name="job">The job.</param>
        private async void ImportJob(Job job)
        {
            try
            {
                asyncOperationRunning = true;
                await Task.Run(() => job.LoadFileContents());
                asyncOperationRunning = false;

                Batch.Jobs.Add(job);
                requiresSave = true;
                RefreshFormControls();

                if (job.CompilationType == CompilationType.Static)
                {
                    cancel = new CancellationTokenSource();
                    Progress<JobProgress> progress = new Progress<JobProgress>();
                    progress.ProgressChanged += progress_ProgressChanged;
                    DisplayMessage(ClientStrings.MainFormJobImportStartedText);
                    asyncOperationRunning = true;
                    RefreshFormControls();

                    await Task.Run(() => job.AddOperations(Connection, cancel.Token, progress));
                    await Task.Run(() => job.ValidateOperations(Connection, cancel.Token, progress));
                }

                if (cancel != default(CancellationTokenSource) && cancel.IsCancellationRequested)
                {
                    DisplayMessage(ClientStrings.MainFormBatchStopSucceededText);
                }
                else
                {
                    DisplayMessage(ClientStrings.MainFormJobImportSuccessfulText);
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormJobImportFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                asyncOperationRunning = false;
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Runs the current batch.
        /// </summary>
        private void runBatchToolStripButton_Click(object sender, EventArgs e)
        {
            RunBatch(true, false);
        }

        /// <summary>
        /// Runs the current batch, optionally displaying a confirmation dialog and throwing exceptions.
        /// </summary>
        /// <param name="displayDialog">If true, indicates that a confirmation dialog should be displayed before running the batch.</param>
        /// <param name="closeWhenCompleted">If true, closes the form when completed.</param>
        private async void RunBatch(bool displayDialog, bool closeWhenCompleted)
        {
            try
            {
                DialogResult result = DialogResult.Yes;

                if (displayDialog)
                {
                    result = MessageBox.Show(string.Format(ClientStrings.MainFormBatchRunConfirmationCaption, Connection.Name), ClientStrings.MainFormBatchRunConfirmationTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }

                if (result == DialogResult.Yes)
                {
                    DisplayMessage(ClientStrings.MainFormBatchRunningText);
                    cancel = new CancellationTokenSource();
                    Progress<JobProgress> progress = new Progress<JobProgress>();
                    progress.ProgressChanged += progress_ProgressChanged;
                    asyncOperationRunning = true;
                    RefreshFormControls();

                    await Task.Run(() => Batch.Run(Connection, cancel.Token, progress, this));

                    if (cancel.IsCancellationRequested)
                    {
                        DisplayMessage(string.Format(ClientStrings.MainFormBatchStopSucceededText, Batch.DurationString));
                    }
                    else if (Batch.Status == BatchStatus.Failed)
                    {
                        DisplayMessage(ClientStrings.MainFormBatchFailedText, defaultErrorColor);
                    }
                    else if (Batch.Status == BatchStatus.Invalid)
                    {
                        DisplayMessage(ClientStrings.MainFormBatchInvalidText, defaultErrorColor);
                    }
                    else
                    {
                        DisplayMessage(string.Format(ClientStrings.MainFormBatchSucceededText, Batch.DurationString));
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormBatchFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                asyncOperationRunning = false;

                if (closeWhenCompleted)
                {
                    Close();
                }
                else
                {
                    RefreshFormControls();
                }
            }
        }

        /// <summary>
        /// Stops the currently executing batch.
        /// </summary>
        private void stopBatchToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                cancel.Cancel();
                DisplayMessage(ClientStrings.MainFormBatchStopStartedText);
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormBatchStopFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Refreshes all form controls based on the current system state.
        /// </summary>
        private void RefreshFormControls()
        {
            RefreshFormControls(false);
        }

        /// <summary>
        /// Refreshes all form controls based on the current system state and optionally forces the redraw of connection controls.
        /// </summary>
        private void RefreshFormControls(bool forceConnectionRedraw)
        {
            try
            {
                // set the form title and batch grid data source based on batch status
                if (Batch != null)
                {
                    // todo - set filename of new batch in dialog
                    string fileName = saveDialog.FileName == string.Empty ? "New Batch" : new FileInfo(saveDialog.FileName).Name;
                    string saveIndicator = requiresSave ? "*" : string.Empty;
                    Text = string.Format("{0} - {1}{2}", CommonStrings.ProductNameLong, fileName, saveIndicator);

                    List<int> selectedRows = new List<int>();

                    foreach (DataGridViewRow row in jobDataGridView.SelectedRows)
                    {
                        selectedRows.Add(row.Index);
                    }

                    BindingSource source = new BindingSource();
                    source.DataSource = Batch.Jobs;
                    jobDataGridView.DataSource = source;

                    // maintain selected rows after rebinding
                    foreach (DataGridViewRow row in jobDataGridView.Rows)
                    {
                        row.Selected = selectedRows.Contains(row.Index);
                    }
                }
                else
                {
                    Text = CommonStrings.ProductNameLong;
                    jobDataGridView.DataSource = null;
                }

                jobDataGridView.Visible = Batch != null;
                Cursor.Current = !asyncOperationRunning ? Cursors.Default : Cursors.WaitCursor;

                // set toolbar button states
                newBatchToolStripButton.Enabled = !asyncOperationRunning;
                openBatchToolStripButton.Enabled = !asyncOperationRunning;
                saveBatchToolStripButton.Enabled = !asyncOperationRunning && Batch != null && requiresSave;
                importJobToolStripItem.Enabled = !asyncOperationRunning && Batch != null;
                importJobFromSqlDynamicFromFileToolStripMenuItem.Enabled = !asyncOperationRunning && Batch != null;
                importJobFromSqlStaticFileToolStripMenuItem.Enabled = !asyncOperationRunning && Batch != null && Connection != null;
                importJobFromExcelToolStripMenuItem.Enabled = !asyncOperationRunning && Batch != null && Connection != null;
                jobImportWizardToolStripMenuItem.Enabled = !asyncOperationRunning && Batch != null && Connection != null;
                publishAllToolStripButton.Enabled = !asyncOperationRunning && Connection != null;
                runToolStripButton.Enabled = !asyncOperationRunning && Batch != null && Batch.Jobs.Count > 0 && Batch.IsValid && Connection != null;
                stopToolStripButton.Enabled = asyncOperationRunning && Batch != null && Batch.Jobs.Count > 0 && Batch.IsValid && Connection != null;   //  -  not perfect

                // set connection control states
                connectionDropDownButton.Text = Connection == null ? ClientStrings.MainFormConnectionNotConnectedText : Connection.Name;
                connectionDropDownButton.Enabled = !asyncOperationRunning;

                const int emptyConnectionDropDownItemCount = 2;

                // redraw connection details if the connection count changes or if forced to do so
                if (connectionDropDownButton.DropDownItems.Count != Settings.Default.Connections.Count + emptyConnectionDropDownItemCount || forceConnectionRedraw)
                {
                    connectionDropDownButton.DropDownItems.Clear();

                    ToolStripMenuItem createNewConnectionToolStripMenuItem = new ToolStripMenuItem();
                    createNewConnectionToolStripMenuItem.Image = global::ScottLane.Surgeon.Client.Properties.Resources.ConnectionCreate;
                    createNewConnectionToolStripMenuItem.Name = "createNewConnectionToolStripMenuItem";
                    createNewConnectionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
                    createNewConnectionToolStripMenuItem.Text = ClientStrings.MainFormConnectionCreateNewText;
                    createNewConnectionToolStripMenuItem.Click += new System.EventHandler(createNewConnectionToolStripMenuItem_Click);
                    connectionDropDownButton.DropDownItems.Add(createNewConnectionToolStripMenuItem);

                    if (Settings.Default.Connections.Count > 0)
                    {
                        ToolStripSeparator separator = new ToolStripSeparator();
                        separator.Name = "connectionToolStripSeparator";
                        separator.Size = new System.Drawing.Size(193, 6);
                        connectionDropDownButton.DropDownItems.Insert(0, separator);

                        foreach (Connection connection in Settings.Default.Connections)
                        {
                            ConnectionToolStripMenuItem connectionMenuItem = new ConnectionToolStripMenuItem(connection);
                            connectionMenuItem.Connect += connectionMenuItem_Connect;
                            connectionMenuItem.Delete += connectionMenuItem_Delete;
                            connectionMenuItem.Edited += connectionMenuItem_Edited;
                            connectionDropDownButton.DropDownItems.Insert(0, connectionMenuItem);
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Connects to the selected connection.
        /// </summary>
        private void connectionMenuItem_Connect(object sender, EventArgs e)
        {
            try
            {
                ConnectionToolStripMenuItem menuItem = (ConnectionToolStripMenuItem)sender;
                Connection = menuItem.Connection;
                ConnectToCurrentConnection();
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormConnectionConnectFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Deletes the selected connection.
        /// </summary>
        private void connectionMenuItem_Delete(object sender, EventArgs e)
        {
            try
            {
                ConnectionToolStripMenuItem menuItem = (ConnectionToolStripMenuItem)sender;
                Connection connection = menuItem.Connection;
                DialogResult result = MessageBox.Show(string.Format(ClientStrings.MainFormConnectionDeleteMessage, connection.Name), "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    Settings.Default.Connections.Remove(menuItem.Connection);
                    Settings.Default.Save();

                    if (Connection != null && Connection == menuItem.Connection)
                    {
                        Connection = null;
                    }

                    DisplayMessage(string.Format(ClientStrings.MainFormConnectionDeleteSucceededText));
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormConnectionDeleteFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Refreshes the form after a connection has been edited.
        /// </summary>
        private void connectionMenuItem_Edited(object sender, EventArgs e)
        {
            try
            {
                ConnectionToolStripMenuItem item = (ConnectionToolStripMenuItem)sender;

                if (item.SetAsCurrentConnection)
                {
                    Connection = item.Connection;
                    ConnectToCurrentConnection();
                }
                else
                {
                    Settings.Default.Save();
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormConnectionEditFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls(true);
            }
        }

        /// <summary>
        /// Creates a new connection.
        /// </summary>
        private void createNewConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ConnectionDetailsForm form = new ConnectionDetailsForm();
                DialogResult result = form.ShowDialog();

                if (result == DialogResult.OK)
                {
                    Settings.Default.Connections.Add(form.Connection);

                    if (form.SetAsCurrentConnection)
                    {
                        Connection = form.Connection;
                        ConnectToCurrentConnection();
                    }
                    else
                    {
                        Settings.Default.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormConnectionCreateFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls(true);
            }
        }

        /// <summary>
        /// Asynchronously connects to the CRM instance associated with the current connection.
        /// </summary>
        private async void ConnectToCurrentConnection()
        {
            try
            {
                DisplayMessage(string.Format(ClientStrings.MainFormConnectionConnectingText, Connection.Name));
                asyncOperationRunning = true;
                ConnectionResult result = await Task.Run(() => Connection.Connect());

                if (result.HasFlag(ConnectionResult.CrmConnectionSucceeded))
                {
                    Settings.Default.CurrentConnection = Connection;
                    Settings.Default.Save();
                    DisplayMessage(string.Format(ClientStrings.MainFormConnectionConnectSucceededText, Connection.Name));
                }
                else
                {
                    DisplayMessage(string.Format(ClientStrings.MainFormConnectionConnectFailedText, Connection.Name), defaultErrorColor);
                }
            }
            finally
            {
                asyncOperationRunning = false;
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Publishes all customisations in the connected CRM instance.
        /// </summary>
        private async void publishAllToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayMessage(string.Format(ClientStrings.MainFormPublishAllStartedText, Connection.Name));
                asyncOperationRunning = true;
                RefreshFormControls();
                await Task.Run(() => Connection.PublishAll());
                DisplayMessage(ClientStrings.MainFormPublishAllSucceededText);
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormPublishAllFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                asyncOperationRunning = false;
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Displays a message with the default colour.
        /// </summary>
        /// <param name="message">The message to display.</param>
        private void DisplayMessage(string message)
        {
            DisplayMessage(message, Color.Black);
        }

        /// <summary>
        /// Displays a message with the specified colour.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="color">The text colour.</param>
        private void DisplayMessage(string message, Color color)
        {
            try
            {
                int start = statusRichTextBox.TextLength;
                statusRichTextBox.AppendText(string.Format("{0}\n", message));
                int length = statusRichTextBox.TextLength - start;
                statusRichTextBox.Select(start, length);
                statusRichTextBox.SelectionColor = color;
                statusRichTextBox.SelectionLength = 0;

                Logger.WriteLine(message);
            }
            catch { }
        }

        /// <summary>
        /// Fires when an asynchronous process progress changes.
        /// </summary>
        private void progress_ProgressChanged(object sender, JobProgress e)
        {
            RefreshFormControls();
        }

        /// <summary>
        /// Prompts to save an edited batch before the form closes.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (asyncOperationRunning)
                {
                    DialogResult result = MessageBox.Show(ClientStrings.MainFormCloseTaskRunningText, ClientStrings.MainFormCloseTaskRunningCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (result == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                }

                if (requiresSave && !e.Cancel)
                {
                    DialogResult result = MessageBox.Show(ClientStrings.MainFormCloseSaveMessage, ClientStrings.MainFormCloseSaveCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        SaveBatch();
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        e.Cancel = true;
                    }
                }

                if (Connection != null && !e.Cancel)
                {
                    Connection.Close();
                }
            }
            catch { }
        }

        /// <summary>
        /// Checks if the the batch needs to be saved and if so prompts the user to save.
        /// </summary>
        /// <param name="message">The save prompt message.</param>
        /// <returns>The DialogResult if the user is prompted to save otherwise DialogResult.None.</returns>
        private DialogResult CheckAndPromptForSave(string message)
        {
            DialogResult result = DialogResult.None;

            if (requiresSave)
            {
                result = MessageBox.Show(message, ClientStrings.MainFormBatchSaveMessage, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    SaveBatch();
                }
                else if (result == DialogResult.No)
                {
                    requiresSave = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Shows an error dialog and returns the error behaviour selected in the dialog.
        /// </summary>
        /// <param name="exception">The exception details.</param>
        /// <param name="operation">The operation details.</param>
        /// <returns>The error behaviour.</returns>
        public override ErrorBehaviour ShowErrorDialog(Exception exception, Operation operation)
        {
            ErrorDialog dialog = new ErrorDialog();
            dialog.Exception = exception;
            dialog.Operation = operation;
            dialog.ShowDialog();

            return dialog.ErrorBehaviour;
        }

        /// <summary>
        /// Stops any currently running operations.
        /// </summary>
        public override void Stop()
        {
            cancel.Cancel();
        }

        /// <summary>
        /// Opens the about help page.
        /// </summary>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(CommonStrings.HelpAboutUrl);
        }

        /// <summary>
        /// Opens the getting started help page.
        /// </summary>
        private void gettingStartedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(CommonStrings.HelpGettingStartedUrl);
        }

        /// <summary>
        /// Opens the job import wizard.
        /// </summary>
        private void jobImportWizardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JobImportWizardForm jobImportWizard = new JobImportWizardForm();
            DialogResult result = jobImportWizard.ShowDialog();

            if (result == DialogResult.OK)
            {
                ImportJob(jobImportWizard.Job);
            }
        }

        /// <summary>
        /// Opens the help contents page.
        /// </summary>
        private void contentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(CommonStrings.HelpContentsUrl);
        }

        #region DataGridView Operations

        /// <summary>
        /// Sets the grid image for each job based on batch and compilation type.
        /// </summary>
        private void jobDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (jobDataGridView.Columns[e.ColumnIndex].Name == typeImageColumn.Name)
                {
                    if (Batch.Jobs.Count > 0)
                    {
                        if (Batch.Jobs[e.RowIndex] is ExcelJob)
                        {
                            e.Value = importJobFromExcelToolStripMenuItem.Image;
                        }
                        else
                        {
                            CompilationType compilationType = (CompilationType)e.Value;

                            if (compilationType == CompilationType.Dynamic)
                            {
                                e.Value = importJobFromSqlDynamicFromFileToolStripMenuItem.Image;
                            }
                            else if (compilationType == CompilationType.Static)
                            {
                                e.Value = importJobFromSqlStaticFileToolStripMenuItem.Image;
                            }
                        }
                    }
                    else
                    {
                        e.Value = null;
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /// <summary>
        /// Handles row selection when the mouse is clicked.
        /// </summary>
        private void jobDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                int jobIndex = jobDataGridView.HitTest(e.X, e.Y).RowIndex;

                // clicked on the header or below the grid
                if (jobIndex == defaultJobIndex)
                {
                    foreach (DataGridViewRow row in jobDataGridView.SelectedRows)
                    {
                        row.Selected = false;
                    }
                }
                else
                {
                    // clicked on the grid
                    if (e.Button == MouseButtons.Right)
                    {
                        if (jobDataGridView.SelectedRows.Count == 0)
                        {
                            jobDataGridView.Rows[jobIndex].Selected = true;
                        }
                        else if (jobDataGridView.SelectedRows.Count == 1)
                        {
                            foreach (DataGridViewRow row in jobDataGridView.SelectedRows)
                            {
                                row.Selected = false;
                            }

                            jobDataGridView.Rows[jobIndex].Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /// <summary>
        /// Moves the selected jobs up one position.
        /// </summary>
        private void moveUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> selectedRows = new List<int>();

                foreach (DataGridViewRow row in jobDataGridView.SelectedRows)
                {
                    selectedRows.Add(row.Index);
                }

                selectedRows.Reverse();

                foreach (int rowIndex in selectedRows)
                {
                    MoveJob(rowIndex, rowIndex - 1);
                }

                requiresSave = true;
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /// <summary>
        /// Moves the selected jobs down one position.
        /// </summary>
        private void moveDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> selectedRows = new List<int>();

                foreach (DataGridViewRow row in jobDataGridView.SelectedRows)
                {
                    selectedRows.Add(row.Index);
                }

                foreach (int rowIndex in selectedRows)
                {
                    MoveJob(rowIndex, rowIndex + 1);
                }

                requiresSave = true;
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /// <summary>
        /// Removes the selected jobs.
        /// </summary>
        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in jobDataGridView.SelectedRows)
                {
                    Batch.Jobs.RemoveAt(row.Index);
                }

                foreach (DataGridViewRow row in jobDataGridView.Rows)
                {
                    row.Selected = false;
                }

                requiresSave = true;
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /// <summary>
        /// Enables job context menu items based on the selected job.
        /// </summary>
        private void jobContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                bool moveUpSupported = true;
                bool moveDownSupported = true;

                foreach (DataGridViewRow row in jobDataGridView.SelectedRows)
                {
                    if (row.Index <= 0)
                    {
                        moveUpSupported = false;
                    }

                    if (row.Index >= Batch.Jobs.Count - 1)
                    {
                        moveDownSupported = false;
                    }
                }

                moveUpToolStripMenuItem.Enabled = moveUpSupported;
                moveDownToolStripMenuItem.Enabled = moveDownSupported;
                e.Cancel = jobDataGridView.SelectedRows.Count == 0;
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /// <summary>
        /// Handles data grid data errors.
        /// </summary>
        private void jobDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DisplayMessage(e.Exception.Message, Color.Red);
        }

        /// <summary>
        /// Ensures no jobs are highlighted after removing a job.
        /// </summary>
        private void jobDataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in jobDataGridView.Rows)
                {
                    row.Selected = false;
                }

                requiresSave = true;
                RefreshFormControls();
            }
            catch (Exception ex)
            {
                DisplayMessage(ex.Message, Color.Red);
            }
        }

        /// <summary>
        /// Moves the job at the source index to destination index and refreshes the grid.
        /// </summary>
        /// <param name="sourceIndex">The index of the job to move.</param>
        /// <param name="destinationIndex">The index of the destination to move to.</param>
        private void MoveJob(int sourceIndex, int destinationIndex)
        {
            if ((sourceIndex < destinationIndex && destinationIndex < Batch.Jobs.Count)
                || (sourceIndex > destinationIndex && destinationIndex >= 0))
            {
                Job job = Batch.Jobs[sourceIndex];
                Batch.Jobs.RemoveAt(sourceIndex);
                Batch.Jobs.Insert(destinationIndex, job);
                jobDataGridView.Rows[sourceIndex].Selected = false;
                jobDataGridView.Rows[destinationIndex].Selected = true;
                RefreshFormControls();
            }
        }

        #endregion

        /// <summary>
        /// Validates that a file dragged onto the form is a batch file and if so displays the drag-drop cursor.
        /// </summary>
        private void MainForm_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    string[] docPath = (string[])e.Data.GetData(DataFormats.FileDrop);
                    string fileName = docPath[0];

                    if (fileName.EndsWith(CommonStrings.BatchFileExtension))
                    {
                        e.Effect = DragDropEffects.All;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.None;
                    }
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormDragDropFailedText, ex.Message), defaultErrorColor);
            }
        }

        /// <summary>
        /// Loads a batch that is dragged onto the form.
        /// </summary>
        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    string[] docPath = (string[])e.Data.GetData(DataFormats.FileDrop);
                    string fileName = docPath[0];

                    if (CheckAndPromptForSave(ClientStrings.MainFormBatchLoadSavePrompt) != DialogResult.Cancel)
                    {
                        LoadBatchFromFile(fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayMessage(string.Format(ClientStrings.MainFormBatchLoadFailedText, ex.Message), defaultErrorColor);
            }
            finally
            {
                RefreshFormControls();
            }
        }
    }

    /// <summary>
    /// Specifies the form running modes.
    /// </summary>
    public enum RunMode
    {
        /// <summary>
        /// Automatically run the batch specified in the command line when the application starts.
        /// </summary>
        Automatic,

        /// <summary>
        /// Manually runs a batch using the interactive form.
        /// </summary>
        Manual
    }
}