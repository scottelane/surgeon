﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScottLane.Surgeon.Client.Helpers;
using ScottLane.Surgeon.Client.Resources;
using ScottLane.Surgeon.Model;

namespace ScottLane.Surgeon.Client.Forms
{
    /// <summary>
    /// A wizard that assists users create and import a job.
    /// </summary>
    public partial class JobImportWizardForm : Form
    {
        private bool entitiesLoaded;
        private bool fieldsLoaded;
        private List<EntityInfo> entities;
        private bool relatedEntitiesLoaded;
        private bool relationshipsLoaded;
        private bool ownersLoaded;
        private bool statesLoaded;
        private Template template;
        private string lastEntitySelected;
        public Job Job { get; set; }

        /// <summary>
        /// Initialises a new instance of the JobImportWizardForm class.
        /// </summary>
        public JobImportWizardForm()
        {
            InitializeComponent();

            entitiesLoaded = false;
            fieldsLoaded = false;
            relatedEntitiesLoaded = false;
            relationshipsLoaded = false;
            ownersLoaded = false;
            statesLoaded = false;
            Icon = Properties.Resources.JobImportWizardIcon;
            template = new Template();
        }

        /// <summary>
        /// Starts asynchronous processes and sets the initial form state.
        /// </summary>
        private void JobImportWizardForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetTemplateOperationType();
                PopulateEntitiesAsync();
                PopulateOwnersAsync();
                RefreshNavigationControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Populates the entity combo box asynchronously.
        /// </summary>
        private async void PopulateEntitiesAsync()
        {
            entities = await Task.Run(() => EntityInfo.GetAllEntities(Settings.Default.CurrentConnection));
            entitiesLoaded = true;
            PopulateEntityComboBox();
        }

        /// <summary>
        /// Populates the entity combo box for the specified operation type.
        /// </summary>
        private void PopulateEntityComboBox()
        {
            if (entitiesLoaded)
            {
                //OperationType operation = (OperationType)Enum.Parse(typeof(OperationType), operationComboBox.Text.Replace(" ", string.Empty));
                entityComboBox.Items.Clear();

                foreach (EntityInfo entity in entities)
                {
                    if (template.OperationType != OperationType.Assign
                        || (template.OperationType == OperationType.Assign && entity.IsUserTeamOwned)
                        )
                    {
                        entityComboBox.Items.Add(entity);
                    }
                }

                if (entityComboBox.Items.Count > 0)
                {
                    entityComboBox.SelectedIndex = 0;
                }

                entityLoadingPictureBox.Visible = false;
                RefreshNavigationControls();
            }
        }

        /// <summary>
        /// Sets the template operation type from the selected operation type.
        /// </summary>
        private void SetTemplateOperationType()
        {
            template.OperationType = (OperationType)Enum.Parse(typeof(OperationType), operationComboBox.Text.Replace(" ", string.Empty));
        }

        /// <summary>
        /// Populates the entity combo box for the selected operation type.
        /// </summary>
        private void operationComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetTemplateOperationType();
                PopulateEntityComboBox();
                PopulateFieldsAsync();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Populates the related entity combo box in the background.
        /// </summary>
        private async void PopulateRelatedEntitiesAsync(EntityInfo entity)
        {
            relatedEntitiesLoaded = false;
            relatedEntityLoadingPictureBox.Visible = true;
            relatedEntityComboBox.Items.Clear();

            List<RelationshipInfo> relationships = await Task.Run(() => entity.GetRelationships(Settings.Default.CurrentConnection));

            foreach (object item in entityComboBox.Items)
            {
                EntityInfo entityItem = (EntityInfo)item;

                if (entityItem != entity)
                {
                    if (relationships.Exists(a => a.EntityLogicalName == entityItem.LogicalName))
                    {
                        relatedEntityComboBox.Items.Add(entityItem);
                    }
                }
            }

            if (relatedEntityComboBox.Items.Count > 0)
            {
                relatedEntityComboBox.SelectedIndex = 0;
            }

            relatedEntityLoadingPictureBox.Visible = false;
            relatedEntitiesLoaded = true;
            RefreshNavigationControls();
        }

        /// <summary>
        /// Populates the fields combo box in the background.
        /// </summary>
        private async void PopulateFieldsAsync()
        {
            if (template.OperationType == OperationType.Create || template.OperationType == OperationType.Update || template.OperationType == OperationType.Upsert)
            {
                if (entityComboBox.SelectedItem != null)
                {
                    EntityInfo entity = (EntityInfo)entityComboBox.SelectedItem;

                    if (entity.LogicalName != lastEntitySelected)
                    {
                        lastEntitySelected = entity.LogicalName;

                        string action = template.OperationType == OperationType.Create ? ClientStrings.JobImportWizardFormFieldCreateText : ClientStrings.JobImportWizardFormFieldUpdateText;
                        fieldsLabel.Text = string.Format(ClientStrings.JobImportWizardFormFieldLabelText, action);

                        fieldsLoaded = false;
                        fieldsLoadingPictureBox.Visible = true;
                        selectAllButton.Enabled = false;
                        selectNoneButton.Enabled = false;
                        fieldsCheckedListBox.Items.Clear();

                        List<FieldInfo> fields = await Task.Run(() => entity.GetFields(Settings.Default.CurrentConnection, template.OperationType));

                        foreach (FieldInfo field in fields)
                        {
                            fieldsCheckedListBox.Items.Add(field);
                        }

                        fieldsLoadingPictureBox.Visible = false;
                        fieldsLoaded = true;
                        selectAllButton.Enabled = true;
                        selectNoneButton.Enabled = true;
                        RefreshNavigationControls();
                    }
                }
            }
        }

        /// <summary>
        /// Populates the state combo box asynchronously.
        /// </summary>
        private async void PopulateStatesAsync(EntityInfo entity)
        {
            statesLoaded = false;
            stateLoadingPictureBox.Visible = true;
            statusLoadingPictureBox.Visible = true;
            stateComboBox.Items.Clear();
            statusComboBox.Items.Clear();

            List<StateInfo> states = await Task.Run(() => entity.GetStates(Settings.Default.CurrentConnection));

            foreach (StateInfo state in states)
            {
                stateComboBox.Items.Add(state);
            }

            if (stateComboBox.Items.Count > 0)
            {
                stateComboBox.SelectedIndex = 0;
            }

            stateLoadingPictureBox.Visible = false;
            RefreshNavigationControls();
        }

        /// <summary>
        /// Populates the status combox box for the specified state.
        /// </summary>
        /// <param name="state">The state.</param>
        private void PopulateStatuses(StateInfo state)
        {
            statusComboBox.Items.Clear();

            foreach (StatusInfo status in state.Statuses)
            {
                statusComboBox.Items.Add(status);
            }

            if (statusComboBox.Items.Count > 0)
            {
                statusComboBox.SelectedIndex = 0;
            }

            statusLoadingPictureBox.Visible = false;
            statesLoaded = true;
            RefreshNavigationControls();
        }

        /// <summary>
        /// Populates the relationship controls in the background.
        /// </summary>
        private async void PopulateRelationships(EntityInfo entity1, EntityInfo entity2)
        {
            relationshipsLoaded = false;
            relationshipLoadingPictureBox.Visible = true;
            relationshipComboBox.Items.Clear();

            List<RelationshipInfo> relationships = await Task.Run(() => entity1.GetRelationships(Settings.Default.CurrentConnection));

            foreach (RelationshipInfo relationship in relationships)
            {
                if (relationship.EntityLogicalName == entity2.LogicalName)
                {
                    relationshipComboBox.Items.Add(relationship);
                }
            }

            if (relationshipComboBox.Items.Count > 0)
            {
                relationshipComboBox.SelectedIndex = 0;
            }

            relationshipLoadingPictureBox.Visible = false;
            relationshipsLoaded = true;
            RefreshNavigationControls();
        }

        /// <summary>
        /// Populates the user and team combo boxes in the background.
        /// </summary>
        private async void PopulateOwnersAsync()
        {
            List<UserInfo> users = await Task.Run(() => UserInfo.GetAllUsers(Settings.Default.CurrentConnection));

            foreach (UserInfo user in users)
            {
                userComboBox.Items.Add(user);
            }

            if (userComboBox.Items.Count > 0)
            {
                userComboBox.SelectedIndex = 0;
            }

            userLoadingPictureBox.Visible = false;

            List<TeamInfo> teams = await Task.Run(() => TeamInfo.GetAllTeams(Settings.Default.CurrentConnection));

            foreach (TeamInfo team in teams)
            {
                teamComboBox.Items.Add(team);
            }

            if (teamComboBox.Items.Count > 0)
            {
                teamComboBox.SelectedIndex = 0;
            }

            teamLoadingPictureBox.Visible = false;
            ownersLoaded = true;
            RefreshNavigationControls();
        }

        private void entityComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateFieldsAsync();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        private void userRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                userComboBox.Enabled = userRadioButton.Checked;
                teamComboBox.Enabled = teamRadioButton.Checked;
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        private void teamRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                userComboBox.Enabled = userRadioButton.Checked;
                teamComboBox.Enabled = teamRadioButton.Checked;
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        private void relatedEntitiyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateRelationships((EntityInfo)entityComboBox.SelectedItem, (EntityInfo)relatedEntityComboBox.SelectedItem);
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        private void stateComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateStatuses((StateInfo)stateComboBox.SelectedItem);
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Selects all fields.
        /// </summary>
        private void selectAllButton_Click(object sender, EventArgs e)
        {
            try
            {
                for (int index = 0; index < fieldsCheckedListBox.Items.Count; index++)
                {
                    fieldsCheckedListBox.SetItemChecked(index, true);
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Unselects all fields.
        /// </summary>
        private void selectNoneButton_Click(object sender, EventArgs e)
        {
            try
            {
                for (int index = 0; index < fieldsCheckedListBox.Items.Count; index++)
                {
                    fieldsCheckedListBox.SetItemChecked(index, false);
                }
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        #region Navigation Controls

        /// <summary>
        /// Moves to the previous wizard step.
        /// </summary>
        private void backButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (wizardTabControl.SelectedTab == entityTabPage)
                {
                    wizardTabControl.SelectedTab = operationTabPage;
                }
                else if (wizardTabControl.SelectedTab == ownerTabPage)
                {
                    wizardTabControl.SelectedTab = entityTabPage;
                }
                else if (wizardTabControl.SelectedTab == relatedEntityTabPage)
                {
                    wizardTabControl.SelectedTab = entityTabPage;
                }
                else if (wizardTabControl.SelectedTab == stateTabPage)
                {
                    wizardTabControl.SelectedTab = entityTabPage;
                }
                else if (wizardTabControl.SelectedTab == fieldTabPage)
                {
                    wizardTabControl.SelectedTab = entityTabPage;
                }
                else if (wizardTabControl.SelectedTab == templateTabPage)
                {
                    if (template.OperationType == OperationType.PublishAll)
                    {
                        wizardTabControl.SelectedTab = operationTabPage;
                    }
                    else if (template.OperationType == OperationType.Assign)
                    {
                        wizardTabControl.SelectedTab = ownerTabPage;
                    }
                    else if (template.OperationType == OperationType.Associate || template.OperationType == OperationType.Disassociate)
                    {
                        wizardTabControl.SelectedTab = relatedEntityTabPage;
                    }
                    else if (template.OperationType == OperationType.SetState)
                    {
                        wizardTabControl.SelectedTab = stateTabPage;
                    }
                    else if (template.OperationType == OperationType.Create || template.OperationType == OperationType.Update || template.OperationType == OperationType.Upsert)
                    {
                        wizardTabControl.SelectedTab = fieldTabPage;
                    }
                    else
                    {
                        wizardTabControl.SelectedTab = entityTabPage;
                    }
                }
                else if (wizardTabControl.SelectedTab == fetchXmlTabPage)
                {
                    wizardTabControl.SelectedTab = templateTabPage;
                }
                else if (wizardTabControl.SelectedTab == finishTabPage)
                {
                    if (Job is FetchXmlJob)
                    {
                        wizardTabControl.SelectedTab = fetchXmlTabPage;
                    }
                    else
                    {
                        wizardTabControl.SelectedTab = templateTabPage;
                    }
                }

                RefreshNavigationControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Moves to the next wizard step and stores values captured in the current step (in most cases).
        /// </summary>
        private void nextButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (wizardTabControl.SelectedTab == operationTabPage)
                {
                    if (template.OperationType == OperationType.PublishAll)
                    {
                        wizardTabControl.SelectedTab = templateTabPage;
                    }
                    else
                    {
                        wizardTabControl.SelectedTab = entityTabPage;
                    }
                }
                else if (wizardTabControl.SelectedTab == entityTabPage)
                {
                    EntityInfo entity = (EntityInfo)entityComboBox.SelectedItem;

                    template.EntityDisplayName = entity.DisplayName;
                    template.EntityLogicalName = entity.LogicalName;
                    template.EntityPluralName = entity.PluralName;
                    template.PrimaryIdFieldName = entity.PrimaryIdFieldName;

                    if (template.OperationType == OperationType.Assign)
                    {
                        wizardTabControl.SelectedTab = ownerTabPage;
                    }
                    else if (template.OperationType == OperationType.Associate || template.OperationType == OperationType.Disassociate)
                    {
                        PopulateRelatedEntitiesAsync(entity);
                        wizardTabControl.SelectedTab = relatedEntityTabPage;
                    }
                    else if (template.OperationType == OperationType.SetState)
                    {
                        PopulateStatesAsync(entity);
                        wizardTabControl.SelectedTab = stateTabPage;
                    }
                    else if (template.OperationType == OperationType.Create || template.OperationType == OperationType.Update || template.OperationType == OperationType.Upsert)
                    {
                        wizardTabControl.SelectedTab = fieldTabPage;
                    }
                    else
                    {
                        wizardTabControl.SelectedTab = templateTabPage;
                    }
                }
                else if (wizardTabControl.SelectedTab == fieldTabPage)
                {
                    template.Fields = new List<FieldInfo>();

                    foreach (FieldInfo field in fieldsCheckedListBox.CheckedItems)
                    {
                        template.Fields.Add(field);
                    }

                    wizardTabControl.SelectedTab = templateTabPage;
                }
                else if (wizardTabControl.SelectedTab == ownerTabPage)
                {
                    if (userRadioButton.Checked)
                    {
                        template.User = ((UserInfo)userComboBox.SelectedItem).Name;
                    }
                    else if (teamRadioButton.Checked)
                    {
                        template.Team = ((TeamInfo)teamComboBox.SelectedItem).Name;
                    }

                    wizardTabControl.SelectedTab = templateTabPage;
                }
                else if (wizardTabControl.SelectedTab == relatedEntityTabPage)
                {
                    EntityInfo relatedEntity = (EntityInfo)relatedEntityComboBox.SelectedItem;

                    template.RelatedEntityLogicalName = relatedEntity.LogicalName;
                    template.RelatedEntityPluralName = relatedEntity.PluralName;
                    template.RelatedEntityPrimaryIdFieldName = relatedEntity.PrimaryIdFieldName;
                    template.RelationshipName = ((RelationshipInfo)relationshipComboBox.SelectedItem).SchemaName;

                    wizardTabControl.SelectedTab = templateTabPage;
                }
                else if (wizardTabControl.SelectedTab == stateTabPage)
                {
                    template.State = ((StateInfo)stateComboBox.SelectedItem).Name;
                    template.Status = ((StatusInfo)statusComboBox.SelectedItem).Name;

                    wizardTabControl.SelectedTab = templateTabPage;
                }
                else if (wizardTabControl.SelectedTab == templateTabPage)
                {
                    template.OrganisationName = Settings.Default.CurrentConnection.OrganisationName;

                    if (sqlDynamicRadioButton.Checked)
                    {
                        template = SqlTemplate.FromTemplate(template);
                        Job = new SqlJob();
                        Job.FileName = template.FileName;
                        Job.CompilationType = CompilationType.Dynamic;
                    }
                    else if (sqlStaticRadioButton.Checked)
                    {
                        template = SqlTemplate.FromTemplate(template);
                        Job = new SqlJob();
                        Job.FileName = template.FileName;
                        Job.CompilationType = CompilationType.Static;
                    }
                    else if (excelRadioButton.Checked)
                    {
                        template = ExcelTemplate.FromTemplate(template);
                        Job = new ExcelJob();
                        Job.FileName = template.FileName;
                        Job.CompilationType = CompilationType.Static;
                    }
                    else if (fetchXmlDynamicRadioButton.Checked)
                    {
                        template = FetchXmlTemplate.FromTemplate(template);
                        Job = new FetchXmlJob();
                        Job.FileName = template.FileName;
                        Job.CompilationType = CompilationType.Dynamic;
                    }
                    else if (fetchXmlStaticRadioButton.Checked)
                    {
                        template = FetchXmlTemplate.FromTemplate(template);
                        Job = new FetchXmlJob();
                        Job.FileName = template.FileName;
                        Job.CompilationType = CompilationType.Static;
                    }

                    if (fetchXmlStaticRadioButton.Checked || fetchXmlDynamicRadioButton.Checked)
                    {
                        wizardTabControl.SelectedTab = fetchXmlTabPage;
                    }
                    else
                    {
                        // todo - get job name from operation
                        Job.Name = new FileInfo(Job.FileName).Name;
                        template.Generate();
                        template.Open();
                        wizardTabControl.SelectedTab = finishTabPage;
                    }
                }
                else if (wizardTabControl.SelectedTab == fetchXmlTabPage)
                {
                    Job.Name = new FileInfo(Job.FileName).Name;
                    template.Generate();
                    template.Open();
                    wizardTabControl.SelectedTab = finishTabPage;
                }

                RefreshNavigationControls();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Completes and closes the wizard.
        /// </summary>
        private void finishButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Closes the wizard.
        /// </summary>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            try
            {
                // todo - stop all async operations
                DialogResult = DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                ClientUtility.HandleFormException(ex);
            }
        }

        /// <summary>
        /// Refreshes all navigation controls.
        /// </summary>
        private void RefreshNavigationControls()
        {
            if (wizardTabControl.SelectedTab == operationTabPage)
            {
                backButton.Enabled = false;
                nextButton.Enabled = true;
                finishButton.Enabled = false;
            }
            else if (wizardTabControl.SelectedTab == entityTabPage)
            {
                backButton.Enabled = true;
                nextButton.Enabled = entitiesLoaded;
                finishButton.Enabled = false;
            }
            else if (wizardTabControl.SelectedTab == fieldTabPage)
            {
                backButton.Enabled = true;
                nextButton.Enabled = fieldsLoaded;
                finishButton.Enabled = false;
            }
            else if (wizardTabControl.SelectedTab == ownerTabPage)
            {
                nextButton.Enabled = ownersLoaded;
                backButton.Enabled = true;
                finishButton.Enabled = false;
            }
            else if (wizardTabControl.SelectedTab == relatedEntityTabPage)
            {
                nextButton.Enabled = relatedEntitiesLoaded && relationshipsLoaded;
                backButton.Enabled = true;
                finishButton.Enabled = false;
            }
            else if (wizardTabControl.SelectedTab == stateTabPage)
            {
                nextButton.Enabled = statesLoaded;
                backButton.Enabled = true;
                finishButton.Enabled = false;
            }
            else if (wizardTabControl.SelectedTab == finishTabPage)
            {
                backButton.Enabled = true;
                nextButton.Enabled = false;
                finishButton.Enabled = true;
            }
            else
            {
                backButton.Enabled = true;
                nextButton.Enabled = true;
                finishButton.Enabled = false;
            }
        }

        #endregion
    }
}
