﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScottLane.Surgeon.Client.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ClientStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ClientStrings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ScottLane.Surgeon.Client.Resources.ClientStrings", typeof(ClientStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The connection name must be specified.
        /// </summary>
        internal static string ConnectionFormConnectionNameErrorMessage {
            get {
                return ResourceManager.GetString("ConnectionFormConnectionNameErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The CRM timeout must be a whole number between 1 and 3600.
        /// </summary>
        internal static string ConnectionFormCrmTimeoutInvalidMessage {
            get {
                return ResourceManager.GetString("ConnectionFormCrmTimeoutInvalidMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Password must be specified for custom authentication.
        /// </summary>
        internal static string ConnectionFormPasswordBlankMessage {
            get {
                return ResourceManager.GetString("ConnectionFormPasswordBlankMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Service URL must be a valid URL.
        /// </summary>
        internal static string ConnectionFormServiceUrlErrorMessage {
            get {
                return ResourceManager.GetString("ConnectionFormServiceUrlErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The SQL timeout must be a whole number between 1 and 86400.
        /// </summary>
        internal static string ConnectionFormSqlTimeoutInvalidMessage {
            get {
                return ResourceManager.GetString("ConnectionFormSqlTimeoutInvalidMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CRM connection test failed.
        /// </summary>
        internal static string ConnectionFormTestCrmFailedText {
            get {
                return ResourceManager.GetString("ConnectionFormTestCrmFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CRM connection test successful.
        /// </summary>
        internal static string ConnectionFormTestCrmSuccessfulText {
            get {
                return ResourceManager.GetString("ConnectionFormTestCrmSuccessfulText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test Failed.
        /// </summary>
        internal static string ConnectionFormTestFailedCaption {
            get {
                return ResourceManager.GetString("ConnectionFormTestFailedCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SQL Server connection test failed.
        /// </summary>
        internal static string ConnectionFormTestSqlFailedText {
            get {
                return ResourceManager.GetString("ConnectionFormTestSqlFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SQL Server connection test skipped.
        /// </summary>
        internal static string ConnectionFormTestSqlSkippedText {
            get {
                return ResourceManager.GetString("ConnectionFormTestSqlSkippedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SQL Server connection test successful.
        /// </summary>
        internal static string ConnectionFormTestSqlSuccessfulText {
            get {
                return ResourceManager.GetString("ConnectionFormTestSqlSuccessfulText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test Succeeded.
        /// </summary>
        internal static string ConnectionFormTestSuccessful {
            get {
                return ResourceManager.GetString("ConnectionFormTestSuccessful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The User Name must be specified for custom authentication.
        /// </summary>
        internal static string ConnectionFormUserNameBlankMessage {
            get {
                return ResourceManager.GetString("ConnectionFormUserNameBlankMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to empty.
        /// </summary>
        internal static string ErrorDialogEmptyText {
            get {
                return ResourceManager.GetString("ErrorDialogEmptyText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exception.
        /// </summary>
        internal static string ErrorDialogExceptionNodeText {
            get {
                return ResourceManager.GetString("ErrorDialogExceptionNodeText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hide Details....
        /// </summary>
        internal static string ErrorDialogHideDetailsText {
            get {
                return ResourceManager.GetString("ErrorDialogHideDetailsText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to null.
        /// </summary>
        internal static string ErrorDialogNullText {
            get {
                return ResourceManager.GetString("ErrorDialogNullText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error Details.
        /// </summary>
        internal static string ErrorDialogRootNodeText {
            get {
                return ResourceManager.GetString("ErrorDialogRootNodeText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Details....
        /// </summary>
        internal static string ErrorDialogShowDetailsText {
            get {
                return ResourceManager.GetString("ErrorDialogShowDetailsText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        internal static string ErrorMessageCaption {
            get {
                return ResourceManager.GetString("ErrorMessageCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Excel files|*{0}.
        /// </summary>
        internal static string ExcelFileFilter {
            get {
                return ResourceManager.GetString("ExcelFileFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to create.
        /// </summary>
        internal static string JobImportWizardFormFieldCreateText {
            get {
                return ResourceManager.GetString("JobImportWizardFormFieldCreateText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to What fields do you want to {0}?.
        /// </summary>
        internal static string JobImportWizardFormFieldLabelText {
            get {
                return ResourceManager.GetString("JobImportWizardFormFieldLabelText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to update.
        /// </summary>
        internal static string JobImportWizardFormFieldUpdateText {
            get {
                return ResourceManager.GetString("JobImportWizardFormFieldUpdateText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application closed{0}.
        /// </summary>
        internal static string LoggerApplicationClosed {
            get {
                return ResourceManager.GetString("LoggerApplicationClosed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application started.
        /// </summary>
        internal static string LoggerApplicationStarted {
            get {
                return ResourceManager.GetString("LoggerApplicationStarted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Batch.
        /// </summary>
        internal static string MainForm {
            get {
                return ResourceManager.GetString("MainForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch creation failed: {0}.
        /// </summary>
        internal static string MainFormBatchCreateFailedText {
            get {
                return ResourceManager.GetString("MainFormBatchCreateFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please create or open a batch.
        /// </summary>
        internal static string MainFormBatchCreateOrOpenText {
            get {
                return ResourceManager.GetString("MainFormBatchCreateOrOpenText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch created successfully.
        /// </summary>
        internal static string MainFormBatchCreateSuccessfulText {
            get {
                return ResourceManager.GetString("MainFormBatchCreateSuccessfulText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch failed: {0}.
        /// </summary>
        internal static string MainFormBatchFailedText {
            get {
                return ResourceManager.GetString("MainFormBatchFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch{0}.
        /// </summary>
        internal static string MainFormBatchFileDefaultName {
            get {
                return ResourceManager.GetString("MainFormBatchFileDefaultName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch files|*{0}.
        /// </summary>
        internal static string MainFormBatchFileFilter {
            get {
                return ResourceManager.GetString("MainFormBatchFileFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch is invalid.
        /// </summary>
        internal static string MainFormBatchInvalidText {
            get {
                return ResourceManager.GetString("MainFormBatchInvalidText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch load failed: {0}.
        /// </summary>
        internal static string MainFormBatchLoadFailedText {
            get {
                return ResourceManager.GetString("MainFormBatchLoadFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The batch has not been saved. Do you want to save?.
        /// </summary>
        internal static string MainFormBatchLoadSavePrompt {
            get {
                return ResourceManager.GetString("MainFormBatchLoadSavePrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch loaded successfully.
        /// </summary>
        internal static string MainFormBatchLoadSucceededText {
            get {
                return ResourceManager.GetString("MainFormBatchLoadSucceededText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The batch has not been saved. Do you want to save?.
        /// </summary>
        internal static string MainFormBatchOpenSavePrompt {
            get {
                return ResourceManager.GetString("MainFormBatchOpenSavePrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to run the batch on {0}?.
        /// </summary>
        internal static string MainFormBatchRunConfirmationCaption {
            get {
                return ResourceManager.GetString("MainFormBatchRunConfirmationCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Run Confirmation.
        /// </summary>
        internal static string MainFormBatchRunConfirmationTitle {
            get {
                return ResourceManager.GetString("MainFormBatchRunConfirmationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch running....
        /// </summary>
        internal static string MainFormBatchRunningText {
            get {
                return ResourceManager.GetString("MainFormBatchRunningText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch save failed: {0}.
        /// </summary>
        internal static string MainFormBatchSaveFailedText {
            get {
                return ResourceManager.GetString("MainFormBatchSaveFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Batch.
        /// </summary>
        internal static string MainFormBatchSaveMessage {
            get {
                return ResourceManager.GetString("MainFormBatchSaveMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch saved successfully.
        /// </summary>
        internal static string MainFormBatchSaveSuccessfulText {
            get {
                return ResourceManager.GetString("MainFormBatchSaveSuccessfulText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stop failed: {0}.
        /// </summary>
        internal static string MainFormBatchStopFailedText {
            get {
                return ResourceManager.GetString("MainFormBatchStopFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stopping....
        /// </summary>
        internal static string MainFormBatchStopStartedText {
            get {
                return ResourceManager.GetString("MainFormBatchStopStartedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stopped successfully.
        /// </summary>
        internal static string MainFormBatchStopSucceededText {
            get {
                return ResourceManager.GetString("MainFormBatchStopSucceededText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch completed successfully in {0}.
        /// </summary>
        internal static string MainFormBatchSucceededText {
            get {
                return ResourceManager.GetString("MainFormBatchSucceededText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Batch.
        /// </summary>
        internal static string MainFormCloseSaveCaption {
            get {
                return ResourceManager.GetString("MainFormCloseSaveCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The batch has not been saved. Do you want to save before closing?.
        /// </summary>
        internal static string MainFormCloseSaveMessage {
            get {
                return ResourceManager.GetString("MainFormCloseSaveMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exit Warning.
        /// </summary>
        internal static string MainFormCloseTaskRunningCaption {
            get {
                return ResourceManager.GetString("MainFormCloseTaskRunningCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A task is currently running. Are you sure you want to exit?.
        /// </summary>
        internal static string MainFormCloseTaskRunningText {
            get {
                return ResourceManager.GetString("MainFormCloseTaskRunningText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Failed to connect to {0}.
        /// </summary>
        internal static string MainFormConnectionConnectFailedText {
            get {
                return ResourceManager.GetString("MainFormConnectionConnectFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connecting to {0}.
        /// </summary>
        internal static string MainFormConnectionConnectingText {
            get {
                return ResourceManager.GetString("MainFormConnectionConnectingText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Successfully connected to {0}.
        /// </summary>
        internal static string MainFormConnectionConnectSucceededText {
            get {
                return ResourceManager.GetString("MainFormConnectionConnectSucceededText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection creation failed: {0}.
        /// </summary>
        internal static string MainFormConnectionCreateFailedText {
            get {
                return ResourceManager.GetString("MainFormConnectionCreateFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create new connection.
        /// </summary>
        internal static string MainFormConnectionCreateNewText {
            get {
                return ResourceManager.GetString("MainFormConnectionCreateNewText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection deletion failed: {0}.
        /// </summary>
        internal static string MainFormConnectionDeleteFailedText {
            get {
                return ResourceManager.GetString("MainFormConnectionDeleteFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete connection {0}?.
        /// </summary>
        internal static string MainFormConnectionDeleteMessage {
            get {
                return ResourceManager.GetString("MainFormConnectionDeleteMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection deleted successfully.
        /// </summary>
        internal static string MainFormConnectionDeleteSucceededText {
            get {
                return ResourceManager.GetString("MainFormConnectionDeleteSucceededText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection edit update failed: {0}.
        /// </summary>
        internal static string MainFormConnectionEditFailedText {
            get {
                return ResourceManager.GetString("MainFormConnectionEditFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not connected.
        /// </summary>
        internal static string MainFormConnectionNotConnectedText {
            get {
                return ResourceManager.GetString("MainFormConnectionNotConnectedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Drag and drop failed: {0}.
        /// </summary>
        internal static string MainFormDragDropFailedText {
            get {
                return ResourceManager.GetString("MainFormDragDropFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job import failed: {0}.
        /// </summary>
        internal static string MainFormJobImportFailedText {
            get {
                return ResourceManager.GetString("MainFormJobImportFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job import started....
        /// </summary>
        internal static string MainFormJobImportStartedText {
            get {
                return ResourceManager.GetString("MainFormJobImportStartedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job imported successfully.
        /// </summary>
        internal static string MainFormJobImportSuccessfulText {
            get {
                return ResourceManager.GetString("MainFormJobImportSuccessfulText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Form load failed: {0}.
        /// </summary>
        internal static string MainFormLoadFailedText {
            get {
                return ResourceManager.GetString("MainFormLoadFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Publish all failed: {0}.
        /// </summary>
        internal static string MainFormPublishAllFailedText {
            get {
                return ResourceManager.GetString("MainFormPublishAllFailedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Publishing all customisations on {0}....
        /// </summary>
        internal static string MainFormPublishAllStartedText {
            get {
                return ResourceManager.GetString("MainFormPublishAllStartedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Publish all completed successfully.
        /// </summary>
        internal static string MainFormPublishAllSucceededText {
            get {
                return ResourceManager.GetString("MainFormPublishAllSucceededText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The -batch parameter is missing.
        /// </summary>
        internal static string ProgramBatchParameterMissingText {
            get {
                return ResourceManager.GetString("ProgramBatchParameterMissingText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The specified connection could not be found.
        /// </summary>
        internal static string ProgramConnectionNotFoundText {
            get {
                return ResourceManager.GetString("ProgramConnectionNotFoundText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The -connection parameter is missing.
        /// </summary>
        internal static string ProgramConnectionParameterMissingText {
            get {
                return ResourceManager.GetString("ProgramConnectionParameterMissingText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Missing Dependencies.
        /// </summary>
        internal static string ProgramWindowsIdentityFoundationMissingCaption {
            get {
                return ResourceManager.GetString("ProgramWindowsIdentityFoundationMissingCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Windows Identity Foundation is required to run this program but is not installed on this computer.{0}{0}Do you want to view installation instructions?.
        /// </summary>
        internal static string ProgramWindowsIdentityFoundationMissingText {
            get {
                return ResourceManager.GetString("ProgramWindowsIdentityFoundationMissingText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to T-SQL scripts|*{0}.
        /// </summary>
        internal static string SqlFileFilter {
            get {
                return ResourceManager.GetString("SqlFileFilter", resourceCulture);
            }
        }
    }
}
