﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using ScottLane.Surgeon.Client.Forms;
using ScottLane.Surgeon.Client.Helpers;
using ScottLane.Surgeon.Client.Resources;
using ScottLane.Surgeon.Common.Resources;
using ScottLane.Surgeon.Model;

namespace ScottLane.Surgeon.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread] 
        static int Main(string[] arguments)
        {
            try
            {
                Logger.WriteLine(ClientStrings.LoggerApplicationStarted);

                if (CheckDependencies())
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    MainForm form = new MainForm();
                    ReadCommandLineArguments(arguments, form);
                    Application.Run(form);
                }

                Environment.ExitCode = 0;
            }
            catch (Exception ex)
            {
                Environment.ExitCode = -1;
                Logger.WriteLine(ex.Message);
            }
            finally
            {
                Logger.WriteLine(string.Format(ClientStrings.LoggerApplicationClosed, Environment.NewLine));
            }

            return Environment.ExitCode;
        }

        private static bool CheckDependencies()
        {
            bool success = true;

            try
            {
                Assembly.Load("Microsoft.IdentityModel, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35");
            }
            catch
            {
                success = false;

                if (MessageBox.Show(string.Format(ClientStrings.ProgramWindowsIdentityFoundationMissingText, Environment.NewLine), ClientStrings.ProgramWindowsIdentityFoundationMissingCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    if (Environment.OSVersion.Version.Major > 6 || Environment.OSVersion.Version.Major == 6 && Environment.OSVersion.Version.Minor > 1)
                    {
                        Process.Start("http://blogs.technet.com/b/tvishnun1/archive/2012/06/12/windows-identity-foundation-in-windows-8.aspx");
                    }
                    else
                    {
                        Process.Start("http://support.microsoft.com/kb/974405");
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Reads command line arguments and prepares for automatic execution if arguments are correct.
        /// </summary>
        /// <param name="arguments">The command line arguments.</param>
        /// <param name="form">The main form.</param>
        private static void ReadCommandLineArguments(string[] arguments, MainForm form)
        {
            if (arguments.Length > 0)
            {
                form.RunMode = RunMode.Automatic;

                // arguments are specified in pairs
                for (int argumentIndex = 0; argumentIndex < arguments.Length; argumentIndex = argumentIndex + 2)
                {
                    string command = arguments[argumentIndex];
                    string value = null;

                    if (argumentIndex < arguments.Length - 1)
                    {
                        value = arguments[argumentIndex + 1];
                    }

                    if (command.ToLower() == "-connection" || command.ToLower() == "-c")
                    {
                        // todo - implement predicate
                        foreach (Connection connection in Settings.Default.Connections)
                        {
                            if (connection.Name.ToLower() == value.ToLower())
                            {
                                form.Connection = connection;
                            }
                        }

                        if (form.Connection == null)
                        {
                            throw new ArgumentException(ClientStrings.ProgramConnectionNotFoundText, value);
                        }
                    }
                    else if (command.ToLower() == "-batch" || command.ToLower() == "-b")
                    {
                        form.Batch = Batch.Load(value);
                    }
                    else if (command == "/?")
                    {
                        Process.Start(CommonStrings.CommandLineInterfaceUrl);
                    }
                }

                if (form.Batch == null)
                {
                    throw new ArgumentException(ClientStrings.ProgramBatchParameterMissingText);
                }

                if (form.Connection == null)
                {
                    throw new ArgumentException(ClientStrings.ProgramConnectionParameterMissingText);
                }
            }
            else
            {
                form.RunMode = RunMode.Manual;
            }
        }
    }
}
